using org.bit.ElasticSearch.Models;
using org.bit.ElasticSearch.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace org.bit.ElasticSearch
{
  public class ElasticSearchManager
  {
    private const string SearchStr = "_search";
    private const string PrettyStr = "pretty";
    private const string OpenStr = "_open";
    private const string CloseStr = "_close";
    private const string SettingsStr = "_settings";
    private const string SizeStr = "size";
    private const int SearchCount = 1000;

    private readonly Uri _baseUri;

    public ElasticSearchManager(string url)
    {
      _baseUri = new Uri(url);
    }

    #region Настройки Index
    public bool HasIndex(string index)
    {
      var url = CreatePath(index, withoutPretty: true);

      var result = ElasticSender.Send(url);

      if (result.Success)
      {
        return true;
      }

      if (NotFoundResponseErrorTest(result))
      {
        return false;
      }

      throw result.Exception;
    }

    public async Task<bool> HasIndexAsync(string index)
    {
      var url = CreatePath(index, withoutPretty: true);

      var result = await ElasticSender.SendAsync(url);

      if (result.Success)
      {
        return true;
      }

      if (NotFoundResponseErrorTest(result))
      {
        return false;
      }

      throw result.Exception;
    }

    public bool OpenIndex(string index)
    {
      var url = CreatePath(index, OpenStr);

      var result = ElasticSender.Send(url, method: "POST");

      if (!result.Success)
      {
        return false;
      }

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public async Task<bool> OpenIndexAsync(string index)
    {
      var url = CreatePath(index, OpenStr);

      var result = await ElasticSender.SendAsync(url, method: "POST");

      if (!result.Success)
      {
        return false;
      }

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public bool CloseIndex(string index)
    {
      var url = CreatePath(index, CloseStr);

      var result = ElasticSender.Send(url, method: "POST");

      if (!result.Success)
      {
        return false;
      }

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public async Task<bool> CloseIndexAsync(string index)
    {
      var url = CreatePath(index, CloseStr);

      var result = await ElasticSender.SendAsync(url, method: "POST");

      if (!result.Success)
      {
        return false;
      }

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public bool DeleteIndex(string index)
    {
      var url = CreatePath(index, withoutPretty: true);

      var result = ElasticSender.Send(url, method: "DELETE");

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public async Task<bool> DeleteIndexAsync(string index)
    {
      var url = CreatePath(index, withoutPretty: true);

      var result = await ElasticSender.SendAsync(url, method: "DELETE");

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public bool CreateIndex(string index)
    {
      var url = CreatePath(index, withoutPretty: true);

      var result = ElasticSender.Send(url, method: "PUT");

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public async Task<bool> CreateIndexAsync(string index)
    {
      var url = CreatePath(index, withoutPretty: true);

      var result = await ElasticSender.SendAsync(url, method: "PUT");

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public dynamic GetIndexSettings(string index)
    {
      var url = CreatePath(index, SettingsStr, withoutPretty: true);

      var result = ElasticSender.Send(url);

      if (!result.Success)
      {
        throw result.Exception;
      }

      var obj = JsonUtility.Deserialize(result.Content);

      Console.WriteLine(result.Content);

      return obj;
    }

    public async Task<dynamic> GetIndexSettingsAsync(string index)
    {
      var url = CreatePath(index, SettingsStr, withoutPretty: true);

      var result = await ElasticSender.SendAsync(url);

      if (!result.Success)
      {
        throw result.Exception;
      }

      var obj = JsonUtility.Deserialize(result.Content);

      Console.WriteLine(result.Content);

      return obj;
    }

    public ElasticAnalyzerOptions GetIndexAnalyzerOptions(string index)
    {
      var settings = GetIndexSettings(index);

      IDictionary<string, object> dict = settings;

      if (!dict.ContainsKey(index))
      {
        return null;
      }

      dynamic indexEl = dict[index];

      var analyzer = indexEl.settings.index.analysis.analyzer;

      dict = analyzer;

      if (dict.Keys.Count == 0)
      {
        return null;
      }

      var analyzerName = dict.Keys.First();

      dynamic analyzerSettings = dict[analyzerName];

      var result = JsonUtility.Deserialize<ElasticAnalyzerOptions>(JsonUtility.Serialize(analyzerSettings));

      return result;
    }

    public async Task<ElasticAnalyzerOptions> GetIndexAnalyzerOptionsAsync(string index)
    {
      var settings = await GetIndexSettingsAsync(index);

      IDictionary<string, object> dict = settings;

      if (!dict.ContainsKey(index))
      {
        return null;
      }

      dynamic indexEl = dict[index];

      var analyzer = indexEl.settings.index.analysis.analyzer;

      dict = analyzer;

      if (dict.Keys.Count == 0)
      {
        return null;
      }

      var analyzerName = dict.Keys.First();

      dynamic analyzerSettings = dict[analyzerName];

      var result = JsonUtility.Deserialize<ElasticAnalyzerOptions>(JsonUtility.Serialize(analyzerSettings));

      return result;
    }

    public bool SetIndexAnalyzer(string index, string name, ElasticAnalyzerOptions options = null)
    {
      if (!CloseIndex(index))
      {
        return false;
      }

      var url = CreatePath(index, SettingsStr, withoutPretty: true);

      var settings = new ElasticSettings(name, options ?? new ElasticAnalyzerOptions());

      var json = JsonUtility.Serialize(settings);

      var result = ElasticSender.Send(url, json, method: "PUT");

      if (!OpenIndex(index))
      {
        return false;
      }

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }

    public async Task<bool> SetIndexAnalyzerAsync(string index, string name, ElasticAnalyzerOptions options = null)
    {
      if (!CloseIndex(index))
      {
        return false;
      }

      var url = CreatePath(index, SettingsStr, withoutPretty: true);

      var settings = new ElasticSettings(name, options ?? new ElasticAnalyzerOptions());

      var json = JsonUtility.Serialize(settings);

      var result = await ElasticSender.SendAsync(url, json, method: "PUT");

      if (!OpenIndex(index))
      {
        return false;
      }

      var answer = JsonUtility.Deserialize<ElasticCommandAnswer>(result.Content);

      return answer.Acknowledged;
    }
    #endregion

    public bool DeleteType(string index, string type, string id)
    {
      var sendPath = CreatePath(index, type, id);

      var result = ElasticSender.Send(sendPath, method: "DELETE");

      if (result.Success)
      {
        var answer = JsonUtility.Deserialize<ElasticAddAnswer>(result.Content);

        return answer.Result == ElasticResultType.Deleted;
      }

      if (NotFoundResponseErrorTest(result))
      {
        return true;
      }

      throw result.Exception;
    }

    public async Task<bool> DeleteTypeAsync(string index, string type, string id)
    {
      var sendPath = CreatePath(index, type, id);

      var result = await ElasticSender.SendAsync(sendPath, method: "DELETE");

      if (result.Success)
      {
        var answer = JsonUtility.Deserialize<ElasticAddAnswer>(result.Content);

        return answer.Result == ElasticResultType.Deleted;
      }

      if (NotFoundResponseErrorTest(result))
      {
        return true;
      }

      throw result.Exception;
    }

    public bool AddTypeObject(string index, string type, string id, object obj)
    {
      if (string.IsNullOrWhiteSpace(id))
      {
        throw new ArgumentException("Предан пустой параметер id", nameof(id));
      }

      var sendPath = CreatePath(index, type, id);

      var json = JsonUtility.Serialize(obj);

      var result = ElasticSender.Send(sendPath, json);

      if (!result.Success)
      {
        throw result.Exception;
      }

      var answer = JsonUtility.Deserialize<ElasticAddAnswer>(result.Content);

      return answer.Result == ElasticResultType.Created || answer.Result == ElasticResultType.Updated;

    }

    public async Task<bool> AddTypeObjectAsync(string index, string type, string id, object obj)
    {
      if (string.IsNullOrWhiteSpace(id))
      {
        throw new ArgumentException("Предан пустой параметер id", nameof(id));
      }

      var sendPath = CreatePath(index, type, id);

      var json = JsonUtility.Serialize(obj);

      var result = await ElasticSender.SendAsync(sendPath, json);

      if (!result.Success)
      {
        throw result.Exception;
      }

      var answer = JsonUtility.Deserialize<ElasticAddAnswer>(result.Content);

      return answer.Result == ElasticResultType.Created || answer.Result == ElasticResultType.Updated;


    }

    public IEnumerable<ElasticHit> Search(IEnumerable<string> indexes, string query, IEnumerable<string> searchFields)
    {
      var sendPath = CreatePath(string.Join(',', indexes), SearchStr, countSearch: SearchCount);

      var search = new Models.ElasticSearch
      {
        QueryString = query,
        SearchFields = searchFields,
        Source = false
      };

      var json = JsonUtility.Serialize(search);

      var result = ElasticSender.Send(sendPath, json);

      if (!result.Success)
      {
        return null;
      }

      var searchResult = JsonUtility.Deserialize<ElasticSearchAnswer>(result.Content);

      return searchResult.Hits.Hits;
    }

    public async Task<IEnumerable<ElasticHit>> SearchAsync(IEnumerable<string> indexes, string query, IEnumerable<string> searchFields)
    {
      var sendPath = CreatePath(string.Join(',', indexes), SearchStr, countSearch: SearchCount);

      var search = new Models.ElasticSearch
      {
        QueryString = query,
        SearchFields = searchFields,
        Source = false
      };

      var json = JsonUtility.Serialize(search);

      var result = await ElasticSender.SendAsync(sendPath, json);

      if (!result.Success)
      {
        return null;
      }

      var searchResult = JsonUtility.Deserialize<ElasticSearchAnswer>(result.Content);

      return searchResult.Hits.Hits;
    }

    #region Private Methods
    private string CreatePath(string index, string additionalPath = null, string id = null, bool withoutPretty = false, int? countSearch = null)
    {
      var adParams = new List<string>();

      if (countSearch.HasValue)
      {
        adParams.Add(string.Format("{0}={1}", SizeStr, countSearch));
      }

      if (!withoutPretty)
      {
        adParams.Add(PrettyStr);
      }

      var paramsStr = adParams.Any()
        ? $"?{string.Join("&", adParams)}"
        : string.Empty;

      var result = new Uri(_baseUri, $"{index}{(string.IsNullOrWhiteSpace(additionalPath) ? string.Empty : "/" + additionalPath.Trim())}{(string.IsNullOrWhiteSpace(id) ? string.Empty : "/" + id.Trim())}{paramsStr}");//{(withoutPretty? string.Empty : "?" + PrettyStr)}");

      return result.ToString();
    }

    private static bool NotFoundResponseErrorTest(ElasticSenderResult result)
    {
      if (!(result.Exception is WebException ex))
      {
        return false;
      }

      return ((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound;
    }
    #endregion
  }
}
