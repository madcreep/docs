using System;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{

  //"_index" : "blog",
  //"_type" : "post",
  //"_id" : "2",
  //"_version" : 3,
  //"result" : "updated",
  //"_shards" : {
  //  "total" : 2,
  //  "successful" : 1,
  //  "failed" : 0
  //},
  //"_seq_no" : 5,
  //"_primary_term" : 1

  public class ElasticAddAnswer : ElasticAnswer
  {
    [JsonProperty("result")] private string _result;

    [JsonIgnore]
    public ElasticResultType Result {
      get
      {
        Enum.TryParse(_result, true, out ElasticResultType outputValue);

        return outputValue;

      }
    }

    [JsonProperty("_shards")]
    public ElasticShards ElasticShards { get; set; }
  }
}
