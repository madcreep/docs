using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
  public class ElasticSettings
  {
    [JsonProperty("settings")] internal ElasticInternalSettings Settings { get; set; }

    public ElasticSettings(string name, ElasticAnalyzerOptions options)
    {
      Settings = new ElasticInternalSettings(name, options);
    }
  }

  internal class ElasticInternalSettings
  {
    [JsonProperty("analysis")] internal ElasticAnalysis Analysis { get; set; }

    public ElasticInternalSettings(string name, ElasticAnalyzerOptions options)
    {
      Analysis = new ElasticAnalysis(name, options);
    }

  }

  public class ElasticAnalysis
  {
    [JsonProperty("analyzer")] public dynamic Analyzer { get; set; }

    public ElasticAnalysis(string name, ElasticAnalyzerOptions options)
    {
      var result = new System.Dynamic.ExpandoObject();

      var expandoDict = (IDictionary<string, object>)result;

      expandoDict.Add(name.ToLower(), options);

      Analyzer = result;
    }
  }

  public class ElasticAnalyzerOptions
  {
    [JsonProperty("type")] public string Type { get; set; } = "standard";

    [JsonProperty("stopwords")] public string Stopwords { get; set; } = "_russian_";

    [JsonProperty("max_token_length")] public int? MaxTokenLength { get; set; } = 5;
  }
}
