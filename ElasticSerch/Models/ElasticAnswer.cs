using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
  public class ElasticAnswer
  {
    [JsonProperty("_index")]
    public string Index { get; set; }

    [JsonProperty("_type")]
    public string Type { get; set; }

    [JsonProperty("_id")]
    public string Id { get; set; }

    [JsonProperty("_version")]
    public int Version { get; set; }

    [JsonProperty("_seq_no")]
    public int SeqNo { get; set; }

    [JsonProperty("_primary_term")]
    public int PrimaryTerm { get; set; }
  }
}
