using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{

  //{
  //  "acknowledged" : true,
  //  "shards_acknowledged" : true,
  //  "indices" : {
  //    "blog" : {
  //      "closed" : true
  //    }
  //  }
  //}

  internal class ElasticCommandAnswer
  {
    [JsonProperty("acknowledged")] public bool Acknowledged { get; set; }

    [JsonProperty("shards_acknowledged")] public bool ShardsAcknowledged { get; set; }
  }
}
