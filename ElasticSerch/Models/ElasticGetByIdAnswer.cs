using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
//{
//  "_index" : "blog",
//  "_type" : "post",
//  "_id" : "2",
//  "_version" : 7,
//  "_seq_no" : 9,
//  "_primary_term" : 1,
//  "found" : true,
//  "_source" : {
//     "title" : "Веселые котята",
//     "content" : "<p>Смешная история про котят<p>",
//     "tags" : [
//        "котята",
//        "смешная история"
//     ],
//     "published_at" : "2014-09-12T20:44:42+00:00"
//   }
//}
  public class ElasticGetByIdAnswer<T> : ElasticAnswer
  {
    [JsonProperty("found")]
    public bool Found { get; set; }

    [JsonProperty("_source")]
    public T Source { get; set; }
  }
}
