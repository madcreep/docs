using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
  public class ElasticHits
  {
    [JsonProperty("total")] public ElasticTotal Total { get; set; }

    [JsonProperty("max_score")] public decimal? MaxScore { get; set; }

    [JsonProperty("hits")] public List<ElasticHit> Hits { get; set; }
  }

  public class ElasticTotal
  {
    [JsonProperty("value")] public int Value { get; set; }

    [JsonProperty("relation")] public string Relation { get; set; }
  }

  public class ElasticHit
  {
    [JsonProperty("_index")] public string Index { get; set; }

    [JsonProperty("_type")] public string Type { get; set; }

    [JsonProperty("_id")] public string Id { get; set; }

    [JsonProperty("_score")] public string Score { get; set; }
  }

}
