using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
  public enum ElasticResultType
  {
    [EnumMember(Value = "undefined")]
    Undefined = 0,
    [EnumMember(Value = "created")]
    Created,
    [EnumMember(Value = "updated")]
    Updated,
    [EnumMember(Value = "deleted")]
    Deleted
  }
}
