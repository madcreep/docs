using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using org.bit.ElasticSearch.Attributes;

namespace org.bit.ElasticSearch.Models
{
  public class ElasticSearch<T>
  {
    public ElasticSearch(T obj, string query, ElasticSearchOptions options = null)
    {
      Query = new ElasticQuery<T>(obj, query, options);
    }

    [JsonProperty("query")] public ElasticQuery<T> Query { get; set; }

    [JsonProperty("_source")] public bool Source { get; set; } = true;
  }

  public class ElasticQuery<T>
  {
    public ElasticQuery(T match, string query, ElasticSearchOptions options = null)
    {
      //Match = match;
      MultiMatch = new MultiMatch<T>(match, query, options);
    }

    //[JsonProperty("match")]
    //public T Match { get; set; }

    [JsonProperty("multi_match")]
    public MultiMatch<T> MultiMatch { get; set; }
  }

  public class MultiMatch<T> : ElasticSearchOptions
  {
    private readonly T _obj;

    [JsonProperty("fields")]
    public string[] Fields { get; set; }

    [JsonProperty("query")]
    public string Query { get; set; }

    public MultiMatch(T obj, string query, ElasticSearchOptions options = null)
    {
      if (obj == null)
      {
        throw new ArgumentNullException(nameof(obj));
      }
      _obj = obj;

      Fields = GetSearchFields();

      Query = query;

      if (options != null)
      {
        ApplyOptions(options);
      }
    }

    private string[] GetSearchFields()
    {
      var type = _obj.GetType();

      var properties = type.GetProperties(BindingFlags.Public)
        .Where(prop => prop.IsDefined(typeof(ElasticFieldSearchAttribute), false))
        .Select(prop =>
        {
          if (!prop.IsDefined(typeof(JsonPropertyAttribute), false))
          {
            return prop.Name.ToLower();
          }

          var attr = (JsonPropertyAttribute)prop.GetCustomAttribute(typeof(JsonPropertyAttribute), false);

          return string.IsNullOrWhiteSpace(attr.PropertyName) ? prop.Name.ToLower() : attr.PropertyName;

        });

      return properties.ToArray();
    }

    private void ApplyOptions(ElasticSearchOptions options)
    {
      this.CopyOptions(options);
    }
  }


  public class ElasticSearch
  {
    [JsonIgnore] public string QueryString
    {
      get => _query.MultiMatch.Query;
      set => _query.MultiMatch.Query = value;
    }

    [JsonIgnore] public IEnumerable<string> SearchFields
    {
      get => _query.MultiMatch.Fields;
      set => _query.MultiMatch.Fields = value;
    }

    [JsonIgnore]
    public ElasticSearchOptions Options
    {
      get => _query.MultiMatch.Options;
      set => _query.MultiMatch.Options = value;
    }

    [JsonProperty("_source")] public bool Source { get; set; }

    [JsonProperty("query")]
    private ElasticQuery _query = new ElasticQuery();
  }

  public class ElasticQuery
  {
    //[JsonProperty("match")]
    //public T Match { get; set; }

    [JsonProperty("multi_match")]
    public MultiMatch MultiMatch { get; set; } = new MultiMatch();
  }

  public class MultiMatch : ElasticSearchOptions
  {
    [JsonProperty("query")] public string Query { get; set; } = string.Empty;

    [JsonIgnore] private IEnumerable<string> _fields = new string[0];

    [JsonProperty("fields")] private IEnumerable<string> _fieldsForJson = new string[0];

    [JsonIgnore]
    public IEnumerable<string> Fields
    {
      get => _fields;
      set
      {
        _fields = value;

        _fieldsForJson = value
          .Select(el => el.ToLower())
          .ToArray();
      }
    }

    [JsonIgnore] private ElasticSearchOptions _options = new ElasticSearchOptions();

    [JsonIgnore]
    public ElasticSearchOptions Options
    {
      get => _options;

      set
      {
        _options = value ?? new ElasticSearchOptions();

        ApplyOptions(_options);
      }
    }

    private void ApplyOptions(ElasticSearchOptions options)
    {
      this.CopyOptions(options);
    }
  }
}
