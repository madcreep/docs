using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
  //{
  //  "took" : 12,
  //  "timed_out" : false,
  //  "_shards" : {
  //    "total" : 1,
  //    "successful" : 1,
  //    "skipped" : 0,
  //    "failed" : 0
  //  },
  //  "hits" : {
  //    "total" : {
  //      "value" : 1,
  //      "relation" : "eq"
  //    },
  //    "max_score" : 0.3955629,
  //    "hits" : [
  //      {
  //        "_index" : "blog",
  //        "_type" : "post",
  //        "_id" : "2",
  //        "_score" : 0.3955629
  //      }
  //    ]
  //  }
  //}

  public class ElasticSearchAnswer
  {
    [JsonProperty("took")] public int? Took { get; set; }

    [JsonProperty("timed_out")] public bool? TimedOut { get; set; }

    [JsonProperty("_shards")] public ElasticShards Shards { get; set; }

    [JsonProperty("hits")] public ElasticHits Hits { get; set; }
  }
}
