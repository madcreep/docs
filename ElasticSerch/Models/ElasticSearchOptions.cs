using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
  public class ElasticSearchOptions
  {
    //[JsonProperty("fuzziness")] public string Fuzziness { get; set; } = "6";
    [JsonIgnore] public string Fuzziness { get; set; } = "6";

    [JsonProperty("prefix_length")] public int? PrefixLength { get; set; } = 2;
  }

  public static class ElasticExtensions
  {
    public static void CopyOptions(this ElasticSearchOptions to, ElasticSearchOptions from)
    {
      to.Fuzziness = from.Fuzziness;
      to.PrefixLength = from.PrefixLength;
    }
  }
}
