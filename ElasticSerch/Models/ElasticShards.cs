using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace org.bit.ElasticSearch.Models
{
  public class ElasticShards
  {
    [JsonProperty("total")] public int? Total { get; set; }

    [JsonProperty("successful")] public int? Successful { get; set; }

    [JsonProperty("skipped")] public int? Skipped { get; set; }

    [JsonProperty("failed")] public int? Failed { get; set; }
  }
}
