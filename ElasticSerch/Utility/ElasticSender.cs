using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace org.bit.ElasticSearch.Utility
{
  public static class ElasticSender
  {
    private static readonly Mutex SyncMutex;

    static ElasticSender()
    {
      SyncMutex = new Mutex();
    }

    public static ElasticSenderResult Send(string uri, string content = null, string method = null)
    {
      var request = CreateRequest(uri, content, method);

      var result = GetResponse(request);

      return result;
    }

    public static async Task<ElasticSenderResult> SendAsync(string uri, string content = null, string method = null)
    {
      var request = CreateRequest(uri, content, method);

      return await GetResponseAsync(request);
    }

    private static WebRequest CreateRequest(string uri, string json, string method)
    {
      var httpWebRequest = (HttpWebRequest) WebRequest.Create(uri);

      if (string.IsNullOrWhiteSpace(json))
      {
        httpWebRequest.Method = method ?? "GET";

        return httpWebRequest;
      }

      httpWebRequest.Method = method ?? "POST";

      httpWebRequest.ContentType = "application/json";

      using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
      {
        streamWriter.Write(json);
      }

      return httpWebRequest;
    }

    private static ElasticSenderResult GetResponse(WebRequest request)
    {
      HttpWebResponse httpResponse = null;
      ElasticSenderResult result;
      try
      {
        if (SyncMutex.WaitOne(10000))
        {
          throw new TimeoutException("Истекло время ожидания освобождения мьютекса (ElasticSender.GetResponse)");
        }

        httpResponse = (HttpWebResponse)request.GetResponse();

        var responseStream = httpResponse?.GetResponseStream();

        string answer;

        if (responseStream == null)
        {
          answer = null;
        }
        else
        {
          using (var streamReader = new StreamReader(responseStream))
          {
            answer = streamReader.ReadToEnd();
          }
        }

        result = answer == null ? null : new ElasticSenderResult(answer);
      }
      catch (Exception ex)
      {
        result = new ElasticSenderResult(ex);
      }
      finally
      {
        httpResponse?.Close();

        httpResponse?.Dispose();

        SyncMutex.ReleaseMutex();
      }

      return result;
    }

    private static async Task<ElasticSenderResult> GetResponseAsync(WebRequest request)
    {
      HttpWebResponse httpResponse = null;

      try
      {
        var response = await request.GetResponseAsync();

        httpResponse = (HttpWebResponse)response;

        var responseStream = httpResponse.GetResponseStream();

        if (responseStream == null)
        {
          return null;
        }

        using (var streamReader = new StreamReader(responseStream))
        {
          var result = await streamReader.ReadToEndAsync();

          return new ElasticSenderResult(result);
        }
      }
      catch (Exception ex)
      {
        return new ElasticSenderResult(ex);
      }
      finally
      {
        httpResponse?.Close();

        httpResponse?.Dispose();
      }
    }
  }
}
