using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;

namespace org.bit.ElasticSearch.Utility
{
  public class ElasticSenderResult
  {
    public Exception Exception { get; }

    public string Content { get; }

    public bool Success => Exception == null;

    private ElasticSenderResult(string content, Exception ex)
    {
      Exception = ex;

      Content = content;
    }

    public ElasticSenderResult(string content)
      : this(content, null)
    { }

    public ElasticSenderResult(Exception ex)
      : this(null, ex)
    { }
  }
}
