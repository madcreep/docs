using System.Dynamic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace org.bit.ElasticSearch.Utility
{
  public static class JsonUtility
  {
    public static string Serialize(object obj)
    {
      var serializer = new JsonSerializer();

      serializer.Converters.Add(new JavaScriptDateTimeConverter());

      serializer.NullValueHandling = NullValueHandling.Ignore;

      //2014-09-12T20:44:42+00:00

      return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings
      {
        NullValueHandling = NullValueHandling.Ignore,
        DateFormatString = "yyyy-MM-ddTHH:mm:sszzz",
        ContractResolver = new CamelCasePropertyNamesContractResolver()
      });
    }

    public static T Deserialize<T>(string json)
    {
      if (string.IsNullOrWhiteSpace(json))
      {
        return default(T);
      }

      var answer = (T)JsonConvert.DeserializeObject(json, typeof(T));

      return answer;
    }

    public static dynamic Deserialize(string json)
    {
      var converter = new ExpandoObjectConverter();

      var answer = JsonConvert.DeserializeObject<ExpandoObject>(json, converter);

      return answer;
    }
  }
}
