using System.Security.Claims;
using System.Threading.Tasks;
using docs.Controllers;
using docs.Database;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace docs.Areas.Authorization.Controllers
{
  [Area("Authorization")]
  public class LoginController : Controller
  {
    private readonly UserManager<User> _userManager;
    private readonly SignInManager<User> _signInManager;
    private readonly RoleManager<Role> _roleManager;

    public LoginController(UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
    {
      _userManager = userManager;
      _roleManager = roleManager;
      _signInManager = signInManager;
    }

    public async Task<IActionResult> Login(string login, string password)
    {
      try
      {
        if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password))
        {
          return RedirectToAction("Index", "LegislativeProcess", new { area = "Users" });
        }

        var dbUser = await _userManager.FindByEmailAsync(login);

        if (dbUser == null)
        {
          return RedirectToAction("Index", "LegislativeProcess", new { area = "Users" });
        }

        var valid = await _userManager.CheckPasswordAsync(dbUser, password);

        if (valid)
        {
          var signInResult = await ProceedWithLogin(dbUser);

          if (signInResult)
          {
            if (await _userManager.IsInRoleAsync(dbUser, "user"))
            {
              return RedirectToAction("Index", "Sessions", new { area = "Users" });
            }

            if (await _userManager.IsInRoleAsync(dbUser, "admin"))
            {
              return RedirectToAction("Index", "Administration", new { area = "Management" });
            }
          }
        }

        return RedirectToAction("Index", "LegislativeProcess", new { area = "Users" });
      }
      catch
      {
        return RedirectToAction("Index", "LegislativeProcess", new { area = "Users" });
      }
    }

    [Authorize]
    public async Task<IActionResult> Logout()
    {
      await _signInManager.SignOutAsync();

      return RedirectToAction("Index", "LegislativeProcess", new { area = "Users" });
    }

    private async Task<bool> ProceedWithLogin(User dbUser)
    {
      var userForm = await AuthService.UserToUserForm(dbUser, _userManager, _roleManager);

      var principal = await _signInManager.CreateUserPrincipalAsync(dbUser);

      if (!(principal.Identity is ClaimsIdentity identity))
      {
        throw new UserException("Вход не выполнен");
      }

      identity.AddClaims(AuthService.GetClaims(userForm));

      await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, principal, new AuthenticationProperties
      {
        IsPersistent = false
      });

      return principal.Identity.IsAuthenticated;
    }
  }
}
