using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using docs.Database;
using docs.Extensions;
using docs.Helpers.Find;
using docs.Helpers.Mail;
using docs.Helpers.Xls;
using docs.Managers;
using docs.Managers.ElasticSearch;
using docs.Models;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Syncfusion.XlsIO.Calculate;

namespace docs.Areas.Files.Controllers
{
  [Area("Files")]
  public class FileController : Controller
  {
    private readonly DocumentsContext _context;

    public FileController([FromServices] DocumentsContext context)
    {
      _context = context;
    }

    [HttpGet]
    [Route("Files/File/Get/{docUuid}/{fileUuid}/{fileName}")]
    public async Task<IActionResult> Get(string fileName, Guid docUuid, Guid fileUuid)
    {
      var stream = await GetFileStream(fileName, docUuid, fileUuid);

      return File(stream, MediaTypeNames.Application.Pdf);
    }

    [HttpGet]
    [Route("Files/File/Download/{docUuid}/{fileUuid}/{fileName}")]
    public async Task<IActionResult> Download(string fileName, Guid docUuid, Guid fileUuid)
    {
      var stream = await GetFileStream(fileName, docUuid, fileUuid);

      return File(stream, MediaTypeNames.Application.Pdf, fileName);

    }

    [HttpPost]
    public async Task<IActionResult> UploadAntiCorruptionFile([FromServices] IMailManager mailManager, [FromForm] FInfo fInfo)
    {
      try
      {
        string filesDirectoryName;

        string folderName;

        do
        {
          filesDirectoryName = Guid.NewGuid().ToString();

          folderName = Path.Combine(mailManager.MailStoragePath, filesDirectoryName);

        } while (Directory.Exists(folderName));

        Directory.CreateDirectory(folderName);

        var doc = await _context.Documents
          .FirstOrDefaultAsync(el => el.Uuid == fInfo.docUuid);

        if (doc == null)
        {
          return Json(new { success = false, message = "Не найден документ" });
        }

        var now = DateTime.Today;

        if (!doc.AnticorruptionExpertise
            || doc.ExpertiseStart > now
            || doc.ExpertiseEnd < now)
        {
          return Json(new
          { success = false, message = "Для данного документа прием антикоррупционных экспертиз закрыт" });
        }

        var formFile = Request.Form.Files[0];

        if (formFile.Length > 1048576)
        {
          return Json(new
          { success = false, message = "Загрузка документов размером более 1Mb запрещена" });
        }

        var fileFullName = Path.Combine(folderName, formFile.FileName);

        using (var stream = new FileStream(fileFullName, FileMode.Create))
        {
          await formFile.CopyToAsync(stream);

          await stream.FlushAsync();

          stream.Close();
        }

        var message = new AntiCorruptionMessage(doc, filesDirectoryName);

        var sendResult = await mailManager.SendAsync(message, doc.ProfileCommittee);

        if (!sendResult)
        {
          return Json(new
          { success = false, message = "Ошибка отправки сообщения на E_mail" });
        }

        return Json(new {success = true});
      }
      catch (Exception)
      {
        return Json(new {success = false, message = "Ошибка сервера"});
      }
    }

    [HttpGet]
    [Route("Files/File/GetSessionSummary/{sessionId}")]
    public async Task<IActionResult> GetSessionSummary([FromServices] IConfiguration configuration, long? sessionId, string baseSiteName)
    {
      if (!sessionId.HasValue)
      {
        return NotFound();
      }

      var sitesForConclusion = configuration.GetSection("SitesForConclusion").Get<List<string>>();

      var forLan = sitesForConclusion.Contains(baseSiteName);

      var findModel = new FindFormModel
      {
        SessionFind = sessionId
      };

      var session = await _context.Sessions
        .WithAllFields()
        .ById(sessionId.Value)
        .FirstOrDefaultAsync();

      if (session == null)
      {
        return NotFound();
      }

      var sType = session.SessionType == "Очередная" ? "очередном" : "внеочередном";

      var title = new[]
      {
        $"Сводка принятых документов на {sType} {session.NumberInt} пленарном заседании",
        $"Законодательного Cобрания Краснодарского края {session.Convocation.Number} созыва"
      };

      var findResult = await FindDocumentsHelper.FindAsync(_context, findModel, null, forLan);

      var stream = ExcelGenerator.GenerateSessionDocumentsSummary(findResult.Documents.ToArray(), title);

      var fileName = $"Сводка по документам {session.NumberInt} сессии.xlsx";

      stream.Position = 0;

      return File(stream, MediaTypeNames.Application.Octet, fileName);
     
    }

    //[HttpGet]
    [Route("Files/File/GetFindSummary")]
    public async Task<IActionResult> GetFindSummary(
      [FromServices] IConfiguration configuration,
      [FromServices] ElasticManager elasticManager,
      [FromQuery] FindFormModel findModel)
    {
      var sitesForConclusion = configuration.GetSection("SitesForConclusion").Get<List<string>>();

      var forLan = sitesForConclusion.Contains(findModel.BaseSiteName);

      var findResult = await FindDocumentsHelper.FindAsync(_context, findModel, elasticManager, forLan);

      var title = new List<string>();

      title.Add($"Результаты поиска ({DateTime.Today:dd.MM.yyyy})");

      if (!findModel.IsEmpty)
      {
        title.Add("Параметры фильтра:");

        if (!string.IsNullOrWhiteSpace(findModel.ContextFind))
        {
          title.Add($"Поиск по контексту : {findModel.ContextFind}");
        }

        if (findModel.NpaFind.HasValue)
        {
          title.Add($"Форма НПА : {findModel.NpaFind.Value.FindName()}");
        }

        if (findModel.DateFind.HasValue)
        {
          title.Add($"Год : {findModel.DateFind.Value}");
        }

        if (findModel.SessionFind.HasValue)
        {
          var session = await _context.Sessions
            .ById(findModel.SessionFind.Value)
            .FirstOrDefaultAsync();

          if (session != null)
          {
            title.Add($"Сесия : {session.NumberInt} {session.SessionType}");
          }
        }

        if (!string.IsNullOrWhiteSpace(findModel.NumberActFind))
        {
          title.Add($"Номер правового акта : {findModel.NumberActFind}");
        }

        if (!string.IsNullOrWhiteSpace(findModel.SubjectFind))
        {
          title.Add($"Субъект права законодательной инициативы : {findModel.SubjectFind.Replace("\r", string.Empty).Replace("\n", string.Empty)}");
        }

        if (findModel.HasExpertizeFind)
        {
          var expertizeStr = new StringBuilder("Антикоррупционная экспертиза : есть");

          if (findModel.DateExpertizeFromFind.HasValue)
          {
            expertizeStr.Append($" c {findModel.DateExpertizeFromFind:dd.MM.yyyy}");
          }

          if (findModel.DateExpertizeToFind.HasValue)
          {
            expertizeStr.Append($" по {findModel.DateExpertizeToFind:dd.MM.yyyy}");
          }

          title.Add(expertizeStr.ToString());
        }
      }

      var stream = ExcelGenerator.GenerateSessionDocumentsSummary(findResult.Documents.ToArray(), title.ToArray());

      const string fileName = "Результаты поиска (Законодательный процесс).xlsx";

      stream.Position = 0;

      return File(stream, MediaTypeNames.Application.Octet, fileName);

    }

    private async Task<Stream> GetFileStream(string fileName, Guid docUuid, Guid fileUuid)
    {
      var file = await _context.DocumentFiles
        .FirstOrDefaultAsync(el => el.Uuid == fileUuid);

      if (file == null)
      {
        return null;
      }

      var filePath = FileManager.GetFilePath(file);

      if (!System.IO.File.Exists(filePath))
      {
        return null;
      }

      var memory = new MemoryStream();

      using (var stream = new FileStream(filePath, FileMode.Open))
      {
        await stream.CopyToAsync(memory);
      }

      memory.Position = 0;

      return memory;
    }

    public class FInfo
    {
      public Guid? docUuid { get; set; }
    }
  }
}
