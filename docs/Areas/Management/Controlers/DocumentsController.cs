using docs.Database;
using docs.Extensions;
using docs.Managers.ElasticSearch;
using docs.Models;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docs.Areas.Management.Contrilers
{
    [Area("Management")]
    [Authorize(Roles = "admin")]
    public class DocumentsController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DocumentsContext));

        private readonly DocumentsContext _context;

        public DocumentsController(DocumentsContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] bool? displayed)
        {
            var convocations = await _context.Convocations
              .WithAllFields()
              .OrderByDescending(el => el.Number)
              .ToArrayAsync();

            var convocationsForSelectList = convocations
              .Select(el => new
              {
                  el.Id,
                  el.Number,
                  Name = el.Number + " созыв"
              })
              .ToArray();

            var first = convocationsForSelectList.FirstOrDefault();

            ViewBag.Convocations = new SelectList(convocationsForSelectList, "Id", "Name", first?.Id);

            return View(convocations);
        }

        public async Task<IActionResult> GetDocumentsBySession(long? sessionId)
        {
            var documents = _context.Documents
              .OnlyProjects();

            if (sessionId.HasValue)
            {
                documents = documents
                  .Where(el => el.ChildeDocuments.Any(x =>
                     (x.DocumentType == DocumentType.Decree) && x.SessionId != null))
                  .Where(el =>
                    el.ChildeDocuments.Where(x => x.DocumentType == DocumentType.Decree && x.SessionId != null)
                      .OrderByDescending(x => x.Date).First().SessionId == sessionId.Value);
            }
            else
            {
                documents = documents
                  .Where(el => !el.ChildeDocuments.Any(x =>
                    (x.DocumentType == DocumentType.Decree) && x.SessionId != null));
            }

            documents = documents
              .WithAllFields();

            return PartialView("SessionDocuments", await documents.ToArrayAsync());
        }

        [HttpGet]
        public async Task<IActionResult> GetDocumentsBySessionJson(long? sessionId)
        {
            IQueryable<Document> docResult;

            var documents = _context.Documents
              .WithAllFields();

            if (sessionId.HasValue)
            {
                var findModel = new FindFormModel
                {
                    SessionFind = sessionId
                };

                docResult = documents
                  .Find(findModel);
            }
            else
            {
                docResult = documents
                  .Where(el => (el.DocumentType == DocumentType.DecreeProject && el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Decree))
                               || (el.DocumentType == DocumentType.LawProject && el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Law) && el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Decree))
                               || ((el.DocumentType == DocumentType.Law || el.DocumentType == DocumentType.Decree) && el.ParentUuid == null && el.SessionId == null));
            }

            var result = docResult
              .Select(el => new
              {
                  uuid = el.Uuid,
                  displayOnTheSite = el.ParentDocument == null ? el.DisplayOnTheSite : el.ParentDocument.DisplayOnTheSite,
                  documentNumber = el.FinalDocumentNumber,
                  documentName = el.DocumentType.Name(),
                  content = el.Content,
                  date = el.Date,
                  anticorruptionExpertise = el.ParentDocument == null ? el.AnticorruptionExpertise : el.ParentDocument.AnticorruptionExpertise,
                  expertiseStart = el.ParentDocument == null ? el.ExpertiseStart : el.ParentDocument.ExpertiseStart,
                  expertiseEnd = el.ParentDocument == null ? el.ExpertiseEnd : el.ParentDocument.ExpertiseEnd,
                  publishDate = el.FinalPublishDate,
                  dependentDocuments = el.ParentUuid == null

                  ? _context.SingleRangeLinks
                    .Where(d => d.From == el.Id)
                    .Join(_context.Documents,
                      sr => sr.To,
                      d => d.Id,
                      (sr, d) => d)
                    .Union(_context.SingleRangeLinks
                      .Where(d => d.To == el.Id)
                      .Join(_context.Documents,
                        sr => sr.From,
                        d => d.Id,
                        (sr, d) => d))
                    .Select(d => new
                    {
                        number = d.Number,
                        name = d.DocumentType.Name(),
                        date = d.Date
                    })

                  : el.ParentDocument
                  .ChildeDocuments
                  .Where(c => c.Uuid != el.Uuid && (c.DocumentType == DocumentType.Decree || c.DocumentType == DocumentType.Law))
                  .Select(c => new
                  {
                      number = c.Number,
                      name = c.DocumentType.Name(),
                      date = c.Date

                  })
              })

              .ToArray();

            return Json(new { success = true, data = result });
        }

        [HttpGet]
        [AllowAnonymous]//window.location.protocol
        public async Task<IActionResult> Details([FromServices] IConfiguration configuration, Guid? id, string baseSiteName, string scheme)
        {
            var sitesForConclusion = configuration.GetSection("SitesForConclusion").Get<List<string>>();

            var forLanSite = !string.IsNullOrWhiteSpace(baseSiteName)
                                 && sitesForConclusion.Contains(baseSiteName);

            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents
              .OnlyForLanSite(forLanSite)
              .ByUuid(id.Value)
              .WithAllFields()
              .FirstOrDefaultAsync();

            if (document == null)
            {
                return NotFound();
            }

            if (document.DocumentType == DocumentType.Decree ||
                document.DocumentType == DocumentType.Law && !document.ParentUuid.HasValue)
            {
                var singleRangeDocs = await GetSingleRangeDocs(document);

                if (document.DocumentType == DocumentType.Decree)
                {
                    var law = singleRangeDocs
                      .ByDocumentType(DocumentType.Law)
                      .FirstOrDefault();

                    if (law != null)
                    {
                        var lawSingleRangeDocs = await GetSingleRangeDocs(law);

                        singleRangeDocs = singleRangeDocs
                          .Union(lawSingleRangeDocs)
                          .GroupBy(c => c.Id, (key, c) => c.First())
                          .OrderBy(el => el.Date)
                          .ToList();
                    }
                }

                document.SingleRangeDocuments = singleRangeDocs;
            }


            if (document.DocumentType == DocumentType.Decree && document.ParentDocument?.DocumentType == DocumentType.DecreeProject)
            {
                document.ParentDocument = await _context.Documents
                  .OnlyForLanSite(forLanSite)
                  .ByUuid(document.ParentDocument.Uuid)
                  .WithAllFields()
                  .FirstOrDefaultAsync();
            }

            if (document.DocumentType == DocumentType.LawProject &&
                document.ChildeDocuments.Any(el => el.DocumentType == DocumentType.Law))
            {
                var law = document.ChildeDocuments.First(el => el.DocumentType == DocumentType.Law);

                document.SubjectOfLaw = law.SubjectOfLaw ?? law.Correspondent;
            }

            if (document.DocumentType == DocumentType.Law && document.ParentDocument != null)
            {
                var subjectOfLaw = document.SubjectOfLaw ?? document.Correspondent;

                document = await _context.Documents.ByUuid(document.ParentDocument.Uuid)
                  .WithAllFields()
                  .FirstOrDefaultAsync();

                document.SubjectOfLaw = subjectOfLaw;
            }
            //document = document.DocumentType == DocumentType.Law && document.ParentDocument != null
            //  ? await _context.Documents.ByUuid(document.ParentDocument.Uuid).WithAllFields().FirstOrDefaultAsync()
            //  : document;

            if (document.DocumentType == DocumentType.Decree &&
                document.ParentDocument != null &&
                document.ParentDocument.DocumentType == DocumentType.LawProject)
            {
                var parentDoc = await _context.Documents
                  .OnlyForLanSite(forLanSite)
                  .WithAllFields()
                  .FirstOrDefaultAsync(el => el.Uuid == document.ParentUuid);

                if (parentDoc != null)
                {
                    var orderedDocs = parentDoc.ChildeDocuments
                      .OrderByDescending(el => el.Date);

                    var last2Docs = orderedDocs
                      .Take(2);

                    if (last2Docs.Any(el => el.Uuid == document.Uuid))
                    {
                        var law = last2Docs
                          .FirstOrDefault(el => el.DocumentType == DocumentType.Law);

                        if (law != null)
                        {
                            document.ChildeDocuments.Add(law);
                        }
                    }

                    if (document.Session == null)
                    {
                        var tmp = orderedDocs.SkipWhile(el => el.Uuid != document.Uuid).Skip(1).FirstOrDefault();

                        if (tmp.DocumentType == DocumentType.Letter1 || tmp.DocumentType == DocumentType.Letter2 ||
                            tmp.DocumentType == DocumentType.Letter3)
                        {
                            document.Session = tmp.Session;
                        }
                    }
                }

                if (string.IsNullOrWhiteSpace(document.ProfileCommittee))
                {
                    document.ProfileCommittee = parentDoc.ProfileCommittee;
                }

                if (string.IsNullOrWhiteSpace(document.Correspondent))
                {
                    document.Correspondent = parentDoc.Correspondent;
                }
            }

            var title = document.DocumentType == DocumentType.Decree
              ? $"{document.DocumentType.Name()} № {document.Number} от {document.Date.ToString("dd.MM.yyyy")} г."
              : $"{document.FinalDocumentType.SmallName()} № {document.FinalDocumentNumber} от {document.FinalDate.ToString("dd.MM.yyyy")} г.";



            Logger.Debug($"baseSiteName : {baseSiteName}; withConclusion : {forLanSite}");

            ExtractConclusion(document, forLanSite);

            string content;

            ViewData["baseSiteName"] = baseSiteName;

            ViewData["scheme"] = scheme?.Trim(':') ?? "https";

            switch (document.DocumentType)
            {
                case DocumentType.Law:
                    content = await this.RenderViewToStringAsync("/Areas/Users/Views/Shared/LargeCardLaw.cshtml", document, ViewData);
                    break;
                case DocumentType.Decree:
                    content = await this.RenderViewToStringAsync("/Areas/Users/Views/Shared/LargeCardDecree.cshtml", document, ViewData);
                    break;
                case DocumentType.LawProject:
                case DocumentType.DecreeProject:
                    content = await this.RenderViewToStringAsync("/Areas/Users/Views/Shared/LargeCardProject.cshtml", document, ViewData);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return Json(new { title, content });
        }

        private void ExtractConclusion(Document doc, bool withConclusion)
        {
            if (!withConclusion)
            {
                return;
            }

            var mainDoc = doc.DocumentType == DocumentType.DecreeProject || doc.DocumentType == DocumentType.LawProject
              ? doc
              : doc.ParentDocument;

            if (mainDoc?.ChildeDocuments == null
                || !mainDoc.ChildeDocuments.Any(el =>
                  el.DocumentType == DocumentType.Conclusion1 || el.DocumentType == DocumentType.Conclusion2))
            {
                return;
            }

            var conclusionDocs = mainDoc.ChildeDocuments
              .Where(el => el.DocumentType == DocumentType.Conclusion1 || el.DocumentType == DocumentType.Conclusion2);

            mainDoc.Conclusion = conclusionDocs
              .ToArray();
        }

        private async Task<List<Document>> GetSingleRangeDocs(Document document)
        {
            var byFrom = _context.Documents
              .ByUuid(document.Uuid)
              .Join(_context.SingleRangeLinks,
                d => d.Id,
                sr => sr.From,
                (d, sr) => new
                {
                    d,
                    refDoc = sr.To
                })
              .Join(_context.Documents.Displayed().WithAllFields(),
                dsr => dsr.refDoc,
                rd => rd.Id,
                (dsr, rd) => rd);

            var byTo = _context.Documents
              .ByUuid(document.Uuid)
              .Join(_context.SingleRangeLinks,
                d => d.Id,
                sr => sr.To,
                (d, sr) => new
                {
                    d,
                    refDoc = sr.From
                })
              .Join(_context.Documents.Displayed().WithAllFields(),
                dsr => dsr.refDoc,
                rd => rd.Id,
                (dsr, rd) => rd);


            var singleRangeDocs = await byFrom
              .Union(byTo)
              .Distinct()
              .OrderBy(el => el.Date)
              .ToListAsync();
            return singleRangeDocs;
        }

        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents
              .ByUuid(id.Value)
              .FirstOrDefaultAsync();

            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var document = await _context.Documents
              .ByUuid(id)
              .FirstOrDefaultAsync();

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> ChangePublishValue([FromServices] ElasticManager elasticManager, [FromBody] ChangePublishModel data)
        {
            try
            {
                if (!data.DocumentUuid.HasValue)
                {
                    return Json(new { success = false, documentUuid = "null" });
                }

                var document = await _context.Documents
                  .WithAllFields()
                  .ByUuid(data.DocumentUuid.Value)
                  .SingleAsync();

                if (document == null)
                {
                    return Json(new { success = false, data.DocumentUuid });
                }

                if (document.DisplayOnTheSite == data.Display)
                {
                    return Json(new { success = false, data.DocumentUuid });
                }

                if (document.ParentUuid.HasValue)
                {
                    document = await _context.Documents
                      .WithAllFields()
                      .ByUuid(document.ParentUuid.Value)
                      .SingleAsync();
                }

                document.DisplayOnTheSite = data.Display;

                if (document.ChildeDocuments != null && document.ChildeDocuments.Any())
                {
                    foreach (var childeDocument in document.ChildeDocuments)
                    {
                        childeDocument.DisplayOnTheSite = data.Display;
                    }
                }

                await _context.SaveChangesAsync();

                var _ = document.DisplayOnTheSite
                  ? await elasticManager.AddAsync(document, true)
                  : await elasticManager.DeleteAsync(document);

                return Json(new { success = true, data.DocumentUuid });
            }
            catch (Exception ex)
            {
                Logger.Error($"Ошибка установки признака отображения на сайте документа (Uuid : {data.DocumentUuid})", ex);

                return Json(new { success = false, data.DocumentUuid });
            }
        }

        public class ChangePublishModel
        {
            public Guid? DocumentUuid { get; set; }

            public bool Display { get; set; }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetSesions(long id)
        {
            var sesions = await _context.Sessions
              .ByConvocationId(id)
              .OrderByDescending(el => el.NumberInt)
              .Select(el => new
              {
                  id = el.Id,
                  name = el.NumberInt + " сессия"
              })
              .ToArrayAsync();

            return Json(new { success = true, data = sesions });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetConvocations()
        {
            var convocations = await _context.Convocations
              .OrderByDescending(el => el.Number)
              .Select(el => new
              {
                  id = el.Id,
                  name = el.Number + " созыв"
              })
              .ToListAsync();

            convocations.Insert(0, new
            {
                id = -1L,
                name = "Без созыва"
            });

            return Json(new { success = true, data = convocations });
        }
    }
}
