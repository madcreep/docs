using docs.Database;
using docs.Extensions;
using docs.Managers.ElasticSearch;
using docs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace docs.Areas.Management.Contrilers
{
  [Area("Management")]
  [Authorize(Roles = "admin")]
  public class IndexingController : Controller
  {
    private readonly DocumentsContext _context;

    private readonly ElasticManager _elasticManager;

    public IndexingController(DocumentsContext context, ElasticManager elasticManager)
    {
      _context = context;

      _elasticManager = elasticManager;
    }

    // GET: Indexing
    public ActionResult Index()
    {
      return View();
    }

    public async Task<ActionResult> GetIndexingSummary()
    {
      return Json(new { success = true, data = await CreateIndexingSummaryDataModelAsync() });
    }

    private async Task<IndexingSummaryDataModel> CreateIndexingSummaryDataModelAsync()
    {
      var displayed = _context.Documents
        .Independent()
        .Displayed();

      var countDocuments = await displayed
        .CountAsync();

      var countDocumentsNotIndexing = await displayed
        .NotIndexing()
        .CountAsync();

      //var files = _context.DocumentFiles
      //  .Where(el => (((el.Document.DocumentType == DocumentType.DecreeProject
      //                  || el.Document.DocumentType == DocumentType.LawProject)
      //                 && el.Document.DisplayOnTheSite)
      //                || (el.Document.ParentDocument != null && el.Document.ParentDocument.DisplayOnTheSite)));



      //var files1 = displayed
      //  .Where(el => el.DocumentType == DocumentType.DecreeProject || el.DocumentType == DocumentType.LawProject)
      //  .Select(el => el.Files);

      //var files2 = displayed
      //  .Where(el => el.DocumentType == DocumentType.DecreeProject || el.DocumentType == DocumentType.LawProject)
      //  .Select(el => el.ChildeDocuments.Select(x => x.Files));

      //var files3 = displayed
      //  .Where(el => el.DocumentType != DocumentType.DecreeProject && el.DocumentType != DocumentType.LawProject)
      //  .Select(el => el.Files);

      var files = _context.DocumentFiles
        .Where(el => el.Document.Indexing);
      
      var countFiles = await files
        .CountAsync();

      var countNotIndexingFiles = await files
        .NotIndexing()
        .CountAsync();

      var model = new IndexingSummaryDataModel
      {
        CountDocuments = countDocuments,
        NotIndexing = countDocumentsNotIndexing,
        CountFiles = countFiles,
        NotIndexingFiles = countNotIndexingFiles,
        IndexingInited = _elasticManager.Inited,
        ProcessReindexingPercent = _elasticManager.ReindexPercent
      };

      return model;
    }

    // GET: Indexing/Details/5
    [HttpPost]
    [Route("Management/Indexing/ReindexAll/{onlyNotIndexing}")]
    public ActionResult ReindexAll(bool onlyNotIndexing = false)
    {
      if (!_elasticManager.Inited || _elasticManager.ReindexPercent.HasValue)
      {
        return Json(new { success = false });
      }

      var _ = _elasticManager.ReindexingAll(onlyNotIndexing);

      return Json(new { success = true, percent = _elasticManager.ReindexPercent });
    }

    // GET: Indexing/Create
    public ActionResult GetReindexPercent()
    {
      return !_elasticManager.Inited
        ? Json(new { success = false })
        : Json(new { success = true, percent = _elasticManager.ReindexPercent });
    }

    public async Task<ActionResult> ReinitIndexing()
    {
      var result = await _elasticManager.ReinitAsync();

      return Json(new { success = result });
    }
  }
}
