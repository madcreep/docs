using docs.Database;
using docs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace docs.Areas.Management.Contrilers
{
  [Area("Management")]
  [Authorize(Roles = "admin")]
  public class UsersController : Controller
  {
    private readonly DocumentsContext _context;

    public UsersController(DocumentsContext context)
    {
      _context = context;
    }

    // GET: Users
    public async Task<ActionResult> Index()
    {
      var users = await (from r in _context.Roles.Where(el => el.Name == "user")
                   join ur in _context.UserRoles on r.Id equals ur.RoleId
                   join u in _context.Users on ur.UserId equals u.Id
                   select new UserModel
                   {
                     Name = u.FullName,
                     Email = u.Email,
                     Id = u.Id,
                     Phone = u.PhoneNumber
                   })
          .ToListAsync();

      return View(users);
    }

    // GET: Users/Details/5
    public ActionResult Details(int id)
    {
      return View();
    }

    // GET: Users/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Users/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Create([FromServices] UserManager<User> userManager, [FromForm] UserModel user)
    {
      if (!ModelState.IsValid)
      {
        return View(user);
      }

      try
      {
        var dbUser = new User
        {
          UserName = user.Email,
          Email = user.Email,
          FullName = user.Name
        };

        var result = await userManager.CreateAsync(dbUser, user.Password);

        if (!result.Succeeded)
        {
          return View(user);
        }

        result = await userManager.AddToRoleAsync(dbUser, "user");

        if (!result.Succeeded)
        {
          await userManager.DeleteAsync(dbUser);

          return View(user);
        }

        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return View(user);
      }
    }

    // GET: Users/Edit/5
    public async Task<ActionResult> Edit(Guid id)
    {
      var user = await _context.Users
        .Where(el => el.Id == id)
        .Select(el => new UserModel
        {
          Name = el.FullName,
          Email = el.Email,
          Id = el.Id,
          Phone = el.PhoneNumber
        })
        .FirstAsync();

      return View(user);
    }

    // POST: Users/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Edit(UserModel user)
    {
      if (!ModelState.IsValid)
      {
        return View(user);
      }

      try
      {
        var dbUser = await _context.Users
          .Where(el => el.Id == user.Id)
          .FirstAsync();

        dbUser.FullName = user.Name;

        if (dbUser.PhoneNumber != user.Phone)
        {
          dbUser.PhoneNumber = user.Phone;

          dbUser.PhoneNumberConfirmed = false;
        }

        if (dbUser.Email != user.Email)
        {
          dbUser.Email = user.Email;

          dbUser.EmailConfirmed = false;
        }

        await _context.SaveChangesAsync();

        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return View(user);
      }
    }

    // GET: Users/Delete/5
    [HttpGet]
    public async Task<ActionResult> Delete(Guid id)
    {
      var user = await _context.Users
        .Where(el => el.Id == id)
        .Select(el => new UserModel
        {
          Name = el.FullName,
          Email = el.Email,
          Id = el.Id,
          Phone = el.PhoneNumber
        })
        .FirstOrDefaultAsync();

      if (user == null)
      {
        return RedirectToAction(nameof(Index));
      }

      return View(user);
    }

    // POST: Users/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> DeleteConfirmed([FromServices] UserManager<User> userManager, [FromForm] Guid id)
    {
      try
      {
        var dbUser = await _context.Users
          .Where(el => el.Id == id)
          .FirstOrDefaultAsync();

        if (dbUser == null
            || await userManager.IsInRoleAsync(dbUser, "admin")
            || await userManager.IsInRoleAsync(dbUser, "service"))
        {
          return RedirectToAction(nameof(Index));
        }

        _context.Users
          .Remove(dbUser);

        await _context.SaveChangesAsync();

        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return RedirectToAction(nameof(Index));
      }
    }
  }
}
