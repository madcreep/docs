using docs.Database;
using docs.Extensions;
using docs.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace docs.Areas.Users.Controllers
{
  [Area("Users")]
  public class AntiCorruptionController : Controller
  {
    private readonly DocumentsContext _context;

    public AntiCorruptionController(DocumentsContext context)
    {
      _context = context;
    }

    public async Task<IActionResult> Index()
    {
      var now = DateTime.Today;

      //var model = await _context.Documents
      //  .WithAllFields()
      //  .Where(el => el.AnticorruptionExpertise && el.ExpertiseStart <= now && el.ExpertiseEnd >= now)
      //  .ToArrayAsync();

      //ViewBag.BaseSiteName = baseSiteParameters.BaseSiteName;

      return View();
    }

    public async Task<IActionResult> Documents([FromServices] IConfiguration configuration, [FromQuery] BaseSiteParameters baseSiteParameters)
    {
      var now = DateTime.Today;

      var sitesForConclusion = configuration.GetSection("SitesForConclusion").Get<List<string>>();

      var forLan = sitesForConclusion.Contains(baseSiteParameters.BaseSiteName);

      var model = await _context.Documents
        .OnlyForLanSite(forLan)
        .WithAllFields()
        .Where(el => (el.DocumentType == DocumentType.DecreeProject || el.DocumentType == DocumentType.LawProject) && el.AnticorruptionExpertise && el.ExpertiseStart <= now && el.ExpertiseEnd >= now)
        .ToArrayAsync();

      return Json(new
      {
        success = true,

        context = await this.RenderViewToStringAsync("/Areas/Users/Views/AntiCorruption/Documents.cshtml", model)
      });
    }
  }
}
