using docs.Controllers;
using docs.Database;
using docs.Extensions;
using docs.Helpers.Find;
using docs.Managers.ElasticSearch;
using docs.Models;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace docs.Areas.Users.Controllers
{
  [Area("Users")]
  public class LegislativeProcessController : Controller
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(ServiceController));

    private readonly DocumentsContext _context;

    public LegislativeProcessController(DocumentsContext context)
    {
      _context = context;
    }

    [HttpGet]
    public async Task<IActionResult> Index(Guid? id)
    {
      var displayedDocuments = _context.Documents
        .Displayed()
        .OnlyForLanSite(true);

      var subjectsOfLaw = displayedDocuments
        .Where(el => el.SubjectOfLaw != null)
        .Select(el => el.SubjectOfLaw);

      var correspondents = displayedDocuments
        .Where(el => el.SubjectOfLaw == null && el.Correspondent != null)
        .Select(el => el.Correspondent);

      ViewBag.SubjectsOfLaw = await subjectsOfLaw
        .Union(correspondents)
        .Distinct()
        .OrderBy(el=>el)
        .ToArrayAsync();
      //ViewBag.SubjectsOfLaw = await displayedDocuments
      //  .Where(el => el.Correspondent != null)
      //  .Select(el => el.Correspondent)
      //  .Distinct()
      //  .ToArrayAsync();

      ViewBag.PageSize = GlobalProperties.PageSize;

      ViewBag.StartYear = GlobalProperties.StartYear;

      ViewBag.ShowDocument = id.HasValue ? id.Value.ToString() : null;

      var today = DateTime.Today;

      var lastSession = await _context.Sessions
                          .Where(el =>
                            el.Date != null && el.Date >= today && _context.Documents.Any(x => x.SessionId == el.Id))
                          .OrderBy(el => el.Date)
                          .FirstOrDefaultAsync()
                        ?? await _context.Sessions
                          .Where(el => el.Date != null && el.Date < today)
                          .OrderByDescending(el => el.Date)
                          .FirstOrDefaultAsync();

      ViewBag.StartSessionId = lastSession?.Id;

      ViewBag.StartDate = lastSession?.Date;

      return View();
    }

    [Route("Document/{docNumber}")]
    public async Task<IActionResult> SingleDocument(string docNumber)
    {
      var docUuid = await _context.Documents
        .OnlyForLanSite(true)
        .Where(el => el.TechNumber == docNumber || el.Number == docNumber)
        .Select(el => el.Uuid)
        .FirstOrDefaultAsync();

      return RedirectToAction("Index", new { id = docUuid });
    }

    [HttpPost]
    public async Task<IActionResult> GetSessionsByYear([FromBody] GetSessionsByYearModel model)
    {
      if (!model.year.HasValue)
      {
        return Json(new { success = true, data = new string[0] });
      }

      var year = model.year ?? DateTime.Today.Year;

      var start = new DateTime(year, 1, 1);
      var end = new DateTime(year, 12, 31);

      var result = await _context.Sessions
        .Include(el => el.Convocation)
        .Where(el => el.Date >= start && el.Date <= end)
        .OrderByDescending(el => el.Convocation.Number)
        .ThenByDescending(el => el.NumberInt)
        .Select(el => new
        {
          id = el.Id,
          value = el.NumberInt + " " + el.SessionType
        })
        .ToArrayAsync();

      return Json(new { success = true, data = result });

    }

    public class TmpParams
    {
            public bool ForSession { get; set; }
            public string Scheme { get; set; }
    }

    [HttpPost]
    public async Task<IActionResult> Find(
      [FromServices] IConfiguration configuration,
      [FromServices] ElasticManager elasticManager,
      [FromForm] FindFormModel model,
      TmpParams param)
      //[FromQuery] bool forSession,
      //[FromQuery] string scheme)
        {
      var outModel = new FindResult
      {
        ForSession = param.ForSession
      };

      var sitesForConclusion = configuration.GetSection("SitesForConclusion").Get<List<string>>();

      var forLan = sitesForConclusion.Contains(model.BaseSiteName);

      model.Page = model.Page ?? 1;

      var findResult = await FindDocumentsHelper.FindAsync(_context, model, elasticManager, forLan);

      outModel.Documents = findResult.Documents;

      outModel.Count = findResult.Count;

      var session = model.SessionFind.HasValue
        ? await _context.Sessions
          .Where(el => el.Id == model.SessionFind.Value)
          .FirstOrDefaultAsync()
        : null;

      outModel.Title = param.ForSession
        ? null
        : session == null
          ? "Результат поиска"
          : $"{(session.SessionType.ToLower() == "очередная" ? "Очередное" : "Внеочередное")} {session.NumberInt} пленарное заседание";

      ViewData["scheme"] = param.Scheme?.Trim(':') ?? "https";

            return Json(new
      {
        count = findResult.Count,

        context = await this.RenderViewToStringAsync("/Areas/Users/Views/Shared/Cards.cshtml", outModel, ViewData)
      });
    }

    [HttpGet]
    public async Task<IActionResult> Find(Guid id, [FromQuery]string scheme)
    {
      var document = await _context.Documents
        .WithAllFields()
        .Displayed()
        .ByUuid(id)
        .FirstOrDefaultAsync();

      var result = new FindResult
      {
        ForSession = false,
        Documents = new[] { document },
        Count = document == null ? 0 : 1
      };

      ViewData["scheme"] = scheme?.Trim(':') ?? "https";

      return Json(new
      {
        count = document == null ? 0 : 1,
        context = await this.RenderViewToStringAsync("/Areas/Users/Views/Shared/Cards.cshtml", result, ViewData)
      });
    }

    public async Task<IActionResult> Details(Guid id)
    {
      var document = await _context.Documents
        .ByUuid(id)
        .WithAllFields()
        .FirstOrDefaultAsync();

      if (document.DocumentType == DocumentType.LawProject || document.DocumentType == DocumentType.DecreeProject)
      {
        return View(document);
      }

      if (document.DocumentType == DocumentType.Decree &&
          document.ParentDocument.DocumentType == DocumentType.LawProject)
      {
        var law = await _context.Documents
          .WithAllFields()
          .FirstOrDefaultAsync(el => el.ParentUuid == document.ParentUuid && el.DocumentType == DocumentType.Law);

        if (law == null)
        {
          document = null;
        }
        else
        {
          document.ChildeDocuments.Add(law);
        }
      }
      else
      {
        document = null;
      }

      return View(document);
    }
  }
}
