using docs.Database;
using docs.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Threading.Tasks;

namespace docs.Areas.Users.Controllers
{
    [Area("Users")]
    public class PdfViewerController : Controller
    {
        private readonly DocumentsContext _context;

        public PdfViewerController(DocumentsContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("Users/PdfViewer/Index/{docUuid}/{fileUuid}/{fileName}/{scheme}")]
        public async Task<IActionResult> Index(Guid docUuid, Guid fileUuid, string fileName, string scheme)
        {
            string outFileName;

            if (fileName == "CONTENT")
            {
                var document = await _context.Documents
                  .ByUuid(docUuid)
                  .SingleAsync();

                ViewBag.FileTitle = document
                  .Content;

                ViewBag.SubTitle = document
                  .DocumentType.SmallName();

                outFileName = (await _context.DocumentFiles
                  .SingleAsync(el => el.Uuid == fileUuid))
                  .Description;
            }
            else
            {
                var fileNameWithoutExtension = Path.GetExtension(fileName).ToLower() == ".pdf"
                  ? Path.GetFileNameWithoutExtension(fileName)
                  : fileName;

                ViewBag.FileTitle = fileNameWithoutExtension;

                outFileName = fileName;
            }

            var file = Url.Action("Get", "File", new { area = "Files", docUuid, fileUuid, fileName = outFileName },
                scheme?.Trim(':') ?? "https");
            //var file = Url.Action("Get", "File", new { area = "Files", docUuid, fileUuid, fileName = outFileName });

            //var file = $"/Files/File/Get/{docUuid}/{fileUuid}/{outFileName}";

            ViewBag.FileUrl = $"{Url.Content("~/viewer.html")}?file={file}";

            return View();
        }
    }
}
