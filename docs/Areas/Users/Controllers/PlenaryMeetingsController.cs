using System.Collections.Generic;
using docs.Database;
using docs.Extensions;
using docs.Helpers.Find;
using docs.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using docs.Controllers;
using log4net;
using Microsoft.Extensions.Configuration;

namespace docs.Areas.Users.Controllers
{
  [Area("Users")]
  public class PlenaryMeetingsController : Controller
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(ServiceController));

    private DocumentsContext _context;

    public PlenaryMeetingsController([FromServices] DocumentsContext context)
    {
      _context = context;
    }

    public async Task<IActionResult> Index()
    {
      var convocations = await _context.Convocations
        .OrderByDescending(el => el.Number)
        .Select(el => new
        {
          el.Id,
          el.Number,
          Name = el.Number + " созыв"
        })
        .ToArrayAsync();

      var first = convocations.FirstOrDefault();

      ViewBag.Convocations = new SelectList(convocations, "Id", "Name", first?.Id);

      ViewBag.ConvocationNumber = first?.Number;

      return View();
    }

    [HttpGet]
    public async Task<IActionResult> ConvocationsSessions([FromServices] IConfiguration configuration, long? id, string baseSiteName)
    {
      if (!id.HasValue)
      {
        return null;
      }

      var sitesForConclusion = configuration.GetSection("SitesForConclusion").Get<List<string>>();

      var forLan = sitesForConclusion.Contains(baseSiteName);

      var sessionIdsWithDocuments = _context.Documents
        .OnlyForLanSite(forLan)
        .ByDocumentType(DocumentType.Decree)
        .Displayed()
        .Where(el=>el.Session.Convocation.Id == id.Value)
        .GroupBy(el => el.Session.Id)
        .Select(el => el.Key);

      var sessions = await _context.Sessions
        .ByConvocationId(id.Value)
        .WithAllFields()
        .OrderByDescending(el => el.NumberInt)
        .Select(el => new SessionCardModel
        {
          Session = el,
          HasAgenda = el.SessionDocuments.Agenda != null,
          HasDocuments = sessionIdsWithDocuments.Contains(el.Id),
          HasAnnotation = el.SessionDocuments.Annotation != null
        })
        .ToArrayAsync();

        sessions = sessions
        .Where(el => el.HasAgenda || el.HasAnnotation || el.HasDocuments)
        .ToArray();

      return View("/Areas/Users/Views/PlenaryMeetings/Partial/SessionCards.cshtml", sessions);

    }

    [HttpGet]
    public async Task<IActionResult> SessionAgenda(long id)
    {
      var result = await _context.SessionDocuments
        .Include(el => el.Session)
        .ThenInclude(el => el.Convocation)
        .Where(el => el.SessionId == id && el.Agenda != null)
        .Select(el => new
        {
          SessionNumber = el.Session.NumberInt,
          el.Session.SessionType,
          ConvocationNumber = el.Session.Convocation.Number,
          el.Agenda
        })
        .FirstOrDefaultAsync();

      var title =
        $"Повестка {(result.SessionType.ToLower().StartsWith("внеочеред") ? "внеочередного" : "очередного")} {result.SessionNumber} пленарного заседания Законодательного Собрания Краснодарского края {result.ConvocationNumber} созыва";


      return Json(new { success = true, title, text = result.Agenda });
    }

    [HttpGet]
    public async Task<IActionResult> SessionAnnotation(long id)
    {
      var result = await _context.SessionDocuments
        .Include(el => el.Session)
        .ThenInclude(el => el.Convocation)
        .Where(el => el.SessionId == id && el.Annotation != null)
        .Select(el => new
        {
          SessionNumber = el.Session.NumberInt,
          ConvocationNumber = el.Session.Convocation.Number,
          el.Annotation
        })
        .FirstOrDefaultAsync();

      var title =
        $"Перечень правовых актов, принятых на {result.SessionNumber} сессии Законодательного Собрания Краснодарского края {result.ConvocationNumber} созыва, с краткой аннотацией";

      return Json(new { success = true, title, text = result.Annotation });
    }

    [HttpGet]
    public async Task<IActionResult> Find([FromServices] IConfiguration configuration, long id, string baseSiteName)
    {
      var outModel = new FindResult
      {
        ForSession = true
      };

      var model = new FindFormModel
      {
        SessionFind = id,
        NpaFind = DocumentType.Decree
      };

      var sitesForConclusion = configuration.GetSection("SitesForConclusion").Get<List<string>>();

      var forLan = sitesForConclusion.Contains(baseSiteName);

      var findResult = await FindDocumentsHelper.FindAsync(_context, model, null, forLan);

      model.NpaFind = DocumentType.Law;

      var findResult1 = await FindDocumentsHelper.FindAsync(_context, model, null, forLan);

      outModel.Documents = findResult.Documents
        .Union(findResult1.Documents)
        .OrderByDescending(el => el.Date);

      outModel.Count = findResult.Count + findResult1.Count;

      return Json(new
      {
        count = outModel.Documents.Count(),
        context = await this.RenderViewToStringAsync("/Areas/Users/Views/Shared/Cards.cshtml", outModel)
      });
    }
  }
}
