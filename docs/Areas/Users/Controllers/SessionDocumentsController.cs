using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using docs.Database;

namespace docs.Areas.Users.Controllers
{
    [Area("Users")]
    public class SessionDocumentsController : Controller
    {
        private readonly DocumentsContext _context;

        public SessionDocumentsController(DocumentsContext context)
        {
            _context = context;
        }

        // GET: Users/SessionDocuments
        public async Task<IActionResult> Index()
        {
            var documentsContext = _context.Documents;

            return View(await documentsContext.ToListAsync());
        }

        // GET: Users/SessionDocuments/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents
                .Include(d => d.ParentDocument)
                .Include(d => d.Reading)
                .Include(d => d.Result)
                .Include(d => d.Rubric)
                .Include(d => d.Session)
                .Include(d => d.Stage)
                .FirstOrDefaultAsync(m => m.Uuid == id);
            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }

        // GET: Users/SessionDocuments/Create
        public IActionResult Create()
        {
            ViewData["ParentUuid"] = new SelectList(_context.Documents, "Uuid", "Uuid");
            ViewData["ReadingId"] = new SelectList(_context.Readings, "Id", "Id");
            ViewData["ResultId"] = new SelectList(_context.Results, "Id", "Id");
            ViewData["RubricUuid"] = new SelectList(_context.Rubrics, "Uuid", "Uuid");
            ViewData["SessionId"] = new SelectList(_context.Sessions, "Id", "Id");
            ViewData["StageId"] = new SelectList(_context.Stages, "Id", "Id");
            return View();
        }

        // POST: Users/SessionDocuments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Uuid,ParentUuid,Id,ParentId,Order,DocumentType,IsProject,UploadDate,Date,Number,TechNumber,PublishDate,AnticorruptionExpertise,ExpertiseStart,ExpertiseEnd,Сorrespondent,ProfileCommittee,Convening,SessionNumber,SubjectOfLaw,Content,Group,HasApprovedAct,RubricUuid,SessionId,AgendaNumber,ReadingId,StageId,ResultId")] Document document)
        {
            if (ModelState.IsValid)
            {
                document.Uuid = Guid.NewGuid();
                _context.Add(document);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentUuid"] = new SelectList(_context.Documents, "Uuid", "Uuid", document.ParentUuid);
            ViewData["ReadingId"] = new SelectList(_context.Readings, "Id", "Id", document.ReadingId);
            ViewData["ResultId"] = new SelectList(_context.Results, "Id", "Id", document.ResultId);
            ViewData["RubricUuid"] = new SelectList(_context.Rubrics, "Uuid", "Uuid", document.RubricUuid);
            ViewData["SessionId"] = new SelectList(_context.Sessions, "Id", "Id", document.SessionId);
            ViewData["StageId"] = new SelectList(_context.Stages, "Id", "Id", document.StageId);
            return View(document);
        }

        // GET: Users/SessionDocuments/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents.FindAsync(id);
            if (document == null)
            {
                return NotFound();
            }
            ViewData["ParentUuid"] = new SelectList(_context.Documents, "Uuid", "Uuid", document.ParentUuid);
            ViewData["ReadingId"] = new SelectList(_context.Readings, "Id", "Id", document.ReadingId);
            ViewData["ResultId"] = new SelectList(_context.Results, "Id", "Id", document.ResultId);
            ViewData["RubricUuid"] = new SelectList(_context.Rubrics, "Uuid", "Uuid", document.RubricUuid);
            ViewData["SessionId"] = new SelectList(_context.Sessions, "Id", "Id", document.SessionId);
            ViewData["StageId"] = new SelectList(_context.Stages, "Id", "Id", document.StageId);
            return View(document);
        }

        // POST: Users/SessionDocuments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Uuid,ParentUuid,Id,ParentId,Order,DocumentType,IsProject,UploadDate,Date,Number,TechNumber,PublishDate,AnticorruptionExpertise,ExpertiseStart,ExpertiseEnd,Сorrespondent,ProfileCommittee,Convening,SessionNumber,SubjectOfLaw,Content,Group,HasApprovedAct,RubricUuid,SessionId,AgendaNumber,ReadingId,StageId,ResultId")] Document document)
        {
            if (id != document.Uuid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(document);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DocumentExists(document.Uuid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentUuid"] = new SelectList(_context.Documents, "Uuid", "Uuid", document.ParentUuid);
            ViewData["ReadingId"] = new SelectList(_context.Readings, "Id", "Id", document.ReadingId);
            ViewData["ResultId"] = new SelectList(_context.Results, "Id", "Id", document.ResultId);
            ViewData["RubricUuid"] = new SelectList(_context.Rubrics, "Uuid", "Uuid", document.RubricUuid);
            ViewData["SessionId"] = new SelectList(_context.Sessions, "Id", "Id", document.SessionId);
            ViewData["StageId"] = new SelectList(_context.Stages, "Id", "Id", document.StageId);
            return View(document);
        }

        // GET: Users/SessionDocuments/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents
                .Include(d => d.ParentDocument)
                .Include(d => d.Reading)
                .Include(d => d.Result)
                .Include(d => d.Rubric)
                .Include(d => d.Session)
                .Include(d => d.Stage)
                .FirstOrDefaultAsync(m => m.Uuid == id);
            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }

        // POST: Users/SessionDocuments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var document = await _context.Documents.FindAsync(id);
            _context.Documents.Remove(document);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DocumentExists(Guid id)
        {
            return _context.Documents.Any(e => e.Uuid == id);
        }
    }
}
