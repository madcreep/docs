import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FeatherModule } from 'angular-feather';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { DocumentsComponent } from './documents/documents.component';
import { DocumentCardComponent } from './documents/document.card.component';
import { IndexingComponent } from './indexing/indexing.component';
import { SummaryComponent } from './summary/summary.component';

import {
  Camera,
  Heart,
  Github,
  Home,
  File,
  ShoppingCart,
  Users,
  ChevronLeft,
  ChevronRight,
  UserCheck
} from 'angular-feather/icons';
import { BsModalRef, ModalModule, TabsModule } from 'ngx-bootstrap';
import { UserComponent } from "./user/user.component";
import { EmptyGuard } from "./empty.guard";
import { RolesComponent } from "./roles/roles.component";
import { RoleComponent } from "./role/role.component";

const icons = {
  Camera,
  Heart,
  Github,
  Home,
  File,
  ShoppingCart,
  Users,
  UserCheck,
  ChevronLeft,
  ChevronRight
};


function getBaseUrl(): string {
  return document.getElementsByTagName('base')[0].href;
}

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    SideMenuComponent,
    LoginComponent,
    RegisterComponent,
    UsersComponent,
    UserComponent,
    RolesComponent,
    RoleComponent,
    DocumentsComponent,
    DocumentCardComponent,
    IndexingComponent,
    SummaryComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    RouterModule.forRoot([
      //{ path: '', redirectTo: '/', pathMatch: 'full' },
      { path: '', component: SummaryComponent, pathMatch: 'full' },
      //{ path: 'logout', component: LoginComponent },
      { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
      { path: 'documents', component: DocumentsComponent, canActivate: [AuthGuard] },
      { path: 'indexing', component: IndexingComponent, canActivate: [AuthGuard] }
    ]),
    JwtModule.forRoot({

      config: {
        tokenGetter: function tokenGetter() {
          return localStorage.getItem('jwt');
        },
        whitelistedDomains: ["localhost"],
        blacklistedRoutes: [
          getBaseUrl() + 'api/auth/login',
          getBaseUrl() + 'api/auth/register'
        ]
      }
    }),
    FeatherModule.pick(icons)
  ],
  exports: [
    FeatherModule, LoginComponent
  ],
  providers: [AuthGuard, EmptyGuard, JwtHelperService, BsModalRef],
  bootstrap: [AppComponent],
  entryComponents: [LoginComponent, RegisterComponent, AppComponent, UserComponent, RoleComponent]
})
export class AppModule { }
