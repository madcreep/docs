import { Inject, Injectable } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subscription } from "rxjs";
import { User } from "./user";
import { UsersService } from "./users.service";
import { Router } from "@angular/router";
//import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInUser: User;

  constructor(private jwtHelper: JwtHelperService,
    private userService: UsersService,
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string) { }

  private userFromResponse(response) {
    const token = (<any>response).token;
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      this.login(token);
      const user = (<any>response).user;
      if (user) {
        this.loggedInUser = this.userService.elementToUser(user);
      }
    }
  }

  public login(token: string) {
    localStorage.setItem('jwt', token);
  }

  public isLoggedIn() {
    const token = localStorage.getItem('jwt');
    //console.log("TokenExpirationDate: " + this.jwtHelper.getTokenExpirationDate(token));
    return token !== null && !this.jwtHelper.isTokenExpired(token);
  }

  public logout(requestServer: boolean = true, success): Subscription {
    if (!requestServer) {
      localStorage.removeItem('jwt');
      this.loggedInUser = null;
      success();
      return new Subscription(() => { });
    }
    //return this.http.post(this.baseUrl + "api/auth/logoff", {}).pipe(tap(() => {
    //  localStorage.removeItem('jwt');
    //  this.loggedInUser = null;
    //})).toPromise();
    return this.http.post(this.baseUrl + "api/auth/logoff", {})
      .subscribe(response => {
        localStorage.removeItem('jwt');
        this.loggedInUser = null;
        success();
      }, err => {

      });
  }

  public registerUser(credentials: string, success, error): Subscription {
    return this.http.post<any>(this.baseUrl + "api/auth/register", credentials, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      this.userFromResponse(response);
      success();
    }, err => {
      error(err);
    });
  }

  public loginUser(credentials: string, success, error): Subscription {
    return this.http.post(this.baseUrl + "api/auth/login", credentials, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      this.userFromResponse(response);
      success();
    }, err => {
      error(err);
    });
  }

  public requestLoggedInUser() {
    this.http.get(this.baseUrl + "api/auth/getCurrentUser")
      .subscribe(response => {
        this.userFromResponse(response);
      }, err => {
        this.logout(false, () => {
        });
        //console.error(err);
      });
  }

  //public isAuthorized(roleNames: string): boolean {
  //  if (!this.loggedInUser || !this.isLoggedIn()) {
  //    return false;
  //  }
  //  const roles = roleNames.split(',')
  //    .map(e => e.trim());

  //  let found = false;

  //  for (let i = 0; i < roles.length; i++) {
  //    if (this.loggedInUser.userRoles.find(a => a === roles[i])) {
  //      found = true;
  //      break;
  //    }
  //  }
  //  return found;
  //}
}
