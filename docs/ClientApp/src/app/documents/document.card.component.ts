import { Component, Input } from '@angular/core';
import { formatDate } from '@angular/common';
import { DocumentsService, Document } from "./documents.service";

@Component({
  selector: 'app-document-card',
  templateUrl: './document.card.component.html',
  styleUrls: ['./document.card.component.css']
})
export class DocumentCardComponent {
  private subscriber: any;

  @Input() doc: Document;

  constructor(private docService: DocumentsService) { }

  sendToggle = false;

  onToggle() {
    this.sendToggle = true;

    this.subscriber = this.docService.changeDocPublish(this.doc.uuid, this.doc.displayOnTheSite)
      .subscribe((result: boolean) => {
        if (!result) {
          this.doc.displayOnTheSite = !this.doc.displayOnTheSite;
        }

        this.sendToggle = false;

        this.unsubscribe();
      });
  }

  private unsubscribe() {
    if (this.subscriber) {
      this.subscriber.unsubscribe();

      this.subscriber = null;
    }
  }
}
