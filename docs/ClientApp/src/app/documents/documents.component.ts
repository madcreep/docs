import { Component, OnInit, OnDestroy } from '@angular/core';
import { DocumentsService, Convocation, Session, Document } from "./documents.service";

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit, OnDestroy {

  private subscriber: any;

  convocations: Array<Convocation> = [];

  convocationId: string;

  sessions: Array<Session> = [];

  documents: Array<Document> = [];

  selectedSessionId: number;

  isConvDisabled: Boolean = true;

  constructor(private docService: DocumentsService) {}

  ngOnInit() {

    this.subscriber = this.docService.getConvocations()
      .subscribe((data: Array<Convocation>) => {
        if (data.length > 0) {
          this.convocationId = data[0].id.toString();
        }

        this.convocations = data;

        this.isConvDisabled = false;

        this.unsubscribe();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  onConvocationChange() {
    this.sessions = [];
    this.documents = [];
    this.isConvDisabled = true;
    if (this.convocationId == "-1") {
      this.sessionSelected(null, null);
    } else {
      this.subscriber = this.docService.getSessions(this.convocationId)
        .subscribe((data: Array<Session>) => {
          this.sessions = data;

          this.isConvDisabled = false;
          this.unsubscribe();
        });
    }
  }


  sessionSelected($event, sessionId) {

    this.documents = [];

    this.isConvDisabled = true;

    this.selectedSessionId = sessionId;

    this.subscriber = this.docService.getDocuments(sessionId)
      .subscribe((data: Array<Document>) => {

        this.documents = data;

        this.isConvDisabled = false;

        this.unsubscribe();
      });

    return false;
  }

  private unsubscribe() {
    if (this.subscriber) {
      this.subscriber.unsubscribe();

      this.subscriber = null;
    }
  }
}
