import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  constructor(private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string) { }

  getConvocations(): Observable<Array<Convocation>> {
    return this.http.get(this.baseUrl + "Management/Documents/GetConvocations", {})
      .pipe(
        map(
          (res) => {
            return res["data"] as Array<Convocation>;
          }
        )
      );
  }

  getSessions(id:string): Observable<Array<Session>> {
    return this.http.get(this.baseUrl + "Management/Documents/GetSesions",
      { params: new HttpParams().set('id', id)})
      .pipe(
        map(
          (res) => {
            return res["data"] as Array<Session>;
          }
        )
      );
  }

  getDocuments(sessionId: string): Observable<Array<Document>> {
    return this.http.get(this.baseUrl + "Management/Documents/GetDocumentsBySessionJson",
      { params: new HttpParams().set('sessionId', sessionId) })
      .pipe(
        map(
          (res) => {
            return res["data"] as Array<Document>;
          }
        )
      );
  }

  changeDocPublish(uuid: string, display: boolean): Observable<boolean> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      })
    };


    const params = {
      documentUuid: uuid,
      display: display
    }

    return this.http.post(this.baseUrl + "Management/Documents/ChangePublishValue", params)
      .pipe(
        map(
          (res) => {
            return res["success"] as boolean;
          }
        )
      );
  }
}

export class Convocation {
  id: number;
  name:string;
}

export class Session {
  id: number;
  name: string;
}

export class Document {
  uuid: string;
  displayOnTheSite: boolean;
  documentNumber: string;
  documentName: string;
  content: string;
  date: Date;
  anticorruptionExpertise: boolean;
  expertiseStart: Date;
  expertiseEnd: Date;
  publishDate: Date;
}
