import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval } from 'rxjs';
import { IndexingService, IndexingSummary } from "./indexing.service";

@Component({
  selector: 'app-indexing',
  templateUrl: './indexing.component.html',
  styleUrls: ['./indexing.component.css']
})
export class IndexingComponent implements OnInit {
  private subscriber: any;

  private tictSubscriber: any;

  private tictInterval: any;

  indexingSummary: IndexingSummary;

  processing = true;

  onlyNotIndexing = true;

  constructor(private indexingService: IndexingService) { }

  ngOnInit() {
    this.getIndexingSummary();
  }

  getIndexingSummary() {
    this.processing = true;

    this.subscriber = this.indexingService.getIndexigSummary()
      .subscribe((data: IndexingSummary) => {
        
        this.indexingSummary = data;

        this.unsubscribe();

        if (data.processReindexingPercent != null) {
          this.startTict();
        } else {
          this.processing = false;
        }
      });
  }

  reinitIndexing() {
    this.processing = true;

    this.subscriber = this.indexingService.reinitIndexig()
      .subscribe((data: boolean) => {

        this.indexingSummary.indexingInited = data;

        this.unsubscribe();

        this.processing = false;
      });
  }

  reindexAll() {
    this.processing = true;

    this.subscriber = this.indexingService.reindexAll(this.onlyNotIndexing)
      .subscribe((data: number) => {

        this.indexingSummary.processReindexingPercent = data;

        this.unsubscribe();

        if (data == null) {
          this.processing = false;
        } else {
          this.processing = true;
          this.startTict();
        }
      });
  }

  startTict() {
    this.tictInterval = interval(1000);
    this.tictSubscriber = this.tictInterval.subscribe(val => this.getReindexPercent());
  }

  getReindexPercent() {
    this.subscriber = this.indexingService.getReindexPercent()
      .subscribe((data: number) => {
        this.unsubscribe();

        if (data != null) {
          this.indexingSummary.processReindexingPercent = data;
          this.processing = true;
        } else {
          
          this.indexingSummary.processReindexingPercent = 100;

          if (this.tictSubscriber) {

            this.tictSubscriber.unsubscribe();

            this.tictSubscriber = null;
          }

          setTimeout(() => this.cancelReindexing(), 1000);
        }
      });
  }

  private cancelReindexing() {
    this.indexingSummary.processReindexingPercent = null;

    this.processing = false;
  }

  private unsubscribe() {
    if (this.subscriber) {
      this.subscriber.unsubscribe();

      this.subscriber = null;
    }
  }
}
