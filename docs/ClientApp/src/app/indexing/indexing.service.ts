import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IndexingService {
  constructor(private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string) { }

  getIndexigSummary(): Observable<IndexingSummary> {
    return this.http.get(this.baseUrl + "Management/Indexing/GetIndexingSummary")
      .pipe(
        map(
          (res) => {
            return res["data"] as IndexingSummary;
          }
        )
      );
  }

  reinitIndexig(): Observable<boolean> {
    return this.http.get(this.baseUrl + "Management/Indexing/ReinitIndexing")
      .pipe(
        map(
          (res) => {
            return res["success"] as boolean;
          }
        )
      );
  }

  reindexAll(onlyNotIndexing: boolean): Observable<number> {
    return this.http.post(this.baseUrl + "Management/Indexing/ReindexAll/" + onlyNotIndexing, null)
      .pipe(
        map(
          (res) => {
            return res["percent"] as number;
          }
        )
      );
  }

  getReindexPercent(): Observable<number> {
    return this.http.get(this.baseUrl + "Management/Indexing/GetReindexPercent")
      .pipe(
        map(
          (res) => {
            return res["percent"] as number;
          }
        )
      );

  }


}

export class IndexingSummary {
  countDocuments: number;

  notIndexing: number;

  countFiles: number;

  notIndexingFiles: number;

  processReindexingPercent: number;

  indexingInited: boolean;
}
