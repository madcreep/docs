import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from "../auth.service";
import {Subscription} from "rxjs";
import {BsModalRef} from "ngx-bootstrap";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  invalidLogin: boolean;
  url: string;
  loginFormGroup: FormGroup;
  private subscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private auth: AuthService,
    public activeModal: BsModalRef) {
    this.loginFormGroup = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }

  login() {
    let credentials = JSON.stringify({
      email: this.loginFormGroup.controls.email.value,
      password: this.loginFormGroup.controls.password.value
    });
    this.subscription = this.auth.loginUser(credentials, () => {
      this.invalidLogin = false;
      if (!this.url) {
        this.activeModal.hide();
      } else {
        if (this.url) {
          this.router.navigate([this.url]);
        } else {
          this.router.navigate(["/"]);
        }
      }
    }, err => {
      this.invalidLogin = true;
    });
  }

  isModal() {
    if (this.url) {
      return false;
    }
    return true;

  }

  logOut() {
    this.auth.logout(true, () => {
      this.router.navigate(["/"]);
    });
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.url = params.get('url');
    });
    if (this.router.url.startsWith('/logout')) {
      this.subscription = this.auth.logout(true, () => {
        this.router.navigate(["/"]);
      });
      //this.auth.logout(true)
      //  .then(() => {
      //    if (this.url) {
      //      this.router.navigate([this.url]);
      //    } else {
      //      this.router.navigate(["/"]);
      //    }
      //  })
      //  .catch(err => {
      //    console.error(err);
      //    if (this.url) {
      //      this.router.navigate([this.url]);
      //    }
      //  });
    }
  }
}

