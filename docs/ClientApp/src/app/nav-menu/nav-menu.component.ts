import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";
import { LoginComponent } from "../login/login.component";
import { ToggleService } from '../toggle.service';
import { BsModalService } from "ngx-bootstrap";
import { timer } from 'rxjs';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  show: boolean;
  toggled: boolean;
  timer;
  timerSubscribe;

  constructor(private auth: AuthService,
    private toggleService: ToggleService,
    private router: Router,
    private modalService: BsModalService) { }


  toggle() {
    this.toggled = !this.toggled;
    this.toggleService.changeToggled(this.toggled);
    const element = document.getElementById("wrapper");
    if (element) {
      if (this.toggled) {
        element.classList.add('toggled');
      } else {
        element.classList.remove('toggled');
      }
    }
  }

  getLoggedInUser() {
    return this.auth.loggedInUser;
  }

  loggedIn() {
    const isLoggedIn = this.auth.isLoggedIn();
    return isLoggedIn;
  }

  ngOnInit(): void {
    if (!this.auth.loggedInUser) {
      this.auth.requestLoggedInUser();
    }

    this.toggleService.currentMessage
      .subscribe(message => this.toggled = message);

    this.oberserableTimerStart();
  }

  ngOnDestroy(): void {
    if (this.timerSubscribe) {
      this.timerSubscribe.unsubscribe();

      this.timerSubscribe = null;
    }

    if (this.timer) {
      this.timer = null;
    }
  }

  login() {
    this.modalService
      .show(LoginComponent);
  }

  logout() {
    this.auth.logout(true, () => {
      this.router.navigate(["/"]);
    });
  }

  oberserableTimerStart() {
    this.timer = timer(100, 60000);
    this.timerSubscribe = this.timer.subscribe(val => {
      this.auth.requestLoggedInUser();
      if (!this.loggedIn()) {
        this.router.navigate(["/"]);
      }
    });
  }
}
