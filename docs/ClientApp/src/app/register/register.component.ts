import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {RegistrationValidator} from '../register.validator';
import {AuthService} from "../auth.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

  registrationFormGroup: FormGroup;
  passwordFormGroup: FormGroup;
  registrationErrors: object = {};
  subscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private auth: AuthService) {

    this.passwordFormGroup = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      repeatPassword: ['', Validators.required]
    }, {
      validator: RegistrationValidator.validate.bind(this)
    });
    this.registrationFormGroup = this.formBuilder.group({
      fullname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      passwordFormGroup: this.passwordFormGroup
    });
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }

  register() {
    const credentials = JSON.stringify({
      fullname: this.registrationFormGroup.controls.fullname.value,
      email: this.registrationFormGroup.controls.email.value,
      password: this.passwordFormGroup.controls.password.value
    });
    this.registrationErrors = {};
    this.subscription = this.auth.registerUser(credentials, () => {
      this.router.navigate(["/"]);
    }, err => {
      if (err.error && err.error.errors) {
        for (let prop in err.error.errors) {
          if (err.error.errors.hasOwnProperty(prop)) {
            this.registrationErrors[prop] = err.error.errors[prop];
          }
        }
      }
    });
  }

}

