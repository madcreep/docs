export class Role {
  id: string;
  roleName: string;
  description: string;
  specificRight: boolean;
}
