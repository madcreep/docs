import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Role} from "../role";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsersService} from "../users.service";
import {BsModalRef} from "ngx-bootstrap";

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit, OnDestroy {

  @Input() role: Role;
  @Output() action = new EventEmitter();

  // private subscription: Subscription;
  //@Input() id: string;
  error: boolean;
  errorText: string;

  roleFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private userService: UsersService,
              public activeModal: BsModalRef) {
    this.roleFormGroup = this.formBuilder.group({
      roleName: ['', Validators.required],
      description: [''],
      specificRight: ['']
    });
  }

  ngOnInit() {
    this.roleFormGroup.patchValue(this.role);
    if (this.role.roleName === 'admin' || this.role.roleName === 'user' || this.role.roleName === 'service') {
      this.roleFormGroup.controls.roleName.disable();
    }
  }

  ngOnDestroy(): void {
  }

  save() {
    const role : Role = Object.assign({},  this.roleFormGroup.value);
    role.id = this.role.id;
    //role.id = this.id;
    this.userService.saveRole(role)
      .then((newRole) => {
        this.action.emit(newRole);
        this.activeModal.hide();
      })
      .catch(err => {
        console.error(err);
        if (err.error.message) {
          this.errorText = err.error.message;
          if (err.error.errors) {
            for (let prop in err.error.errors) {
              if (err.error.errors.hasOwnProperty(prop)) {
                this.errorText += "\n" + err.error.errors[prop];
              }
            }
          }
        }
        this.error = true;
      });
  }

}
