import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Role} from "../role";
import {UsersService} from "../users.service";
import {RoleComponent} from "../role/role.component";
import {BsModalService, ModalOptions} from "ngx-bootstrap";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  roles: Role[];

  subscriptions: Subscription[] = [];

  constructor(
    private userService: UsersService,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef) { }

  ngOnInit() {
    this.getRoles();
  }

  getRoles() {
    this.userService.getAllRoles()
      .then(elements => {
        this.roles = elements;
      })
      .catch(err => console.error(err));
  }

  editRole(id) {
    this.userService.getRole(id)
      .then(role => {
        const options = new ModalOptions();
        options.initialState = {role: role};
        const modal = this.modalService.show(RoleComponent, options);
        this.modalService.onHidden.subscribe(() => this.changeDetection.markForCheck());
        this.subscriptions.push(
          this.modalService.onHidden.subscribe((reason: any) => {
            this.unsubscribe();
          })
        );
        modal.content.action.subscribe(role => {
          for (let i = 0; i < this.roles.length; i++) {
            if (this.roles[i].id === role.id) {
              this.roles[i] = role;
            }
          }
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      if (subscription != null && !subscription.closed)
        subscription.unsubscribe();
    });
    this.subscriptions = [];
  }


  deleteRole(id: string): void {
    this.userService.deleteRole(id)
      .then(() => {
        const element:Role = this.roles.find(r => r.id == id);
        if (element) {
          const idx = this.roles.indexOf(element);
          this.roles.splice(idx, 1);
        }
      })
      .catch(err => {
        console.error(err);
      });
  }
}
