import { Component, OnInit } from '@angular/core';
import { ToggleService } from '../toggle.service';
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  toggled: boolean;

  constructor(
    private toggleService: ToggleService,
    private auth: AuthService) { }

    ngOnInit() {
        this.toggleService.currentMessage.subscribe(message => this.toggled = message);
  }

}
