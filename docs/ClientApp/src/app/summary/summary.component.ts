import { Component, OnInit, OnDestroy } from '@angular/core';
import { SummaryService, Summary } from "./summary.service";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit, OnDestroy {
  summary: Summary;

  private subscriber: any;

  constructor(private summaryService: SummaryService) { }

  ngOnInit() {
    this.subscriber = this.summaryService.getSummary()
      .subscribe((data: Summary) => {
        this.summary = data;

        this.unsubscribe();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  private unsubscribe() {
    if (this.subscriber) {
      this.subscriber.unsubscribe();

      this.subscriber = null;
    }
  }
}


