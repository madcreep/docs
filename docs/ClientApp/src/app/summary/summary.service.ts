import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SummaryService {
  constructor(private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string) { }

  getSummary(): Observable<Summary> {
    return this.http.get(this.baseUrl + "api/Summary", {})
      .pipe(
        map(
          (res) => {
            return res["data"] as Summary;
          }
        )
      );
  }
}

export class Summary {
  elasticIsInit: boolean;
  documentsCount: number;
  notSentEmailCount: number;
}
