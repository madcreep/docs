import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToggleService {

  private toggled = new BehaviorSubject(false);
  currentMessage = this.toggled.asObservable();

  constructor() { }

  changeToggled(toggled: boolean) {
    this.toggled.next(toggled);
  }

}