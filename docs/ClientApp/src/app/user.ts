import {Role} from "./role";

export class User {
  id: string;
  email: string;
  fullName: string;
  password: string;
  userRoles: string[];
  isNotDeleted: boolean;
  onlyPasswordChange: boolean;
  allRoles: Role[];
}
