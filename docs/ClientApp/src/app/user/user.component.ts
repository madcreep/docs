import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, ValidatorFn} from "@angular/forms";
import {Subscription} from "rxjs";
import {User} from "../user";
import {UsersService} from "../users.service";
import {Role} from "../role";
import {BsModalRef} from "ngx-bootstrap";

const confirmPasswordValidator: ValidatorFn = (fg: FormGroup) => {
  const start = fg.get('password').value;
  const end = fg.get('confirmPassword').value;
  return start === null || start === "" || start === end
    ? null
    : { range: false };
};

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  @Input() user: User;
  @Output() action = new EventEmitter();

  roles: Role[];
  private subscription: Subscription;
  error: boolean;

  userFormGroup: FormGroup;
  rolesFormGroup: FormGroup;


  constructor(
    private formBuilder: FormBuilder,
    private userService: UsersService,
    public bsModalRef: BsModalRef) {

    this.rolesFormGroup = this.formBuilder.group({});
    this.userFormGroup = this.formBuilder.group({
      email: ['', Validators.required],
      fullName: ['', Validators.required],
      password: ['', [Validators.minLength(6)]],
      confirmPassword: ['', [Validators.minLength(6)]],
      rolesFormGroup: this.rolesFormGroup
    }, { validator: confirmPasswordValidator });
  }
  //
  // public fetchUser() {
  //   if (this.id) {
  //     this.userService.getUser(this.id)
  //       .then(user => {
  //         this.user = user;
  //         this.roles = user.allRoles;
  //         for (let i = 0;i < this.roles.length; i++) {
  //           this.rolesFormGroup.addControl(this.roles[i].id, new FormControl());
  //         }
  //         this.userFormGroup.patchValue(user);
  //       })
  //       .catch(err => {
  //         console.error(err);
  //         this.error = true;
  //       });
  //   } else {
  //     this.userService.getAllRoles()
  //       .then(roles => {
  //         this.roles = roles;
  //       })
  //       .catch(err => {
  //         console.error(err);
  //       })
  //   }
  // }

  ngOnInit() {
    this.roles = this.user.allRoles;
    for (let i = 0; i < this.user.allRoles.length; i++) {
      this.rolesFormGroup.addControl(this.user.allRoles[i].roleName, new FormControl());
      const value = this.user.userRoles.includes(this.user.allRoles[i].roleName);
      this.rolesFormGroup.controls[this.user.allRoles[i].roleName].setValue(value);
    }
    this.userFormGroup.patchValue(this.user);
    // this.user = {id: "", email: "", fullName: "", password: "", userRoles: [], allRoles: []};
    // this.subscription = this.route.paramMap.subscribe(params => {
    //   if (params['id']) {
    //     this.id = params['id'];
    //     this.fetchUser();
    //   }
    // });
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }

  save() {
    const user : User = Object.assign({},  this.userFormGroup.value);
    user.id = this.user.id;
    const vals = this.rolesFormGroup.value;
    user.userRoles = [];
    for (let prop in vals) {
      if (vals.hasOwnProperty(prop) && vals[prop]) {
        user.userRoles.push(prop)
      }
    }
    //user.id = this.id;
    this.userService.saveUser(user)
      .then(user => {
        this.action.emit(user);
        this.bsModalRef.hide();
      })
      .catch(err => {
        console.error(err);
        this.error = true;
      });
  }

}
