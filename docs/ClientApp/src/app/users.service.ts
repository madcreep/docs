import { Inject, Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { catchError, map } from "rxjs/operators";
import { User } from "./user";
import { throwError as observableThrowError } from "rxjs";
import { RestError } from "./rest-error";
import { Role } from "./role";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  private httpErrorHandler(error) {
    if (error instanceof RestError) {
      return observableThrowError(error);
    } else {
      return observableThrowError(new RestError({ http: error }));
    }
  }

  elementToUser(element: any): User {
    const user = {
      id: element['id'] === undefined ? null : element['id'],
      password: null,
      email: element['email'],
      fullName: element['fullName'],
      isNotDeleted: element['isNotDeleted'],
      onlyPasswordChange: element['onlyPasswordChange'],
      userRoles: [],
      allRoles: []
    };
    if (element.userRoles) {
      for (let i = 0; i < element.userRoles.length; i++) {
        user.userRoles.push(element.userRoles[i]);
      }
    }
    if (element.allRoles) {
      for (let i = 0; i < element.allRoles.length; i++) {
        user.allRoles.push(this.elementToRole(element.allRoles[i]));
      }
    }
    return user;
  };

  elementToRole(element: any): Role {
    return {
      id: element['id'] === undefined ? null : element['id'],
      roleName: element['roleName'],
      description: element['description'],
      specificRight: element['specificRight']
    };
  };

  getAllUsers(): Promise<User[]> {
    return this.http
      .get<any>(this.baseUrl + "api/user/list")
      .pipe(
        map(response => response.map(
          element => this.elementToUser(element))
        ),
        catchError(this.httpErrorHandler))
      .toPromise<User[]>();
  }

  getUser(id: string): Promise<User> {
    return this.http
      .get<any>(this.baseUrl + "api/user/get/" + id)
      .pipe(
        map(response => this.elementToUser(response),
          catchError(this.httpErrorHandler))
      ).toPromise<User>();
  }

  saveUser(user: User): Promise<User> {
    return this.http
      .post(this.baseUrl + 'api/user/save', user)
      .pipe(
        map(response => this.elementToUser(response),
          catchError(this.httpErrorHandler))
      ).toPromise<User>();
  }

  getAllRoles(): Promise<Role[]> {
    return this.http
      .get<any>(this.baseUrl + "api/role/list")
      .pipe(
        map(response => response.map(
          element => this.elementToRole(element))
        ),
        catchError(this.httpErrorHandler))
      .toPromise<Role[]>();
  }

  getRole(id: string): Promise<Role> {
    return this.http
      .get<any>(this.baseUrl + "api/role/get/" + id)
      .pipe(
        map(response => this.elementToRole(response),
          catchError(this.httpErrorHandler))
      ).toPromise<Role>();
  }

  saveRole(role: Role): Promise<Role> {
    return this.http
      .post(this.baseUrl + 'api/role/save', role)
      .pipe(
        map(response => this.elementToRole(response),
          catchError(this.httpErrorHandler))
      ).toPromise<Role>();
  }

  deleteRole(id: String): Promise<any> {
    return this.http
      .delete(this.baseUrl + 'api/role/delete/' + id).toPromise<any>();
  }
  deleteUser(id: String): Promise<any> {
    return this.http
      .delete(this.baseUrl + 'api/user/delete/' + id).toPromise<any>();
  }

}
