import {ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import { User } from "../user";
import { UsersService } from "../users.service";
import {UserComponent} from "../user/user.component";
import {BsModalService, BsModalRef, ModalOptions} from "ngx-bootstrap";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {

  users: User[];
  private modalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private userService: UsersService,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
      this.userService.getAllUsers()
        .then(elements => {
          this.users = elements;
        })
        .catch(err => console.error(err));
  }

  editUser(id: string) {
    this.userService.getUser(id)
      .then(user => {
        const options = new ModalOptions();
        options.initialState = {user: user};
        this.modalRef = this.modalService.show(UserComponent, options);
        this.modalService.onHidden.subscribe(() => this.changeDetection.markForCheck());
        this.subscriptions.push(
          this.modalService.onHidden.subscribe((reason: any) => {
            this.unsubscribe();
          })
        );
        this.modalRef.content.action.subscribe(user => {
          for (let i = 0; i < this.users.length; i++) {
            if (this.users[i].id === user.id) {
              this.users[i] = user;
            }
          }
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      if (subscription != null && !subscription.closed)
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id)
      .then(() => {
        const element:User = this.users.find(r => r.id == id);
        if (element) {
          const idx = this.users.indexOf(element);
          this.users.splice(idx, 1);
        }
      })
      .catch(err => {
        console.error(err);
      });
  }

}
