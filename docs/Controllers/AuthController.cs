using System;
using docs.Database;
using docs.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using docs.Managers;
using log4net;

namespace docs.Controllers
{
  [Route("api/auth")]
  [ApiController]
  public class AuthController : ControllerBase
  {
    private ILog _logger = LogManager.GetLogger(typeof(AuthController));

    private readonly UserManager<User> _userManager;
    private readonly RoleManager<Role> _roleManager;
    private readonly SignInManager<User> _signInManager;

    public AuthController(UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
    {
      _userManager = userManager;
      _roleManager = roleManager;
      _signInManager = signInManager;
    }

    [HttpPost, Route("login")]
    [AllowAnonymous]
    public async Task<IActionResult> Login([FromBody]UserForm user)
    {
      try
      {
        _logger.Debug($"Login ");

        if (user == null)
        {
          _logger.Debug($"Login => Invalid client request");

          return BadRequest("Invalid client request");
        }

        _logger.Debug($"Login => Email: {user.Email}; Password: {user.Password}");

        var dbUser = await _userManager.FindByEmailAsync(user.Email);

        if (dbUser == null)
        {
          _logger.Warn($"Login => Пользователь не найден (Email: {user.Email}; Password: {user.Password})");

          return BadRequest(new {Message = "Неверный логин или пароль" });
        }

        var valid = await _userManager.CheckPasswordAsync(dbUser, user.Password);
        if (!valid)
        {
          _logger.Warn($"Login => Неверный пароль (Email: {user.Email}; Password: {user.Password})");

          return BadRequest(new {Message = "Неверный логин или пароль"});
        }

        LoginObject tokenObject;
        try
        {
          tokenObject = await ProceedWithLogin(user, dbUser);
        }
        catch (UserException ex)
        {
          _logger.Error($"ОШИБКА Login => Получение токена (Email: {user.Email}; Password: {user.Password})", ex);

          return Unauthorized(new {ex.Message});
        }

        return Ok(tokenObject);
      }
      catch (Exception ex)
      {
        _logger.Error($"ОШИБКА Login => Email: {user.Email}; Password: {user.Password}", ex);

        throw;
      }
    }

    private async Task<LoginObject> ProceedWithLogin(UserForm user, User dbUser)
    {
      var principal = await _signInManager.CreateUserPrincipalAsync(dbUser);
      var identity = principal.Identity as ClaimsIdentity;
      var userForm = await AuthService.UserToUserForm(dbUser, _userManager, _roleManager);
      if (identity == null)
        throw new UserException("Вход не выполнен");
      identity.AddClaims(AuthService.GetClaims(userForm));
      var tokenObject = await LoginUser(dbUser);

      await _signInManager.Context.SignInAsync(IdentityConstants.ApplicationScheme, principal,
          new AuthenticationProperties
          {
            IsPersistent = user.RememberMe
          });
      return tokenObject;
    }

    [HttpPost, Route("register")]
    [AllowAnonymous]
    public async Task<IActionResult> Register([FromBody]UserForm user)
    {
      if (user == null)
      {
        return BadRequest("Invalid client request");
      }

      var dbUser = new User
      {
        UserName = user.Email,
        Email = user.Email,
        FullName = user.FullName
      };

      var result = await _userManager.CreateAsync(dbUser, user.Password);

      if (!result.Succeeded)
      {
        return result.HandleErrors(Unauthorized, "Регистрация пользователя не выполнена");
      }

      var adminRegistered = false;

      foreach (var curUser in _userManager.Users)
      {
        var userRoles = await _userManager.GetRolesAsync(curUser);
        if (userRoles.All(r => r != "admin")) continue;
        adminRegistered = true;
        break;
      }
      if (!adminRegistered)
      {
        dbUser = await _userManager.FindByEmailAsync(user.Email);
        await _userManager.AddToRoleAsync(dbUser, "admin");
      }

      LoginObject tokenObject;
      try
      {
        tokenObject = await ProceedWithLogin(user, dbUser);
      }
      catch (UserException ex)
      {
        return Unauthorized(new { ex.Message });
      }

      return Ok(tokenObject);
    }

    [HttpGet, HttpPost]
    //        [ValidateAntiForgeryToken]
    [Route("logoff")]
    [Authorize]
    public async Task<IActionResult> LogOff()
    {
      try
      {
        await _signInManager.SignOutAsync();

        return Ok();
      }
      catch
      {
        return StatusCode(500);
      }
    }

    [HttpGet, Authorize]
    [Route("getCurrentUser")]
    [AllowAnonymous]
    public async Task<IActionResult> GetCurrentUser()
    {
      if (!HttpContext.User.Identity.IsAuthenticated)
      {
        return Unauthorized(new {Message = "Вход не выполнен"});
      }

      try
      {
        var user = await AuthService.GetCurrentUser(HttpContext, _userManager);

        var token = await LoginUser(user);

        return Ok(token);
      }
      catch (Exception ex)
      {
        _logger.Error("Ошибка проверки пользователя", ex);

        return Unauthorized(new { Exception = "Ошибка проверки пользователя" });
      }
    }


    private async Task<LoginObject> LoginUser(User user)
    {
      var userForm = await AuthService.UserToUserForm(user, _userManager, _roleManager);

      var token = AuthService.GetSecurityToken(userForm);

      HttpContext.Session.SetString("JWToken", token);

      return new LoginObject { Token = token, User = userForm };
    }
  }

  internal class LoginObject
  {
    public string Token { get; set; }
    public UserForm User { get; set; }
  }
}
