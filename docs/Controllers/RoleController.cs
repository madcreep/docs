using System;
using System.Linq;
using System.Threading.Tasks;
using docs.Database;
using docs.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace docs.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/role")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly RoleManager<Role> _roleManager;

        public RoleController(RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }
        
        [Route("list")]
        public IActionResult List() =>
            Ok(_roleManager.Roles.Select(r => AuthService.RoleToRoleForm(r)).ToList());


        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            var result = await _roleManager.CreateAsync(new Role {Name = name});
            if (!result.Succeeded) return result.HandleErrors(BadRequest, "Не удалось создать роль");
            var role = await _roleManager.FindByNameAsync(name);
            return Ok(AuthService.RoleToRoleForm(role));
            
        }
        
        [HttpGet]
        [Route("get/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            return Ok(AuthService.RoleToRoleForm(role));
        }


        [Route("save")]
        [HttpPost]
        public async Task<IActionResult> Save(RoleForm model)
        {
            Role role;
            if (model.Id == Guid.Empty)
            {
                var result = await _roleManager.CreateAsync(new Role {Name = model.RoleName, Description = model.Description});
                if (!result.Succeeded) return result.HandleErrors(BadRequest, "Не удалось создать роль");
            }
            else
            {
                role = await _roleManager.FindByIdAsync(model.Id.ToString());
                if (role == null)
                {
                    return NotFound(new {Message = "Роль не найдена"});
                }

                if (role.Name != "admin" && role.Name != "user" && role.Name != "service")
                {
                    role.Name = model.RoleName;
                }
                else
                {
                    model.RoleName = role.Name;
                }

                role.Description = model.Description;
                role.SpecificRight = model.SpecificRight;

                var result = await _roleManager.UpdateAsync(role);
                if (!result.Succeeded) return result.HandleErrors(BadRequest, "Ошибка при обновлении роли");
            }
            role = await _roleManager.FindByNameAsync(model.RoleName);
            return Ok(AuthService.RoleToRoleForm(role));

        }
         
        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);
            if (role == null) return NotFound(new {Message = "Не найдена роль"});
            if (role.Name == "admin" || role.Name == "user" || role.Name == "service")
            {
                return BadRequest(new {Message = "Нельзя удалить предопределенную роль"});
            }
            var result = await _roleManager.DeleteAsync(role);
            if (result.Succeeded) return Ok();
            return result.HandleErrors(BadRequest, "Ошибка при удалении роли");

        }
    }
}