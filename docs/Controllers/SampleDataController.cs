using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace docs.Controllers
{
  [ApiController]
  [Route("api/test")]
  public class SampleDataController : ControllerBase
  {
    private static string[] Summaries = new[]
    {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

    [HttpGet("[action]"), Authorize(Roles = "admin, user", Policy = "SpecificRight")]
    public IEnumerable<WeatherForecast> WeatherForecasts()
    {
      var rng = new Random();
      return Enumerable.Range(1, 5).Select(index => new WeatherForecast
      {
        DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
        TemperatureC = rng.Next(-20, 55),
        Summary = Summaries[rng.Next(Summaries.Length)]
      });
    }

    [HttpGet, AllowAnonymous]
    public IEnumerable<string> Test()
    {
      return new[] { "Du ", " du hast", "du hast mich" };
    }

    [Route("test1")]
    [HttpGet, AllowAnonymous]
    public string Test1()
    {
      return "Успех";// new[] { "Du ", " du hast", "du hast mich" };
    }

    public class WeatherForecast
    {
      public string DateFormatted { get; set; }
      public int TemperatureC { get; set; }
      public string Summary { get; set; }

      public int TemperatureF
      {
        get
        {
          return 32 + (int)(TemperatureC / 0.5556);
        }
      }
    }
  }
}
