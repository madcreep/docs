using docs.Database;
using docs.Database.Helpers;
using docs.Extensions;
using docs.Managers;
using docs.Managers.ElasticSearch;
using docs.Model;
using docs.Models;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace docs.Controllers
{
  [ApiController, Route("api/service")]
  [Authorize(Roles = "service")]
  public class ServiceController : ControllerBase
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(ServiceController));

    private readonly DocumentsContext _context;

    private readonly ElasticManager _elasticManager;

    public ServiceController([FromServices] DocumentsContext context, [FromServices] ElasticManager manager)
    {
      Logger.Debug($"Старт контроллера => {nameof(ServiceController)}");

      _context = context;

      _elasticManager = manager;
    }

    [HttpPost, Route("sendDocument")]
    public async Task<IActionResult> PostDocument([FromBody] RcProject model)
    {
      Logger.Debug($"Старт метода {nameof(PostDocument)} (model Isn : {model?.Isn})");

      var result = new PostDocumentAnswer();

      try
      {
        if (model == null)
        {
          Logger.Error($"В методе {nameof(PostDocument)} получена пустая модель.");

          return BadRequest();
        }

        switch (model.RcType)
        {
          case DocumentType.Undefined:
            {
              var deleteHelper = new DeleteHelper(_context);

              result = await deleteHelper.Delete(model);

              return Ok(result);
            }
          case DocumentType.PlenarySession:
            {
              if (await SessionHelper.AddSession(model, _context))
              {
                GC.Collect();

                return Ok(result);
              }

              break;
            }
          case DocumentType.LawProject:
          case DocumentType.DecreeProject:
          case DocumentType.Letter1:
          case DocumentType.Letter2:
          case DocumentType.Letter3:
          case DocumentType.Conclusion1:
          case DocumentType.Conclusion2:
          case DocumentType.Law:
          case DocumentType.Decree:

            var document = await DocumentHelper.GenerateDocument(model, _context, result);

            if (document == null)
            {
              return Ok(result);
            }

            if (document.DocumentType == DocumentType.DecreeProject
                    || document.DocumentType == DocumentType.LawProject
                    || document.DocumentType == DocumentType.Law
                    || document.DocumentType == DocumentType.Decree
                )
            {
              if (document.ParentDocument != null
                  && (document.DocumentType == DocumentType.Law
                      || (document.DocumentType == DocumentType.Decree &&
                          document.ParentDocument.DocumentType == DocumentType.DecreeProject)))
              {
                await _elasticManager.DeleteAsync(document.ParentDocument);
              }

              await _elasticManager.AddAsync(document);

              GC.Collect();
            }

            return Ok(result);
        }

        Logger.Error($"Документ для РК (Isn : {model.Isn}) не создан");

        GC.Collect();

        result.ErrorMsg = "Документ не создан";

        return Ok(result);
      }
      catch (Exception ex)
      {
        Logger.Error($"Ошибка добавления документа (Isn : {model?.Isn}; Type : {model?.RcType})", ex);

        GC.Collect();

        return StatusCode((int)HttpStatusCode.InternalServerError);
      }
    }

    [HttpPost, Route("sendFile/{id}")]
    [RequestSizeLimit(1073741824)]
    [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
    public async Task<IActionResult> UploadFile(int id, [FromForm] FInfo fInfo)
    {
      try
      {
        Logger.Debug($"Старт метода {nameof(UploadFile)} (fInfo Id : {fInfo?.FileId}; fileSize : {fInfo?.FileSize})");

        if (fInfo?.FileId == null || !fInfo.FileSize.HasValue)
        {
          GC.Collect();

          return BadRequest();
        }

        if (Request.Form.Files == null || Request.Form.Files.Count != 1)
        {
          GC.Collect();

          return BadRequest();
        }

        foreach (var formFile in Request.Form.Files)
        {
          if (formFile.Length != fInfo.FileSize)
          {
            GC.Collect();

            return BadRequest();
          }

          var doc = await _context.Documents
            .OnlyForLanSite(true)
            .Include(el => el.Files)
            .ById(id)
            .FirstOrDefaultAsync();

          if (doc != null)
          {

            var docFile = doc.Files
              .FirstOrDefault(el => el.Id == fInfo.FileId.Value && el.FileName == formFile.FileName);

            if (docFile == null)
            {
              GC.Collect();

              return NotFound();
            }

            var fi = FileManager.GetFilePathInfo(docFile);

            if (fi.Directory != null && !fi.Directory.Exists)
            {
              fi.Directory.Create();
            }

            using (var stream = new FileStream(fi.FullName, FileMode.Create))
            {
              await formFile.CopyToAsync(stream);
            }

            docFile.Send = true;

            docFile.Size = formFile.Length;

            await _context.SaveChangesAsync();

            var _ = await _elasticManager.AddAsync(docFile);
          }
          else
          {
            if (Path.GetExtension(formFile.FileName) == ".txt")
            {
              var sessionDocuments = await _context.SessionDocuments
                .FirstOrDefaultAsync(el =>
                  el.AgendaIsn == fInfo.FileId.Value || el.AnnotationIsn == fInfo.FileId.Value);

              if (sessionDocuments != null)
              {
                byte[] bytes;

                using (var stream = formFile.OpenReadStream())
                {
                  bytes = new byte[formFile.Length];

                  stream.Read(bytes, 0, bytes.Length);
                }

                var encodedStr = CodePagesEncodingProvider.Instance.GetEncoding(1251).GetString(bytes);

                if (fInfo.FileId.Value == sessionDocuments.AgendaIsn)
                {
                  sessionDocuments.Agenda = encodedStr;
                }
                else
                {
                  sessionDocuments.Annotation = encodedStr;
                }

                await _context.SaveChangesAsync();
              }
            }
          }
        }

        GC.Collect();

        return Ok();
      }
      catch (Exception ex)
      {
        Logger.Error($"Ошибка в методе {nameof(UploadFile)}", ex);

        GC.Collect();

        return BadRequest();
      }
    }

    public class FInfo
    {
      public int? FileId { get; set; }
      public long? FileSize { get; set; }
    }


    //[HttpPost, Route("sendFile1/{id}")]
    //public async Task<IActionResult> SendFiles1([FromBody] DocumentFileModel[] model, [FromQuery] int id)
    //{
    //  if (model == null) return BadRequest(new { Message = "Required request object is missing" });
    //  if (id == 0) return BadRequest(new { Message = "Document UUID is missing" });
    //  using (var context = new DocumentsContext())
    //  {
    //    var document = await context.Documents.FirstOrDefaultAsync(d => d.Id == id);
    //    if (document == null)
    //    {
    //      return NotFound(new { Message = $"Document with id {id} not found" });
    //    }

    //    var localPath = Path.Combine(GlobalProperties.StoragePath, document.Uuid.ToString());

    //    if (!Directory.Exists(localPath))
    //    {
    //      Directory.CreateDirectory(localPath);
    //    }

    //    if (Request.Form.Files != null && Request.Form.Files.Count > 0)
    //    {
    //      foreach (var formFile in Request.Form.Files)
    //      {
    //        var fileModel = model.FirstOrDefault(m => m.FileName == formFile.FileName);

    //        if (fileModel == null) continue;
    //        var file = fileModel.ToDocumentFile(null);
    //        var dbFile = document.Files.FirstOrDefault(f => f.Id == file.Id);
    //        Guid fileId;
    //        if (dbFile == null)
    //        {
    //          file.Document = document;
    //          context.DocumentFiles.Add(file);
    //          document.Files.Add(file);
    //          fileId = new Guid();
    //          file.Uuid = fileId;
    //        }
    //        else
    //        {
    //          fileId = dbFile.Uuid;
    //        }

    //        var filePath = Path.Combine(localPath, fileId.ToString());
    //        using (var stream = new FileStream(filePath, FileMode.Create))
    //        {
    //          await formFile.CopyToAsync(stream);
    //        }

    //      }

    //      await context.SaveChangesAsync();
    //    }
    //  }

    //  return Ok();
    //}

    [HttpPost, Route("sendStage/{id}")]
    public async Task<IActionResult> SendStage([FromBody] ReviewStageModel model, int id)
    {
      if (id == 0)
      {
        GC.Collect();

        return BadRequest(new { Message = "Missing required id parameter" });
      }

      ReviewStageModel result;
      using (var context = new DocumentsContext())
      {
        var document = await context.Documents.FirstOrDefaultAsync(d => d.Id == id);
        if (document == null)
        {
          GC.Collect();

          return NotFound(new { Message = $"Document with id {id} not found" });
        }

        var dbStage = await context.ReviewStages.FirstOrDefaultAsync(s =>
          s.Document.Id == id && s.StageType == model.StageType);
        if (dbStage == null)
        {
          dbStage = model.ToReviewStage(null);
          dbStage.Document = document;
          document.ReviewStages.Add(dbStage);
        }
        else
        {
          dbStage.Date = model.Date;
          dbStage.Number = model.Number;
          dbStage.ProfileCommittee = model.ProfileCommittee;
          dbStage.SendDate = model.SendDate;
          dbStage.StageType = model.StageType;
        }

        context.SaveChanges();
        result = dbStage.ToStageModel();
      }

      GC.Collect();

      return Ok(result);
    }
  }
}
