using System.Threading.Tasks;
using docs.Database;
using docs.Extensions;
using docs.Managers.ElasticSearch;
using docs.Model;
using docs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace docs.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class SummaryController : ControllerBase
  {
    private readonly DocumentsContext _context;

    public SummaryController([FromServices] DocumentsContext context)
    {
      _context = context;
    }

    [AllowAnonymous]
    public async Task<IActionResult> Get([FromServices] ElasticManager elasticManager)
    {
      var docCount = await _context.Documents
        .CountAsync();

      var notSentEmails = await _context.EmailMessages
        .NotSent()
        .CountAsync();

      var result = new ApiSummary
      {
        DocumentsCount = docCount,
        ElasticIsInit = elasticManager.Inited,
        NotSentEmailCount = notSentEmails
      };

      return Ok(new { success = true, data = result });
    }
    }
}
