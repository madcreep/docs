using docs.Database;
using docs.Model;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace docs.Controllers
{
  [Route("api/user")]
  [ApiController]
  [Authorize(Roles = "admin")]
  public class UserController : ControllerBase
  {
    private ILog _logger = LogManager.GetLogger(typeof(UserController));
    private readonly UserManager<User> _userManager;
    private readonly RoleManager<Role> _roleManager;

    public UserController(UserManager<User> userManager, RoleManager<Role> roleManager)
    {
      _userManager = userManager;
      _roleManager = roleManager;
    }

    [Route("list")]
    public async Task<IActionResult> List()
    {
      var result = new List<UserForm>();
      foreach (var user in _userManager.Users)
      {
        result.Add(await AuthService.UserToUserForm(user, _userManager, _roleManager));
      }

      return Ok(result);
    }

    [HttpGet]
    [Route("get/{id}")]
    public async Task<IActionResult> Get(string id)
    {
      var user = await _userManager.FindByIdAsync(id);
      if (user == null)
      {
        return NotFound();
      }
      return Ok(await AuthService.UserToUserForm(user, _userManager, _roleManager));
    }

    [HttpPost]
    public async Task<IActionResult> Create(UserForm model)
    {
      var user = new User { Email = model.Email, UserName = model.Email, FullName = model.FullName };
      var result = await _userManager.CreateAsync(user, model.Password);
      return result.Succeeded ? Ok() : result.HandleErrors(BadRequest, "Не удалось создать пользователя");
    }

    [Route("save")]
    [HttpPost]
    public async Task<IActionResult> Save(UserForm model)
    {
      IdentityResult result;
      try
      {
        var user = await _userManager.FindByIdAsync(model.Id.ToString());

        if (user == null) return NotFound();

        if (!string.IsNullOrWhiteSpace(model.Password))
        {
          user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, model.Password);
        }

        if (user.OnlyPasswordChange)
        {
          result = await _userManager.UpdateAsync(user);

          return result.Succeeded
            ? Ok(await AuthService.UserToUserForm(user, _userManager, _roleManager))
            : result.HandleErrors(BadRequest, "Не удалось обновить пользователя");
        }

        user.Email = model.Email;
        user.UserName = model.Email;
        user.FullName = model.FullName;

        var userRoles = await _userManager.GetRolesAsync(user);

        var addedRoles = model.UserRoles.Except(userRoles).ToArray();

        var removedRoles = userRoles.Except(model.UserRoles).ToArray();

        using (var scope = new TransactionScope())
        {
          result = await _userManager.UpdateAsync(user);

          if (result.Succeeded && addedRoles.Any())
          {
            result = await _userManager.AddToRolesAsync(user, addedRoles);
          }

          if (result.Succeeded && removedRoles.Any())
          {
            result = await _userManager.RemoveFromRolesAsync(user, removedRoles);
          }

          if (result.Succeeded)
          {
            scope.Complete();
          }
        }

        return result.Succeeded
          ? Ok(await AuthService.UserToUserForm(await _userManager.FindByIdAsync(model.Id.ToString()),
            _userManager,
            _roleManager))
          : result.HandleErrors(BadRequest, "Не удалось обновить пользователя");
      }
      catch (Exception ex)
      {
        _logger.Error($"Ошибка при обновлении данных пользователя (Id = {model.Id})", ex);

        return BadRequest(new { Error = "Ошибка при обновлении данных пользователя"});
      }
    }

    [Route("delete/{id}")]
    [HttpDelete]
    public async Task<IActionResult> Delete(string id)
    {
      User currentUser;
      try
      {
        currentUser = await AuthService.GetCurrentUser(HttpContext, _userManager);
      }
      catch (UserException ex)
      {
        return Unauthorized(new { ex.Message });
      }

      if (currentUser == null)
      {
        return Unauthorized(new { Message = "Вы не авторизованы" });
      }
      var user = await _userManager.FindByIdAsync(id);
      if (user == null) return NotFound(new { Message = "Пользователь не найден" });
      if (user.Id == currentUser.Id)
      {
        return Unauthorized(new { Message = "Нельзя удалить самого себя" });
      }

      if (user.IsNotDeleted)
      {
        return BadRequest();
      }

      var roles = await _userManager.GetRolesAsync(user);

      if (roles.Any(r => r == "admin"))
      {
        var role = await _roleManager.FindByNameAsync("admin");
        using (var context = new DocumentsContext())
        {
          var adminCount = context.UserRoles.Count(r => r.RoleId == role.Id && r.UserId != user.Id);
          if (adminCount == 0)
          {
            return BadRequest(new { Message = "Нельзя удалять единственного администратора" });
          }
        }
      }

      var result = await _userManager.DeleteAsync(user);
      return result.Succeeded ? Ok() : result.HandleErrors(BadRequest, "Не удалось удалить пользователя");
    }
  }
}
