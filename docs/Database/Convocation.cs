using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public class Convocation
  {
    [Key, Column("id")]
    public long Id { get; set; }

    [Column("number")]
    public int Number { get; set; }

    [StringLength(128)]
    [Column("name")]
    public string Name { get; set; }

    public virtual List<Session> Sessions { get; set; }
  }
}
