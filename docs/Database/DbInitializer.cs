using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace docs.Database
{
  public class DbInitializer
  {
    private const string AdminEmail = "admin@gmail.com";
    private const string AdminFullname = "Администратор";
    private const string AdminPassword = "Qwe123";

    private const string ServiceEmail = "service@gmail.com";
    private const string ServiceFullname = "Сервис";
    private const string ServicePassword = "asr42~tq";

    public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<Role> roleManager)
    {
      if (await roleManager.FindByNameAsync("admin") == null)
      {
        await roleManager.CreateAsync(new Role("admin"));
      }
      //if (await roleManager.FindByNameAsync("user") == null)
      //{
      //  await roleManager.CreateAsync(new Role("user"));
      //}
      if (await roleManager.FindByNameAsync("service") == null)
      {
        await roleManager.CreateAsync(new Role("service"));
      }

      if (await userManager.FindByNameAsync(AdminEmail) == null)
      {
        var admin = new User
        {
          Email = AdminEmail,
          UserName = AdminEmail,
          FullName = AdminFullname,
          IsNotDeleted = true,
          OnlyPasswordChange = true
        };

        var result = await userManager.CreateAsync(admin, AdminPassword);

        if (result.Succeeded)
        {
          await userManager.AddToRoleAsync(admin, "admin");
        }
      }

      if (await userManager.FindByNameAsync(ServiceEmail) == null)
      {
        var service = new User
        {
          Email = ServiceEmail,
          UserName = ServiceEmail,
          FullName = ServiceFullname,
          IsNotDeleted = true,
          OnlyPasswordChange = true
        };

        var result = await userManager.CreateAsync(service, ServicePassword);

        if (result.Succeeded)
        {
          await userManager.AddToRoleAsync(service, "service");
        }
      }
    }
  }
}
