using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace docs.Database
{
  public class Document
  {
    private static readonly DateTime LawMainDocOnlyBefore = new DateTime(2014, 1, 1);

    [Key, Column("uuid")] public Guid Uuid { get; set; }

    [Column("parent_uuid")] public Guid? ParentUuid { get; set; }

    [Column("display")] [DefaultValue(true)] public bool DisplayOnTheSite { get; set; } = true;

    [ForeignKey(nameof(ParentUuid))]
    public virtual Document ParentDocument { get; set; }

    [Column("id")] public int Id { get; set; }

    [Column("parent_id")] public int? ParentId { get; set; }

    /// <summary>
    /// Признак публикации на открытом сайте
    /// </summary>
    [Column("publish")] public bool Publish { get; set; }

    [Column("order")] public int? Order { get; set; }

    [Column("document_type")]
    [Display(Name = "Тип документа")]
    public DocumentType DocumentType { get; set; }

    [Column("is_project")] public bool IsProject { get; set; }

    [Column("upload_date")]
    [Display(Name = "Дата выгрузки")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    public DateTime UploadDate { get; set; }

    [Column("date")]
    [Display(Name = "Дата создания")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    public DateTime Date { get; set; }

    [Column("number")]
    [StringLength(64)]
    [Display(Name = "Номер документа")]
    public string Number { get; set; }

    [Column("tech_number")]
    [Display(Name = "Технический номер")]
    [StringLength(255)]
    public string TechNumber { get; set; }

    [Column("publish_date")]
    [Display(Name = "Дата публикации")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    public DateTime? PublishDate { get; set; }

    [Column("anticorruption_expertise")]
    [Display(Name = "Антикоррупционная экспертиза")]
    public bool AnticorruptionExpertise { get; set; }
    [Column("expertise_start")]
    [Display(Name = "Дата начала антикоррупционной экспертизы")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    public DateTime? ExpertiseStart { get; set; }
    [Column("expertise_end")]
    [Display(Name = "Дата окончания антикоррупционной экспертизы")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    public DateTime? ExpertiseEnd { get; set; }

    [Column("correspondent")]
    [StringLength(1024)]
    [Display(Name = "Корреспондент")]
    public string Correspondent { get; set; }

    /// <summary>
    /// Признак того, что корреспондент это подразделение ЗСК
    /// </summary>
    [Column("correspondent_is_zsk")]
    public bool CorrespondentIsZsk { get; set; }

    [Column("profile_committee")]
    [StringLength(1024)]
    [Display(Name = "Профильный комитет")]
    public string ProfileCommittee { get; set; }

    [Column("convening")]
    public int? Convening { get; set; }

    [Column("session_number")]
    public int? SessionNumber { get; set; }

    [Column("subject_of_law"), StringLength(1024)]
    [Display(Name = "Субъект права законодательной инициативы")]
    public string SubjectOfLaw { get; set; }

    [Column("content")]
    [Display(Name = "Описание")]
    public string Content { get; set; }

    [Column("group"), StringLength(1024)]
    [Display(Name = "Группа")]
    public string Group { get; set; }

    [Column("has_approved_act")] public bool? HasApprovedAct { get; set; }

    [Column("rubric_uuid")] public Guid? RubricUuid { get; set; }

    [ForeignKey(nameof(RubricUuid))]
    [Display(Name = "Рубрика")]
    public virtual Rubric Rubric { get; set; }

    public ICollection<ReviewStage> ReviewStages { get; set; }

    [Column("session_is_self")] public bool SessionIsSelf { get; set; }

    [Column("session_id")] public long? SessionId { get; set; }

    [ForeignKey(nameof(SessionId))]
    [Display(Name = "Сессия")]
    public virtual Session Session { get; set; }

    public virtual ICollection<Document> ChildeDocuments { get; set; }

    [StringLength(64)]
    [Column("agenda_number")] public string AgendaNumber { get; set; }

    [Column("reading_id")] public long? ReadingId { get; set; }

    [ForeignKey(nameof(ReadingId))]
    [Display(Name = "Чтение")]
    public virtual Reading Reading { get; set; }

    [Column("stage_id")] public long? StageId { get; set; }

    [ForeignKey(nameof(StageId))]
    [Display(Name = "Результат рассмотрения")]
    public virtual Stage Stage { get; set; }

    [Column("result_id")] public long? ResultId { get; set; }

    [ForeignKey(nameof(ResultId))]
    [Display(Name = "Результат рассмотрения")]
    public virtual Result Result { get; set; }

    [Display(Name = "Файлы")]
    public virtual ICollection<DocumentFile> Files { get; set; }

    [Column("indexing")] public bool Indexing { get; set; } = false;

    [Column("ins_date")] public DateTime? InsDate { get; set; }

    /// <summary>
    /// Место публикации
    /// </summary>
    [Column("place_of_publication"), StringLength(256)] public string PlaceOfPublication { get; set; }

    private Document _finalDoc;

    private bool? _finalDocFindResult;

    public Document FinalDoc
    {
      get
      {
        if (_finalDocFindResult.HasValue)
        {
          return _finalDoc;
        }

        if (DocumentType == DocumentType.DecreeProject || DocumentType == DocumentType.LawProject)
        {
          _finalDoc = ChildeDocuments?
            .FirstOrDefault(el => (DocumentType == DocumentType.LawProject && el.DocumentType == DocumentType.Law)
                                  || (DocumentType == DocumentType.DecreeProject && el.DocumentType == DocumentType.Decree))
            ?? null;
        }
        else
        {
          _finalDoc = this;
        }

        _finalDocFindResult = _finalDoc != null;

        return _finalDoc;
      }
    }

    [NotMapped]
    public DocumentType FinalDocumentType
    {
      get
      {
        if (FinalDoc == null)
        {
          return DocumentType;
        }

        switch (DocumentType)
        {
          case DocumentType.LawProject:
            return DocumentType.Law;
          case DocumentType.DecreeProject:
            return DocumentType.Decree;
          case DocumentType.Decree:
            //return ParentDocument.DocumentType == DocumentType.DecreeProject ? DocumentType.Decree : DocumentType.Law;
            return DocumentType.Decree;
          case DocumentType.Law:
            return DocumentType.Law;
          default:
            throw new ArgumentOutOfRangeException();
        }
      }
    }

    [NotMapped]
    public string FinalDocumentNumber => FinalDoc == null
      ? string.IsNullOrWhiteSpace(TechNumber) ? Number : TechNumber
      : FinalDoc.Number;

    [NotMapped]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    public DateTime FinalDate => FinalDoc?.Date ?? Date;

    [NotMapped]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    public DateTime? FinalPublishDate => FinalDoc?.PublishDate ?? FinalDoc?.Date ?? PublishDate ?? Date;

    [NotMapped]
    public string FinalSubjectOfLaw
    {
      get
      {
        if (FinalDoc == null || string.IsNullOrWhiteSpace(FinalDoc.SubjectOfLaw))
        {
          return SubjectOfLaw;
        }

        return FinalDoc.SubjectOfLaw;
      }
    }

    [NotMapped]
    public string FinalCorrespondent
    {
      get
      {
        if (FinalDoc == null || string.IsNullOrWhiteSpace(FinalDoc.Correspondent))
        {
          return Correspondent;
        }

        return FinalDoc.Correspondent;
      }
    }

    [NotMapped] public string FinalContent => FinalDoc?.Content ?? Content;

    [NotMapped] public DateTime FinalUploadDate => FinalDoc?.UploadDate ?? UploadDate;

    [NotMapped] public Session FinalSession => FinalDoc?.Session ?? Session;

    [NotMapped]
    public DocumentFile FinalFile => FinalDoc?.MainFile;

    [NotMapped]
    public string FinalPlaceOfPublication => FinalDoc?.PlaceOfPublication;

    [NotMapped]
    public DocumentFile MainFile
    {
      get
      {
        if (Files == null || !Files.Any())
        {
          return null;
        }

        string pattern;

        switch (DocumentType)
        {
          case DocumentType.DecreeProject:
            pattern = "проект постановления законодательного собрания";
            break;
          case DocumentType.LawProject:
            pattern = "проект закона краснодарского края";
            break;
          case DocumentType.Law:
            pattern = "основной закон";
            break;
          case DocumentType.Decree:
            pattern = "основное постановление";
            break;
          case DocumentType.Letter1:
          case DocumentType.Letter2:
          case DocumentType.Letter3:
          case DocumentType.Undefined:
          case DocumentType.PlenarySession:
            pattern = null;
            break;
          default:
            throw new ArgumentOutOfRangeException();
        }

        if (pattern == null)
        {
          return null;
        }

        var fileByPattern = Files
          .Where(el=>el.Send)
          .OrderBy(el => el.OrderNum)
          .FirstOrDefault(el => el.Description.ToLower().Contains(pattern));

        if (DocumentType == DocumentType.Law && Date < LawMainDocOnlyBefore)
        {
          return fileByPattern;
        }

        return fileByPattern
               ?? Files.OrderBy(el => el.OrderNum).First();
      }
    }

    [NotMapped]
    public List<Document> SingleRangeDocuments { get; set; }

    [NotMapped]
    public Document[] Conclusion { get; set; }

    /// <summary>
    /// Отметка времени выгрузки
    /// </summary>
    [Column("updating_timestamp")] public DateTime? UnloadingTimestamp { get; set; }
  }

  public enum DocumentType
  {
    /// <summary>
    /// Не определено
    /// </summary>
    Undefined = 0,
    /// <summary>
    /// Проект закона
    /// </summary>
    LawProject = 1,
    /// <summary>
    /// Проект постановления
    /// </summary>
    DecreeProject = 2,
    /// <summary>
    /// Письмо (о включении в повестку)
    /// </summary>
    Letter1 = 51,
    /// <summary>
    /// Письмо (о включении в повестку 2 чтение)
    /// </summary>
    Letter2 = 52,
    /// <summary>
    /// Письмо (о включении  в повестку 3 чтение)
    /// </summary>
    Letter3 = 53,
    /// <summary>
    /// Заключение на НПА (1 чтение)
    /// </summary>
    Conclusion1 = 31,
    /// <summary>
    /// Заключение на НПА (2 чтение)
    /// </summary>
    Conclusion2 = 32,
    /// <summary>
    /// Закон
    /// </summary>
    Law = 11,
    /// <summary>
    /// Постановление
    /// </summary>
    Decree = 21,
    /// <summary>
    /// Пленарное заседание
    /// </summary>
    PlenarySession = 101
  }
}
