using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace docs.Database
{
  public class DocumentFile
  {
    [Key]
    [Column("uuid")]
    public Guid Uuid { get; set; }

    [Column("id")]
    public int Id { get; set; }

    [Column("filename")]
    [StringLength(512)]
    public string FileName { get; set; }

    [Column("extension")]
    [StringLength(12)]
    public string Extension { get; set; }

    [Column("size")]
    public long? Size { get; set; }

    [Column("description")]
    [StringLength(512)]
    public string Description { get; set; }

    [NotMapped]
    public string Name => Path.GetFileNameWithoutExtension(string.IsNullOrWhiteSpace(Description) ? FileName : Description);

    [Column("order_num")]
    public int? OrderNum { get; set; }

    [Column("send")]
    public bool Send { get; set; }

    [Column("document_uuid")]
    public Guid DocumentUuid { get; set; }

    [ForeignKey(nameof(DocumentUuid))]
    public virtual Document Document { get; set; }

    [Column("indexing")] public bool Indexing { get; set; } = false;
  }
}
