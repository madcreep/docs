using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Migrations.Internal;
using System;

namespace docs.Database
{
  public class PostgreHistoryRepository : NpgsqlHistoryRepository
  {
    public PostgreHistoryRepository(HistoryRepositoryDependencies dependencies)
      : base(dependencies)
    {
    }

    protected override void ConfigureTable(EntityTypeBuilder<HistoryRow> history)
    {
      base.ConfigureTable(history);
      history.Property(h => h.MigrationId).HasColumnName("migration_id");
      history.Property(h => h.ProductVersion).HasColumnName("product_version");
    }
  }

  public class DocumentsContext : IdentityDbContext<User, Role, Guid>
  {
    public new DbSet<User> Users { get; set; }
    public DbSet<Document> Documents { get; set; }
    public DbSet<DocumentFile> DocumentFiles { get; set; }
    public DbSet<Rubric> Rubrics { get; set; }
    public DbSet<ReviewStage> ReviewStages { get; set; }
    public DbSet<Session> Sessions { get; set; }
    public DbSet<SessionDocuments> SessionDocuments { get; set; }
    public DbSet<Convocation> Convocations { get; set; }
    public DbSet<Reading> Readings { get; set; }
    public DbSet<Stage> Stages { get; set; }
    public DbSet<Result> Results { get; set; }
    public DbSet<EmailMessage> EmailMessages { get; set; }
    public DbSet<SingleRangeLink> SingleRangeLinks { get; set; }


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.ReplaceService<IHistoryRepository, PostgreHistoryRepository>()
        .UseNpgsql(GlobalProperties.ConnectionString,
          options => options.MigrationsHistoryTable("_ef_migrations", "public")
        );
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      //Users
      modelBuilder.HasDefaultSchema("public");
      modelBuilder.HasPostgresExtension("uuid-ossp")
        .Entity<User>()
        .Property(e => e.Id)
        .HasDefaultValueSql("uuid_generate_v4()");

      modelBuilder.Entity<User>().HasIndex(b => b.UserName);

      //Documents & files
      modelBuilder.Entity<Document>().HasIndex(b => b.Id);
      modelBuilder.Entity<Document>().HasIndex(b => b.Number);
      modelBuilder.Entity<Document>().HasIndex(b => b.TechNumber);
      modelBuilder.Entity<Document>().HasIndex(b => b.Date);

      modelBuilder.Entity<Document>().HasQueryFilter(el => el.Publish);

      modelBuilder.Entity<DocumentFile>().HasIndex(b => b.Id);
      modelBuilder.Entity<DocumentFile>().HasIndex(b => b.FileName);

      

      modelBuilder.Entity<DocumentFile>()
        .HasOne(f => f.Document)
        .WithMany(d => d.Files)
        .IsRequired();
      modelBuilder.Entity<ReviewStage>()
        .HasOne(f => f.Document)
        .WithMany(d => d.ReviewStages)
        .IsRequired();
      modelBuilder.Entity<Rubric>().HasIndex(r => r.Due);
      modelBuilder.Entity<Rubric>().HasIndex(r => r.Id);

      modelBuilder.Entity<Session>().HasQueryFilter(p => !p.IsDeleted);
      modelBuilder.Entity<Session>().HasIndex(r => r.NumberInt);

      var adminRoleName = "admin";
      var userRoleName = "user";
      var serviceRoleName = "service";

      var adminRole = new Role
      {
        Id = new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
        Name = adminRoleName,
        NormalizedName = adminRoleName.ToUpper()
      };
      var userRole = new Role
      {
        Id = new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
        Name = userRoleName,
        NormalizedName = userRoleName.ToUpper()
      };
      var serviceRole = new Role
      {
        Id = new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
        Name = serviceRoleName,
        NormalizedName = serviceRoleName.ToUpper()
      };

      modelBuilder.Entity<Role>().HasData(adminRole, userRole, serviceRole);

      base.OnModelCreating(modelBuilder);
    }

    public DbSet<Models.UserModel> UserModel { get; set; }
  }

}
