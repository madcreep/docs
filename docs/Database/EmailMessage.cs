using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public interface IEmailMessage
  {
    string FromSectionName { get; }
    string ToSectionName { get; }
    string MessageSubject { get; }
    string MessageBody { get; }
    bool IsBodyHtml { get; }
    string FileDirectory { get; }
  }

  public class EmailMessage
  {
    [Key, Column("id")] public long Id { get; set; }

    [Column("from_section"), StringLength(1024)] public string FromSectionName { get; set; }

    [Column("to_section"), StringLength(1024)] public string ToSectionName { get; set; }

    [Column("message_subject"), StringLength(512)] public string MessageSubject { get; set; }

    [Column("file_directory"), StringLength(128)] public string FileDirectory { get; set; }

    [Column("message_body")] public string MessageBody { get; set; }

    [Column("is_body_html")] public bool IsBodyHtml { get; set; }

    [Column("created_date")] public DateTime CreatedDate { get; set; }

    [Column("send_date")] public DateTime? SendDate { get; set; }

    [Column("is_sent")] public bool IsSent { get; set; }

    [Column("sent_error")] public string SentError { get; set; }

    [Column("need_send")] public bool NeedSend { get; set; }
    
  }
}
