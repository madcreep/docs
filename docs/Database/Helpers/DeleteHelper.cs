using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using docs.Extensions;
using docs.Models;
using log4net;
using Microsoft.EntityFrameworkCore;

namespace docs.Database.Helpers
{
  internal class DeleteHelper
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(DeleteHelper));

    private readonly DocumentsContext _context;

    public DeleteHelper(DocumentsContext context)
    {
      _context = context;
    }

    public async Task<PostDocumentAnswer> Delete(RcProject model)
    {
      return await DeleteSession(model);
    }

    private async Task<PostDocumentAnswer> DeleteSession(RcProject model)
    {
      var result = new PostDocumentAnswer();

      try
      {
        var delSession = await _context.Sessions
          .WithAllFields()
          .FirstOrDefaultAsync(el => el.Isn == model.Isn);

        if (delSession == null)
        {
          result.ErrorMsg = $"Запись с Isn = {model.Isn} в сессиях не найдена";

          Logger.Info(result.ErrorMsg);

          return result;
        }

        var hasRc = await _context.Documents
          .BySessionId(delSession.Id)
          .AnyAsync();

        if (hasRc)
        {
          result.ErrorMsg = $"Запись с Isn = {model.Isn} в сессиях не удалена. Есть зависимые документы.";

          Logger.Warn(result.ErrorMsg);

          return result;
        }

        var convocation = await _context.Convocations
          .WithAllFields()
          .ById(delSession.ConvocationId)
          .FirstOrDefaultAsync();

        Logger.DebugFormat("Before delete convocation.Sessions.Count = {0}", convocation.Sessions.Count);

        _context.Sessions.Remove(delSession);

        if (convocation != null)
        {
          convocation.Sessions.Remove(delSession);

          if (!convocation.Sessions.Any())
          {
            _context.Convocations.Remove(convocation);

            Logger.InfoFormat("Запись о созыве № {0} удалена", convocation.Number);
          }
        }

        Logger.DebugFormat("After delete convocation.Sessions.Count = {0}", convocation.Sessions.Count);

        await _context.SaveChangesAsync();

        Logger.InfoFormat("Запись с Isn = {0} в сессиях удалена. (сессия № {1} {2} ({3}) {4} созыва)"
          , model.Isn
          , delSession.NumberInt
          , delSession.SessionType
          , delSession.Date.HasValue ? delSession.Date.Value.ToString("dd.MM.yyyy") : "__.__.____"
          , convocation == null ? "???" : convocation.Number.ToString()
          );

        return result;
      }
      catch (Exception ex)
      {
        result.ErrorMsg = $"Ошибка удаления документа (Isn = {model.Isn}){Environment.NewLine}{ex}";

        result.CanDelete = false;

        Logger.Error(result.ErrorMsg);
      }

      return result;
    }
  }
}
