using docs.Extensions;
using docs.Managers;
using docs.Models;
using docs.Utils;
using log4net;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace docs.Database.Helpers
{
  internal static class DocumentHelper
  {
    private static readonly DateTime WithoutSessionBeforeDate = new DateTime(2002, 1, 1);

    private static readonly ILog Logger = LogManager.GetLogger(typeof(DocumentHelper));

    public static async Task<Document> GenerateDocument(RcProject model, DocumentsContext context, PostDocumentAnswer generateResult)
    {
      var document = await GetDocument(model, context, generateResult);

      if (document == null)
      {
        return null;
      }

      var lawForUpdate = await GetLawForUpdateSession(context, document);

      await UpdateSingleRangeLinks(context, model, document);

      if (!await AddSession(context, model, document))
      {
        generateResult.ErrorMsg = $"Ошибка привязки к сессии (№ {model.Session?.Number} созыв {model.Session?.Convocation})";

        generateResult.CanDelete = false;

        return null;
      }

      foreach (var lawDoc in lawForUpdate)
      {
        await UpdateLawSession(context, lawDoc);
      }

      AddFilesInfo(context, model, document);

      await AddRubric(context, model, document);

      await AddReading(context, model, document);

      await AddStage(context, model, document);

      await AddResult(context, model, document);

      SetPublish(model, document);

      await context.SaveChangesAsync();

      return document;
    }

    private static void SetPublish(RcProject model, Document document)
    {
      if (document.ParentDocument == null || document.DocumentType == DocumentType.Decree)
      {
        document.Publish = model.Publish;

        if ((document.DocumentType != DocumentType.DecreeProject && document.DocumentType != DocumentType.LawProject) ||
            document.ChildeDocuments == null)
        {
          return;
        }

        foreach (var chDoc in document.ChildeDocuments)
        {
          if (chDoc.DocumentType == DocumentType.Decree)
          {
            continue;
          }

          chDoc.Publish = document.Publish;
        }

        return;
      }

      document.Publish = document.ParentDocument.Publish;
    }

    private static async Task<Document> GetDocument(RcProject model, DocumentsContext context, PostDocumentAnswer getResult)
    {
      if (!model.ParentIsn.HasValue && (model.RcType == DocumentType.Letter1
          || model.RcType == DocumentType.Letter2
          || model.RcType == DocumentType.Letter3
          || model.RcType == DocumentType.Conclusion1
          || model.RcType == DocumentType.Conclusion2))
      {
        getResult.ErrorMsg = "Не указан родительский документ";

        Logger.ErrorFormat(
          "Передан документ {0} (№ {1} от {2} (isn={3})) для регистрации без указания родительского документа"
          , model.RcType.SmallName()
          , model.RegistrationNumber
          , model.RegistrationDate.ToString("dd.MM.yyyy")
          , model.Isn);

        return null;
      }

      var document = await context.Documents
        .OnlyForLanSite(true)
        .WithAllFields()
        .ById(model.Isn)
        .FirstOrDefaultAsync();

      if (document != null && document.UnloadingTimestamp.HasValue && model.UnloadingTimestamp <= document.UnloadingTimestamp.Value)
      {
        getResult.ErrorMsg = $"Уже имеется более новое обновление ({document.UnloadingTimestamp.Value:dd.MM.yyyy HH:mm:ss})";

        return null;
      }


      Document parentDoc = null;

      if (model.ParentIsn.HasValue)
      {
        parentDoc = await context.Documents
          .OnlyForLanSite(true)
          .WithAllFields()
          .ById(model.ParentIsn.Value)
          .FirstOrDefaultAsync();

        Logger.Debug($"Родительский документ (Id : {parentDoc?.Id.ToString() ?? "null"})");

        if (model.RcType != DocumentType.Decree && parentDoc == null)
        {
          Logger.Warn($"Не найден родительский документ (Id : {model.ParentIsn})");

          getResult.ErrorMsg = "Не найден родительский документ";

          getResult.CanDelete = false;

          return null;
        }
      }

      if (document == null)
      {
        document = model.ToDocument();

        if (parentDoc != null)
        {
          parentDoc.ChildeDocuments.Add(document);

          document.ParentDocument = parentDoc;

          document.ParentUuid = parentDoc.Uuid;
        }
        else
        {
          context.Documents.Add(document);
        }
      }
      else
      {
        document.ParentId = model.ParentIsn;
        document.ParentDocument = parentDoc;
        document.ParentUuid = parentDoc?.Uuid;

        if (parentDoc != null && parentDoc.ChildeDocuments.All(el => el.Id != model.Isn))
        {
          parentDoc.ChildeDocuments.Add(document);
        }

        document.DocumentType = model.RcType;

        document.Order = model.Order;
        document.AnticorruptionExpertise = model.AntiCorruptionExpertise != null;
        document.Content = model.Content;
        document.Date = model.RegistrationDate;
        document.ExpertiseStart = model.AntiCorruptionExpertise?.From;
        document.ExpertiseEnd = model.AntiCorruptionExpertise?.To;
        document.Number = model.RegistrationNumber;
        document.TechNumber = model.TechNumber;
        document.ProfileCommittee = model.ProfCommittee;
        document.PublishDate = model.PublicationDate;
        document.UploadDate = DateTime.Now;
        document.Correspondent = model.Correspondent;
        document.CorrespondentIsZsk = model.CorrespondentIsZsk;
        document.AgendaNumber = model.AgendaNumber;
        document.SubjectOfLaw = model.SubjectOfLaw;
        document.InsDate = model.InsDate;
        document.PlaceOfPublication = model.PlaceOfPublication;
        document.UnloadingTimestamp = model.UnloadingTimestamp;
        //document.Publish = model.Publish;
      }

      if (!document.ParentId.HasValue && (document.DocumentType == DocumentType.DecreeProject || document.DocumentType == DocumentType.LawProject))
      {
        document.ChildeDocuments = await context.Documents
          .OnlyForLanSite(true)
          .Where(el => el.ParentId == document.Id)
          .ToArrayAsync();
      }

      if (parentDoc == null && document.DocumentType == DocumentType.Decree && string.IsNullOrWhiteSpace(document.Correspondent))
      {
        document.Correspondent = document.SubjectOfLaw;
      }

      Logger.Debug($"Pos 1 doc id = {document.Id}");

      return document;
    }

    private static async Task<bool> AddSession(DocumentsContext context, RcProject model, Document document)
    {
      try
      {
        switch (model.RcType)
        {
          case DocumentType.LawProject:
          case DocumentType.DecreeProject:
            return true;
          case DocumentType.Law:
            return await SetLawSession(context, model, document);
          case DocumentType.Decree:
          case DocumentType.Letter1:
          case DocumentType.Letter2:
          case DocumentType.Letter3:
            return await SetSessionForAnotherDocs(context, model, document);
          case DocumentType.Undefined:
          case DocumentType.PlenarySession:
            return false;
          case DocumentType.Conclusion1:
          case DocumentType.Conclusion2:
            return true;
          default:
            throw new ArgumentOutOfRangeException();
        }
      }
      catch (Exception ex)
      {
        Logger.Error("Ошибка установки сессии", ex);

        return false;
      }
    }

    private static async Task<bool> UpdateLawSession(DocumentsContext context, Document lawDoc)
    {
      Document lastDecree;

      if (lawDoc.ParentDocument == null)
      {
        lastDecree = await DocumentsBySingleLink(context, lawDoc.Id)
          .ByDocumentType(DocumentType.Decree)
          .WithAllFields()
          .OrderByDescending(el => el.Date)
          .FirstOrDefaultAsync();
      }
      else
      {
        lastDecree = lawDoc.ParentDocument.ChildeDocuments
          .ByDocumentType(DocumentType.Decree)
          .OrderByDescending(el => el.Date)
          .FirstOrDefault();
      }

      lawDoc.SessionId = lastDecree?.SessionId;

      lawDoc.Session = lastDecree?.Session;

      lawDoc.SessionIsSelf = false;

      return true;
    }

    private static async Task<Document> GetLawBySingleLinks(DocumentsContext context, int decreeId)
    {
      var result = await DocumentsBySingleLink(context, decreeId)
        .OnlyForLanSite(true)
        .ByDocumentType(DocumentType.Law)
        .WithAllFields()
        .SingleOrDefaultAsync();

      return result;
    }

    private static IQueryable<Document> DocumentsBySingleLink(DocumentsContext context, int docId)
    {
      return context.SingleRangeLinks
        .Where(d => d.From == docId)
        .Join(context.Documents,
          sr => sr.To,
          d => d.Id,
          (sr, d) => d)
        .Union(context.SingleRangeLinks
          .Where(d => d.To == docId)
          .Join(context.Documents,
            sr => sr.From,
            d => d.Id,
            (sr, d) => d));
    }

    private static async Task<bool> SetSessionForAnotherDocs(DocumentsContext context, RcProject model, Document document)
    {
      if (model.Session == null)
      {
        document.SessionId = null;

        document.Session = null;

        document.SessionIsSelf = true;

        return true;
      }

      var modelSession = model.Session;

      if (modelSession.Convocation == null
          || !modelSession.Convocation.TryIntPart(out var convocationNumber)
          || !modelSession.Number.TryIntPart(out var sessionNumber))
      {
        return false;
      }

      var convocation = await context.Convocations
        .WithAllFields()
        .ByNumber(convocationNumber)
        .FirstOrDefaultAsync();

      var session = convocation?.Sessions
        .FirstOrDefault(el => el.NumberInt == sessionNumber);

      if (session == null)
      {
        return false;
      }

      document.SessionId = session.Id;

      document.Session = session;

      document.SessionIsSelf = true;

      return true;
    }

    private static async Task<bool> SetLawSession(DocumentsContext context, RcProject model, Document document)
    {
      return model.Session == null
        ? await UpdateLawSession(context, document)
        : await SetSessionForAnotherDocs(context, model, document);
    }

    private static async Task AddStage(DocumentsContext context, RcProject model, Document document)
    {
      if (string.IsNullOrWhiteSpace(model.Stage))
      {
        document.Stage = null;
        document.StageId = null;
      }
      else
      {
        document.Stage = await context.Stages
                           .ByName(model.Stage)
                           .FirstOrDefaultAsync()
                         ?? new Stage
                         {
                           Name = model.Stage,
                           Number = model.Stage.ExtractNumber()
                         };
      }
    }

    private static async Task AddResult(DocumentsContext context, RcProject model, Document document)
    {
      if (string.IsNullOrWhiteSpace(model.Result))
      {
        document.Result = null;
        document.ResultId = null;
      }
      else
      {
        document.Result = await context.Results
                            .ByName(model.Result)
                            .FirstOrDefaultAsync()
                          ?? new Result
                          {
                            Name = model.Result
                          };
      }
    }

    private static async Task AddReading(DocumentsContext context, RcProject model, Document document)
    {
      if (string.IsNullOrEmpty(model.Reading))
      {
        document.ReadingId = null;
        document.Reading = null;
      }
      else
      {
        document.Reading = await context.Readings
                             .ByName(model.Reading)
                             .FirstOrDefaultAsync()
                           ?? new Reading
                           {
                             Name = model.Reading
                           };
      }
    }

    private static async Task AddRubric(DocumentsContext context, RcProject model, Document document)
    {
      if (!string.IsNullOrWhiteSpace(model.Rubric))
      {
        if (document.Rubric == null || document.Rubric.Name != model.Rubric)
        {
          document.Rubric = await context.Rubrics
                              .ByName(model.Rubric)
                              .FirstOrDefaultAsync()
                            ?? new Rubric
                            {
                              Name = model.Rubric
                            };
        }
      }
      else
      {
        document.Rubric = null;
      }
    }

    private static void AddFilesInfo(DocumentsContext context, RcProject model, Document document)
    {
      if (model.Files != null && model.Files.Any())
      {
        var files = model.Files
          .Select(el => new DocumentFile
          {
            Id = el.Isn,
            FileName = el.Name,
            Description = el.Description,
            Extension = Path.GetExtension(el.Name),
            OrderNum = el.OrderNum
          })
          .ToArray();

        if (document.Files == null || !document.Files.Any())
        {
          document.Files = files.ToArray();
        }
        else
        {
          var modelFiles = files.ToDictionary(el => el.Id);

          var docFiles = document.Files.ToDictionary(el => el.Id);

          var modelFilesIds = modelFiles.Keys.ToArray();

          var docFilesIds = docFiles.Keys.ToArray();

          var toAdd = modelFilesIds.Except(docFilesIds);

          var toDelete = docFilesIds.Except(modelFilesIds);

          var toUpdate = docFilesIds.Intersect(modelFilesIds);

          foreach (var key in toDelete)
          {
            var dFile = docFiles[key];

            document.Files.Remove(dFile);

            context.DocumentFiles.Remove(dFile);

            FileManager.DeleteFile(dFile);
          }

          foreach (var key in toUpdate)
          {
            var dFile = docFiles[key];

            var mFile = modelFiles[key];

            dFile.Description = mFile.Description;
            dFile.FileName = mFile.FileName;
            dFile.Extension = mFile.Extension;
            dFile.OrderNum = mFile.OrderNum;
            dFile.Send = false;
            dFile.Size = null;
          }

          foreach (var key in toAdd)
          {
            var dFile = modelFiles[key];

            document.Files.Add(dFile);
          }
        }
      }
      else
      {
        if (document.Files != null && document.Files.Any())
        {
          context.DocumentFiles.RemoveRange(document.Files);

          FileManager.DeleteFileDirectory(document.Uuid);
        }

        document.Files = null;
      }
    }

    private static async Task<bool> UpdateSingleRangeLinks(DocumentsContext context, RcProject model, Document document)
    {
      if (document.ParentUuid.HasValue)
      {
        var _ = await RemoveSingleRangeLinks(context, document.Id);
      }
      else
      {
        var sLinks = await context.SingleRangeLinks
          .Where(el => el.From == document.Id || el.To == document.Id)
          .ToArrayAsync();

        if (model.SingleRangeIsn == null || !model.SingleRangeIsn.Any())
        {
          context.SingleRangeLinks.RemoveRange(sLinks);
        }
        else
        {
          var forDelete = sLinks
            .Where(el => (el.From == document.Id && !model.SingleRangeIsn.Contains(el.To))
                         || (el.To == document.Id && !model.SingleRangeIsn.Contains(el.From)));

          context.SingleRangeLinks.RemoveRange(forDelete);

          var hasLink = (sLinks.Select(el => el.To)
              .Union(sLinks.Select(el => el.From)))
            .Where(el => el != document.Id)
            .Distinct();

          var forAdd = model.SingleRangeIsn
            .Where(el => !hasLink.Contains(el))
            .Select(el => new SingleRangeLink
            {
              From = document.Id,
              To = el
            });

          context.SingleRangeLinks.AddRange(forAdd);
        }
      }

      return true;
    }

    private static async Task<bool> RemoveSingleRangeLinks(DocumentsContext context, int docId)
    {
      context.SingleRangeLinks.RemoveRange(
        await context.SingleRangeLinks
          .Where(el => el.From == docId || el.To == docId)
          .ToArrayAsync()
      );

      return true;
    }

    private static async Task<Document[]> GetLawForUpdateSession(DocumentsContext context, Document document)
    {
      if (document.DocumentType == DocumentType.Law
          || document.DocumentType == DocumentType.Conclusion1
          || document.DocumentType == DocumentType.Conclusion2)
      {
        return new Document[0];
      }

      var result = document.ParentDocument?
        .ChildeDocuments?
        .ByDocumentType(DocumentType.Law)
        .Where(el => el.SessionId == null || !el.SessionIsSelf)
        .ToList()
        ?? new List<Document>();

      var lawBySingleLinks = await GetLawBySingleLinks(context, document.Id);

      if (lawBySingleLinks != null && (!lawBySingleLinks.SessionId.HasValue || !lawBySingleLinks.SessionIsSelf))
      {
        result.Add(lawBySingleLinks);
      }

      return result
        .GroupBy(el => el.Uuid)
        .Select(el => el.First())
        .ToArray();
    }
  }
}
