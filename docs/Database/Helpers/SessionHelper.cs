using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using docs.Extensions;
using docs.Models;
using docs.Utils;
using Microsoft.EntityFrameworkCore;

namespace docs.Database.Helpers
{
  internal static class SessionHelper
  {
    public static async Task<bool> AddSession(RcProject model, DocumentsContext context)
    {
      if (model.Session == null
          || !model.Session.Convocation.TryIntPart(out var convocationNumber)
          || !model.Session.Number.TryIntPart(out var sessionNumber))
      {
        return false;
      }

      var convocation = await context.Convocations
        .WithAllFields()
        .ByNumber(convocationNumber)
        .FirstOrDefaultAsync();

      if (convocation == null)
      {
        convocation = new Convocation
        {
          Number = convocationNumber,
          Name = model.Session.Convocation,
          Sessions = new List<Session>()
        };

        context.Add(convocation);
      }
      else
      {
        convocation.Name = model.Session.Convocation;
      }

      var session = convocation
        .Sessions
        .FirstOrDefault(el => el.NumberInt == sessionNumber);

      if (session == null)
      {
        session = model.Session.ToDbSession(convocation);

        context.Sessions.Add(session);
      }
      else
      {
        session.Date = model.Session.Date;

        session.SessionType = model.Session.SessionType;
      }

      session.Isn = model.Isn;

      session.IsDeleted = false;

      session.UnloadingTimestamp = model.UnloadingTimestamp;

      if (model.Files != null)
      {
        if (session.SessionDocuments == null)
        {
          session.SessionDocuments = await context.SessionDocuments
                                       .FirstOrDefaultAsync(el=>el.SessionId == session.Id)
                                     ?? new SessionDocuments();
        }

        var AgendaFile = model.Files
          .FirstOrDefault(el => el.Description.ToLower().StartsWith("повестка"));

        var AnnotationFile = model.Files
          .FirstOrDefault(el => el.Description.ToLower().StartsWith("аннотация"));

        if (AgendaFile == null)
        {
          session.SessionDocuments.AgendaIsn = null;
          session.SessionDocuments.Agenda = null;
        }
        else
        {
          if (session.SessionDocuments.AgendaIsn != AgendaFile.Isn)
          {
            session.SessionDocuments.AgendaIsn = AgendaFile.Isn;

            session.SessionDocuments.Agenda = null;
          }
          
        }

        if (AnnotationFile == null)
        {
          session.SessionDocuments.AnnotationIsn = null;

          session.SessionDocuments.Annotation = null;
        }
        else
        {
          if (session.SessionDocuments.AnnotationIsn != AnnotationFile.Isn)
          {
            session.SessionDocuments.AnnotationIsn = AnnotationFile.Isn;

            session.SessionDocuments.Annotation = null;
          }
        }
      }

      await context.SaveChangesAsync();

      return true;
    }
  }
}
