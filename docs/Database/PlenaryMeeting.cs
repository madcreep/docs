using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
    public class PlenaryMeeting
    {
        [Key, Column("uuid")] 
        public Guid Uuid { get; set; }
        
        [Column("convening")]
        public int Convening { get; set; }
        
        [Column("session_number")]
        public int SessionNumber { get; set; }

        [Column("session_type")]
        public SessionTypes SessionType { get; set; }
        
        [Column("agenda")]
        public string Agenda { get; set; }
        [Column("agenda_file_id")]
        public Guid? AgendaFileId { get; set; }

        [Column("annotation")]
        public string Annotation { get; set; }
        [Column("annotation_file_id")]
        public Guid? AnnotationFileId { get; set; }

    }

    public enum SessionTypes
    {
        Regular,
        Extra
    }
}