using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public class Reading
  {
    [Key, Column("id")]
    public long Id { get; set; }

    [StringLength(128), Column("name")]
    public string Name { get; set; }
  }
}
