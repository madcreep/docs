using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public class Result
  {
    [Key, Column("id")] public long Id { get; set; }

    [Column("name"), StringLength(256)] public string Name { get; set; }
  }
}
