using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;

namespace docs.Database
{
  public class ReviewStage
  {

    [Key]
    [Column("uuid")]
    public Guid Uuid { get; set; }

    [Column("date")]
    public DateTime Date { get; set; }

    [Column("number")]
    [StringLength(64)]
    public string Number { get; set; }

    [Column("stage_type")]
    public StageTypes StageType { get; set; }

    [Column("stage_name"), StringLength(1024)]
    public string StageName { get; set; }

    [Column("profile_committee")]
    [StringLength(1024)]
    public string ProfileCommittee { get; set; }

    [Column("send_date")]
    public DateTime? SendDate { get; set; }

    public Document Document { get; set; }
  }

  public enum StageTypes
  {
    Project,
    Registration,
    Files,
    Pass,
    Committee,
    Publication
  }
}
