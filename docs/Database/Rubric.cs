using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public class Rubric
  {
    [Key]
    [Column("Uuid")]
    public Guid Uuid { get; set; }

    [Column("id")]
    public int Id { get; set; }

    [Column("due")]
    public string Due { get; set; }

    [Column("name")]
    public string Name { get; set; }
  }
}
