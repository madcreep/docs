using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace docs.Database
{
  public class Session
  {
    private string _numberStr;

    [Key, Column("id")] public long Id { get; set; }

    [Column("Isn")] public int? Isn { get; set; }

    [Column("number_int")] public int NumberInt { get; set; }

    public virtual ICollection<Document> Documents { get; set; }

    [Column("number")]
    [StringLength(128)]
    [Display(Name = "Номер сессии")]
    public string Number
    {
      get { return _numberStr; }
      set
      {
        var resultString = Regex.Match(value, @"\d+").Value;

        NumberInt = int.Parse(resultString);

        _numberStr = value;
      }
    }

    [Column("date")]
    [Display(Name = "Дата сессии")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
    public DateTime? Date { get; set; }

    [Column("session_type")]
    [Display(Name = "Тип сессии")]
    public string SessionType { get; set; }

    [Column("convocation_id")]
    [ForeignKey("Сonvocation")]
    public long ConvocationId { get; set; }

    public virtual Convocation Convocation { get; set; }

    public virtual SessionDocuments SessionDocuments { get; set; }

    /// <summary>
    /// Отметка времени выгрузки
    /// </summary>
    [Column("is_deleted"), Required] public bool IsDeleted { get; set; }

    /// <summary>
    /// Отметка времени выгрузки
    /// </summary>
    [Column("updating_timestamp")] public DateTime? UnloadingTimestamp { get; set; }
  }
}
