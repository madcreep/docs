using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public class SessionDocuments
  {
    [Key, Column("id")] public long Id { get; set; }

    [ForeignKey("Session"), Column("session_id")]
    public long SessionId { get; set; }

    [Column("agenda_isn")]
    public int? AgendaIsn { get; set; }

    [Column("agenda")]
    public string Agenda { get; set; }

    [Column("annotation_isn")]
    public int? AnnotationIsn { get; set; }

    [Column("annotation")]
    public string Annotation { get; set; }

    public virtual Session Session { get; set; }
  }
}
