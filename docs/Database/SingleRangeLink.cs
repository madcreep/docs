using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public class SingleRangeLink
  {
    [Key, Column("id")] public long Id { get; set; }

    [Column("from")] public int From { get; set; }

    [Column("to")] public int To { get; set; }
  }
}
