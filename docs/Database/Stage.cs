using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace docs.Database
{
  public class Stage
  {
    [Key, Column("id")] public long Id { get; set; }

    [Column("name"), StringLength(128)] public string Name { get; set; }

    [Column("reading_number")] public int? Number { get; set; }
  }
}
