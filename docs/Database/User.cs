using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace docs.Database
{
  [Table("users")]
  public class User : IdentityUser<Guid>
  {
    [Column("fullname")] public string FullName { get; set; }

    [Column("not_deleted")] public bool IsNotDeleted { get; set; }

    [Column("only_password_change")] public bool OnlyPasswordChange { get; set; }
  }

  [Table("roles")]
  public class Role : IdentityRole<Guid>
  {
    public Role()
      : base()
    {
    }

    public Role(string roleName)
      : base(roleName)
    {
    }

    [Column("description")]
    [StringLength(255)]
    public string Description { get; set; }


    [Column("specific_right")] public bool SpecificRight { get; set; }
  }
}
