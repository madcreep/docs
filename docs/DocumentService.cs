using System;
using System.Collections.Generic;
using System.Linq;
using docs.Database;
using docs.Model;

namespace docs
{
    public static class DocumentService
    {
        public static DocumentModel ToDocumentModel(this Document document)
        {
            var model = new DocumentModel
            {
                Id = document.Id,
                Uuid = document.Uuid,
                UploadDate = document.UploadDate,
                Date = document.Date,
                Number = document.Number,
                AnticorruptionExpertise = document.AnticorruptionExpertise,
                Content = document.Content,
                ExpertiseEnd = document.ExpertiseEnd,
                ExpertiseStart = document.ExpertiseStart,
                Сorrespondent = document.Correspondent,
                ProfileCommittee = document.ProfileCommittee,
                SessionNumber = document.SessionNumber,
                PublishDate = document.PublishDate,
                ParentId = document.ParentId,
                ParentUuid = document.ParentUuid
            };
            if (document.Rubric != null)
            {
                model.Rubric = ToRubricModel(document.Rubric);
            }

            if (document.Files != null && document.Files.Count > 0)
            {
                model.Files = new List<DocumentFileModel>();
                foreach (var file in document.Files)
                {
                    model.Files.Add(ToDocumentFileModel(file));
                }
            }
            return model;
        }

        public static RubricModel ToRubricModel(this Rubric rubric)
        {
            return new RubricModel
            {
                Id = rubric.Id,
                Due = rubric.Due,
                Uuid = rubric.Uuid,
                Name = rubric.Name
            };
        }

        public static DocumentFileModel ToDocumentFileModel(this DocumentFile file)
        {
            return new DocumentFileModel
            {
                Id = file.Id,
                Uuid = file.Uuid,
                DocumentId = file.Document.Id,
                DocumentUuid = file.Document.Uuid,
                FileName = file.FileName,
                Extension = file.Extension,
                Size = file.Size,
                Description = file.Description,
                OrderNum = file.OrderNum,
                Send = file.Send
            };
        }

        public static ReviewStageModel ToStageModel(this ReviewStage stage)
        {
            return new ReviewStageModel
            {
                Uuid = stage.Uuid,
                Date = stage.Date,
                Number = stage.Number,
                DocumentId = stage.Document.Id,
                DocumentUuid = stage.Document.Uuid,
                ProfileCommittee = stage.ProfileCommittee,
                SendDate = stage.SendDate,
                StageType = stage.StageType
            };
        }
        
        public static Document ToDocument(this DocumentModel model)
        {
            return new Document
            {
                Id = model.Id,
                AnticorruptionExpertise = model.AnticorruptionExpertise,
                Content = model.Content,
                Date = model.Date,
                ExpertiseStart = model.ExpertiseStart,
                ExpertiseEnd = model.ExpertiseEnd,
                Number = model.Number,
                ProfileCommittee = model.ProfileCommittee,
                PublishDate = model.PublishDate,
                UploadDate = model.UploadDate,
                Correspondent = model.Сorrespondent,
                SessionNumber = model.SessionNumber,
                ParentId = model.ParentId,
                ParentUuid = model.ParentUuid
            };
        }

        public static Rubric ToRubric(this RubricModel model)
        {
            return new Rubric
            {
                Name = model.Name,
                Due = model.Due,
                Id = model.Id
            };
        }

        public static DocumentFile ToDocumentFile(this DocumentFileModel model, DocumentsContext dbContext)
        {
            var dbFile = new DocumentFile
            {
                Id = model.Id,
                Uuid = model.Uuid,
                FileName = model.FileName,
                Extension = model.Extension,
                Size = model.Size,
                Description = model.Description,
                OrderNum = model.OrderNum,
                Send = model.Send
            };
            if (dbContext == null)
                return dbFile;
            if (model.DocumentId != 0)
            {
                dbFile.Document = dbContext.Documents.FirstOrDefault(d => d.Id == model.DocumentId);
            }
            else if (model.DocumentUuid != Guid.Empty)
            {
                dbFile.Document = dbContext.Documents.FirstOrDefault(d => d.Uuid == model.DocumentUuid);
            }

            return dbFile;
        }

        public static ReviewStage ToReviewStage(this ReviewStageModel model, DocumentsContext dbContext)
        {
            var stage = new ReviewStage
            {
                Uuid = model.Uuid,
                Date = model.Date,
                Number = model.Number,
                ProfileCommittee = model.ProfileCommittee,
                SendDate = model.SendDate,
                StageType = model.StageType
            };
            if (dbContext == null)
                return stage;
            if (model.DocumentId != 0)
            {
                stage.Document = dbContext.Documents.FirstOrDefault(d => d.Id == model.DocumentId);
            } else if (model.DocumentUuid != Guid.Empty)
            {
                stage.Document = dbContext.Documents.FirstOrDefault(d => d.Uuid == model.DocumentUuid);
            }

            return stage;
        }

    }
}
