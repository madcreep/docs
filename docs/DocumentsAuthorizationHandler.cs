using System;
using System.Data.Common;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using docs.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace docs
{
    public class SpecificRequirement : IAuthorizationRequirement
    {
        public bool SpecificRight { get; }

        public SpecificRequirement(bool specificRight)
        {
            SpecificRight = specificRight;
        }
    }

    public class DocumentsAuthorizationHandler : AuthorizationHandler<SpecificRequirement>
    {
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
            SpecificRequirement requirement)
        {
//            //var principal = AuthService.ValidateToken(context.Session.GetString("JWToken"));
//            using (var dbContext = new DocumentsContext())
//            {
//                var dbUser = await dbContext.Users.FirstOrDefaultAsync(u => u.Email == context.User.Identity.Name);
//                if (dbUser == null)
//                {
//                    return;
//                }
//
//                var principal = context.User.Identity as ClaimsIdentity;
//                var roleClaims = context.User.Claims.Where(c => c.Type == principal.RoleClaimType);
//                var dbRoles = dbContext.Roles.Where(r => roleClaims.Any(name => name.Value == r.Name));
//                if (requirement.SpecificRight == dbRoles.Any(r => r.SpecificRight))
//                {
//                    context.Succeed(requirement);
//                    return;
//                }
//                context.Fail();
//                return;
//
//            }
            if (!context.User.HasClaim(c => c.Type == "SpecificRight" && c.Issuer == "http://www.biz-it.ru"))
            {
                return;
            }

            var right = bool.Parse(context.User.FindFirst(c => c.Type == "SpecificRight").Value);

            if (right == requirement.SpecificRight)
            {
                context.Succeed(requirement);
            }
        }
    }
    
}
