﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace docs
{
    public static class ErrorHandler
    {

        public static IActionResult HandleErrors(this IdentityResult result, ErrorResult receiver, string message = null) {
            var errors = result.Errors.ToDictionary(error => error.Code, error => error.Description);
            return receiver(new { Message = message, Errors = errors});

        }

    }

    public delegate IActionResult ErrorResult(object data);  
}
