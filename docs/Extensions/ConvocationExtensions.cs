using docs.Database;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace docs.Extensions
{
  public static class ConvocationExtensions
  {
    public static IQueryable<Convocation> ById(this IQueryable<Convocation> convocations, long id)
    {
      return convocations
        .Where(el => el.Id == id);
    }

    public static IQueryable<Convocation> WithAllFields(this IQueryable<Convocation> convocations)
    {
      return convocations
        .Include(d => d.Sessions);
    }

    public static IQueryable<Convocation> WithAllFieldsAndDeleted(this IQueryable<Convocation> convocations)
    {
      return convocations
        .Include(d => d.Sessions)
        .IgnoreQueryFilters();
    }

    public static IQueryable<Convocation> ByNumber(this IQueryable<Convocation> convocations, int number)
    {
      return convocations
        .Where(el => el.Number == number);
    }
  }
}
