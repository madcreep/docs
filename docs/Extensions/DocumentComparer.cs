using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using docs.Database;

namespace docs.Extensions
{
    public class DocumentComparer : IComparer<Document>
  {
    public int Compare(Document x, Document y)
    {
      if (x.DocumentType == DocumentType.Decree && y.DocumentType == DocumentType.Law)
      {
        return -1;
      }
      if (x.DocumentType == DocumentType.Law && y.DocumentType == DocumentType.Decree)
      {
        return 1;
      }

      return x.Date.CompareTo(y.Date);
    }
  }
}
