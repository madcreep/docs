using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using docs.Database;

namespace docs.Extensions
{
  public static class DocumentFilesExtensions
  {
    public static IQueryable<DocumentFile> Indexing(this IQueryable<DocumentFile> files)
    {
      return files.Where(el => el.Indexing);
    }

    public static IQueryable<DocumentFile> NotIndexing(this IQueryable<DocumentFile> files)
    {
      return files.Where(el => !el.Indexing);
    }
  }
}
