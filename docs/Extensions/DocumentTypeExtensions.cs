using docs.Database;
using System;

namespace docs.Extensions
{
  public static class DocumentTypeExtensions
  {
    public static string Name(this DocumentType docType)
    {
      switch (docType)
      {
        case DocumentType.Undefined:
          return "Не определено";
        case DocumentType.LawProject:
          return "Проект закона Краснодарского края";
        case DocumentType.DecreeProject:
          return "Проект постановления ЗСК";
        case DocumentType.Letter1:
          return "Рассмотрение проекта в первом чтении";
        case DocumentType.Letter2:
          return "Рассмотрение проекта во втором чтении";
        case DocumentType.Letter3:
          return "Рассмотрение проекта в третьем чтении";
        case DocumentType.Law:
          return "Закон Краснодарского края";
        case DocumentType.Decree:
          return "Постановление Законодательного Собрания Краснодарского края";
        case DocumentType.PlenarySession:
          return "Информация о сессии";
        case DocumentType.Conclusion1:
          return "Заключение на НПА (1 чтение)";
        case DocumentType.Conclusion2:
          return "Заключение на НПА (2 чтение)";
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    public static string SmallName(this DocumentType docType)
    {
      switch (docType)
      {
        case DocumentType.Undefined:
          return "Не определено";
        case DocumentType.LawProject:
          return "Проект закона Краснодарского края";
        case DocumentType.DecreeProject:
          return "Проект постановления ЗСК";
        case DocumentType.Letter1:
          return "Письмо о включении";
        case DocumentType.Letter2:
          return "Письмо о включении 2-е чтение";
        case DocumentType.Letter3:
          return "Письмо о включении 3-е чтение";
        case DocumentType.Law:
          return "Краевой закон";
        case DocumentType.Decree:
          return "Постановление";
        case DocumentType.PlenarySession:
          return "Информация о сессии";
        case DocumentType.Conclusion1:
          return "Заключение на НПА (1 чтение)";
        case DocumentType.Conclusion2:
          return "Заключение на НПА (2 чтение)";
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    public static string FindName(this DocumentType docType)
    {
      switch (docType)
      {

        case DocumentType.LawProject:
          return "Проекты законов Краснодарского края";
        case DocumentType.DecreeProject:
          return "Проекты постановлений";
        case DocumentType.Law:
          return "Краевые законы";
        case DocumentType.Decree:
          return "Постановления";
        case DocumentType.Letter1:
        case DocumentType.Letter2:
        case DocumentType.Letter3:
        case DocumentType.Undefined:
        case DocumentType.PlenarySession:
        case DocumentType.Conclusion1:
        case DocumentType.Conclusion2:
          return string.Empty;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }
  }
}
