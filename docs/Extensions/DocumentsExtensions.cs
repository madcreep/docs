using docs.Database;
using docs.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace docs.Extensions
{
  public static class DocumentsExtensions
  {
    public static IQueryable<Document> WithAllFields(this IQueryable<Document> docs)
    {
      return docs
        .Include(d => d.Session)
        .Include(d => d.Session).ThenInclude(el => el.Convocation)
        .Include(el => el.Rubric)
        .Include(el => el.Reading)
        .Include(el => el.Stage)
        .Include(el => el.Result)
        .Include(el => el.Files)

        .Include(el => el.ParentDocument).ThenInclude(el => el.Session.Convocation)
        .Include(el => el.ParentDocument).ThenInclude(el => el.Rubric)
        .Include(el => el.ParentDocument).ThenInclude(el => el.Reading)
        .Include(el => el.ParentDocument).ThenInclude(el => el.Stage)
        .Include(el => el.ParentDocument).ThenInclude(el => el.Result)
        .Include(el => el.ParentDocument).ThenInclude(el => el.Files)

        .Include(el => el.ChildeDocuments).ThenInclude(el => el.Session.Convocation)
        .Include(el => el.ChildeDocuments).ThenInclude(el => el.Rubric)
        .Include(el => el.ChildeDocuments).ThenInclude(el => el.Reading)
        .Include(el => el.ChildeDocuments).ThenInclude(el => el.Stage)
        .Include(el => el.ChildeDocuments).ThenInclude(el => el.Result)
        .Include(el => el.ChildeDocuments).ThenInclude(el => el.Files);
    }

    public static IQueryable<Document> ById(this IQueryable<Document> docs, int id)
    {
      return docs
        .Where(el => el.Id == id);
    }

    public static IQueryable<Document> ByUuid(this IQueryable<Document> docs, Guid uuid)
    {
      return docs
        .Where(el => el.Uuid == uuid);
    }

    public static IQueryable<Document> OnlyProjects(this IQueryable<Document> docs)
    {
      return docs
        .ByDocumentType(DocumentType.LawProject, DocumentType.DecreeProject);
    }

    public static IQueryable<Document> OnlyFinal(this IQueryable<Document> docs)
    {
      return docs
        .ByDocumentType(DocumentType.Law, DocumentType.Decree);
    }

    public static IQueryable<Document> OnlyForLanSite(this IQueryable<Document> docs, bool forLanSite)
    {
      return forLanSite
        ? docs.IgnoreQueryFilters()
        : docs;
    }

    public static IQueryable<Document> ByDocumentType(this IQueryable<Document> docs, params DocumentType[] types)
    {
      return types == null || types.Length == 0
        ? docs
        : docs.Where(el => types.Contains(el.DocumentType));
    }

    public static IQueryable<Document> Independent(this IQueryable<Document> docs)
    {
      return docs
        .Where(el => (el.DocumentType == DocumentType.LawProject && el.ChildeDocuments.All(x=>x.DocumentType != DocumentType.Law))
                     || (el.DocumentType == DocumentType.DecreeProject && el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Decree))
                     || el.DocumentType == DocumentType.Decree
                     || el.DocumentType == DocumentType.Law);
    }

    public static IEnumerable<Document> ByDocumentType(this IEnumerable<Document> docs, params DocumentType[] types)
    {
      return types == null || types.Length == 0
        ? docs
        : docs.Where(el => types.Contains(el.DocumentType));
    }

    public static IQueryable<Document> Displayed(this IQueryable<Document> docs, bool? displayed = true)
    {
      if (displayed.HasValue)
      {
        return docs
          .Where(el => el.DisplayOnTheSite == displayed.Value || el.ParentDocument.DisplayOnTheSite == displayed.Value);
      }

      return docs;
    }

    public static IQueryable<Document> BySessionId(this IQueryable<Document> docs, long sessionId)
    {
      return docs
        .Where(el => el.SessionId == sessionId);
    }

    public static IQueryable<Document> Find(this IQueryable<Document> docs, FindFormModel findForm)
    {
      if (findForm.NpaFind.HasValue)
      {
        var npaFindValue = findForm.NpaFind.Value;

        switch (npaFindValue)
        {
          case DocumentType.LawProject:
            docs = docs
              .Where(el => el.DocumentType == DocumentType.LawProject && el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Law));
            break;
          case DocumentType.DecreeProject:
            docs = docs
              .Where(el => el.DocumentType == DocumentType.DecreeProject && el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Decree));
            break;
          case DocumentType.Law:
          case DocumentType.Decree:
            docs = docs.ByDocumentType(npaFindValue);
            break;
          default:
            throw new ArgumentOutOfRangeException();
        }
      }
      else
      {
        docs =
          docs 
            .Where(el => el.DocumentType == DocumentType.Law
                         || el.DocumentType == DocumentType.Decree
                         || (el.DocumentType == DocumentType.DecreeProject &&
                             el.ChildeDocuments.All(x =>
                               x.DocumentType != DocumentType.Decree))
                         || (el.DocumentType == DocumentType.LawProject &&
                             el.ChildeDocuments.All(x =>
                               x.DocumentType != DocumentType.Law))
            );
      }

      if (findForm.HasExpertizeFind)
      {
        docs = docs
          .Where(el=>el.DocumentType == DocumentType.DecreeProject
                     || el.DocumentType == DocumentType.LawProject
                     || el.DocumentType == DocumentType.Law
                     || (el.DocumentType == DocumentType.Decree && (el.ParentDocument == null || el.ParentDocument.DocumentType == DocumentType.DecreeProject)))
          .Where(el => (el.ParentDocument == null && el.AnticorruptionExpertise) || el.ParentDocument.AnticorruptionExpertise);

        if (findForm.DateExpertizeFromFind.HasValue)
        {
          docs = docs
            .Where(el => (el.ParentDocument == null && el.ExpertiseEnd >= findForm.DateExpertizeFromFind.Value) || el.ParentDocument.ExpertiseEnd >= findForm.DateExpertizeFromFind.Value);
        }

        if (findForm.DateExpertizeToFind.HasValue)
        {
          docs = docs
            .Where(el => (el.ParentDocument == null && el.ExpertiseStart <= findForm.DateExpertizeToFind.Value) || el.ParentDocument.ExpertiseStart <= findForm.DateExpertizeToFind.Value);
        }
      }

      if (findForm.DateFind.HasValue && !findForm.SessionFind.HasValue)
      {
        var fromDate = new DateTime(findForm.DateFind.Value, 1, 1);
        var toDate = fromDate.AddYears(1);

        docs = docs
          .Where(el => (el.Date >= fromDate && el.Date < toDate)
                       || el.ChildeDocuments.Any(x => x.Date >= fromDate && x.Date < toDate));
      }

      if (!string.IsNullOrWhiteSpace(findForm.NumberActFind))
      {
        var number = findForm.NumberActFind.Trim();

        docs = docs
          .Where(el => ((el.DocumentType == DocumentType.Law || el.DocumentType == DocumentType.Decree) && el.Number.Contains(number))
                       || (el.DocumentType == DocumentType.LawProject
                           && (el.ChildeDocuments.FirstOrDefault(x => x.DocumentType == DocumentType.Law).Number
                             .Contains(number)))
                       || (el.DocumentType == DocumentType.DecreeProject && (el.ChildeDocuments.OrderBy(x => x.Date)
                             .FirstOrDefault(x => x.DocumentType == DocumentType.Decree).Number.Contains(number)))
                       || (el.DocumentType == DocumentType.LawProject &&
                           el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Law) &&
                           el.TechNumber.Contains(number))
                       || (el.DocumentType == DocumentType.DecreeProject &&
                           el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Decree) &&
                           el.TechNumber.Contains(number))
          );
      }

      if (!string.IsNullOrWhiteSpace(findForm.SubjectFind))
      {
        var subject = findForm.SubjectFind.Trim();

        //docs = docs
        //  .Where(el => (el.DocumentType == DocumentType.LawProject
        //                && (el.ChildeDocuments.FirstOrDefault(x => x.DocumentType == DocumentType.Law).Correspondent ==
        //                    null
        //                  ? el.Correspondent == subject
        //                  : el.ChildeDocuments.FirstOrDefault(x => x.DocumentType == DocumentType.Law).Correspondent ==
        //                    subject)
        //               )
        //               ||
        //               (el.DocumentType == DocumentType.DecreeProject
        //                && (el.ChildeDocuments.OrderByDescending(x => x.Date)
        //                      .FirstOrDefault(x => x.DocumentType == DocumentType.Law).Correspondent == null
        //                  ? el.Correspondent == subject
        //                  : el.ChildeDocuments.OrderByDescending(x => x.Date)
        //                      .FirstOrDefault(x => x.DocumentType == DocumentType.Law).Correspondent == subject)
        //               )
        //               ||
        //               ((el.DocumentType == DocumentType.Law || el.DocumentType == DocumentType.Decree)
        //                && el.Correspondent == null
        //                 ? el.ParentDocument.Correspondent == subject
        //                 : el.Correspondent == subject
        //               )
        //  );

        docs = docs
          .Where(el => el.SubjectOfLaw == subject
                       || (el.SubjectOfLaw == null && (el.Correspondent == subject
                                                       || (el.Correspondent == null && el.ParentDocument != null
                                                                                    && (el.ParentDocument.SubjectOfLaw == subject
                                                                                        || (el.ParentDocument.SubjectOfLaw == null && el.ParentDocument.Correspondent == subject)
                                                                                       )
                                                          )
                                                      )
                          )
          );
      }

      if (findForm.SessionFind.HasValue)
      {
        docs = docs
          .Where(el => ((el.DocumentType == DocumentType.Law || el.DocumentType == DocumentType.Decree)
                       && el.SessionId == findForm.SessionFind.Value)
          || (el.DocumentType == DocumentType.DecreeProject && el.ChildeDocuments.All(x=>x.DocumentType != DocumentType.Decree) && el.ChildeDocuments.Any(x=>x.SessionId == findForm.SessionFind.Value))
          || (el.DocumentType == DocumentType.LawProject
              && el.ChildeDocuments.All(x => x.DocumentType != DocumentType.Law)
              && !el.ChildeDocuments.Any(x=>x.DocumentType == DocumentType.Decree
                                           && x.SessionId == findForm.SessionFind.Value)
              && el.ChildeDocuments.Any(x => x.SessionId == findForm.SessionFind.Value)));
      }

      return docs;
    }

    public static IQueryable<Document> Displayed(this IQueryable<Document> docs)
    {
      //return docs.Where(el => el.DisplayOnTheSite && (el.ParentDocument == null || el.ParentDocument.DisplayOnTheSite));
      return docs.Where(el => el.DisplayOnTheSite);
    }

    public static IQueryable<Document> NotDisplayed(this IQueryable<Document> docs)
    {
      return docs.Where(el => !el.DisplayOnTheSite);
    }

    public static IQueryable<Document> Indexing(this IQueryable<Document> docs)
    {
      return docs.Where(el => el.Indexing);
    }


    public static IQueryable<Document> NotIndexing(this IQueryable<Document> docs)
    {
      return docs.Where(el => !el.Indexing);
    }

    public static ICollection<Document> SortedChildeDocuments(this Document doc)
    {
      if (doc.ChildeDocuments == null)
      {
        return new List<Document>();
      }

      return doc.ChildeDocuments
        .OrderBy(el => el.Date)
        .ToList();
    }

    public static ICollection<DocumentFile> SortedFiles(this Document doc)
    {
      if (doc.Files == null)
      {
        return new List<DocumentFile>();
      }

      return doc.Files
        .OrderBy(el => el.OrderNum)
        .ToList();
    }

    public static bool IsLetter(this Document doc)
    {
      return doc.DocumentType == DocumentType.Letter1
             || doc.DocumentType == DocumentType.Letter2
             || doc.DocumentType == DocumentType.Letter3;
    }

    public static IEnumerable<Document> SortByDocType(this IEnumerable<Document> docs)
    {
      if (docs == null || !docs.Any())
      {
        return docs;
      }

      var tmp = new List<Document>(docs.Count());

      DocumentType type;

      tmp.AddRange(docs
        .Where(el => el.DocumentType == DocumentType.Law)
        .OrderByDescending(el => el.Date)
        .ToArray());

      var allDecree = docs
        .Where(el => el.DocumentType == DocumentType.Decree)
        .ToList();

      for (var i = 2; i >= 0; i--)
      {
        switch (i)
        {
          case 0:
            type = DocumentType.Letter1;
            break;
          case 1:
            type = DocumentType.Letter2;
            break;
          case 2:
            type = DocumentType.Letter3;
            break;
          default:
            type = DocumentType.Undefined;
            break;
        }

        var letters = docs
          .Where(el => el.DocumentType == type)
          .OrderByDescending(el => el.Date)
          .ToArray();

        if (!letters.Any())
        {
            continue;
        }

        var minDate = letters.Min(el => el.Date);

        var decree = allDecree
          .Where(el => el.DocumentType == DocumentType.Decree && el.Date >= minDate)
          .OrderByDescending(el => el.Date)
          .ThenByDescending(el => el.Stage?.Number)
          .ToArray();

        foreach (var delDoc in decree)
        {
          allDecree.Remove(delDoc);
        }
        
        tmp.AddRange(decree);

        tmp.AddRange(letters);
      }

      if (allDecree.Any())
      {
        var lastDecree = allDecree
          .Where(el => el.DocumentType == DocumentType.Decree)
          .OrderByDescending(el => el.Date)
          .ThenByDescending(el=>el.Stage?.Number)
          .ToArray();

        tmp.AddRange(lastDecree);
      }

      tmp.Reverse();

      return tmp;
    }

    public static IEnumerable<DocumentFile> OnlyIsSend(this IEnumerable<DocumentFile> fColl)
    {
      return fColl.Where(el => el.Send);
    }
  }
}
