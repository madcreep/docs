using docs.Models;
using System.Linq;
using docs.Database;

namespace docs.Extensions
{
  public static class EmailMessagesExtantions
  {
    /// <summary>
    /// Нуждаются в отправке
    /// </summary>
    public static IQueryable<EmailMessage> ById(this IQueryable<EmailMessage> eMails, long id)
    {
      return eMails
        .Where(el => el.Id == id);
    }

    /// <summary>
    /// Нуждаются в отправке
    /// </summary>
    public static IQueryable<EmailMessage> NeedSend(this IQueryable<EmailMessage> eMails)
    {
      return eMails
        .Where(el => el.NeedSend);
    }

    /// <summary>
    /// Не отправленные сообщения
    /// </summary>
    public static IQueryable<EmailMessage> NotSent(this IQueryable<EmailMessage> eMails)
    {
      return eMails
        .Where(el => !el.IsSent);
    }

    /// <summary>
    /// Отправленные сообщения
    /// </summary>
    public static IQueryable<EmailMessage> Sent(this IQueryable<EmailMessage> eMails)
    {
      return eMails
        .Where(el => el.IsSent);
    }
  }
}
