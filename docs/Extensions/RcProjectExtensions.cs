using System;
using docs.Database;
using docs.Model;
using docs.Models;

namespace docs.Extensions
{
  public static class RcProjectExtensions
  {
    public static DocumentModel ToDocumentModel(this RcProject rc)
    {
      var model = new DocumentModel
      {
        Id = rc.Isn,
        UploadDate = DateTime.Now,
        Date = rc.RegistrationDate,
        Number = rc.TechNumber,
        AnticorruptionExpertise = rc.AntiCorruptionExpertise != null,
        ExpertiseStart = rc.AntiCorruptionExpertise?.From,
        ExpertiseEnd = rc.AntiCorruptionExpertise?.To,
        Content = rc.Content,
        Сorrespondent = rc.Correspondent,
        ProfileCommittee = rc.ProfCommittee,
        PublishDate = rc.PublicationDate,
        ParentId = rc.ParentIsn
      };

      return model;
    }

    public static Document ToDocument(this RcProject model)
    {
      return new Document
      {
        Id = model.Isn,
        ParentId = model.ParentIsn,
        DocumentType = model.RcType,
        Order = model.Order,
        AnticorruptionExpertise = model.AntiCorruptionExpertise != null,
        ExpertiseStart = model.AntiCorruptionExpertise?.From,
        ExpertiseEnd = model.AntiCorruptionExpertise?.To,
        Content = model.Content,
        Date = model.RegistrationDate,
        Number = model.RegistrationNumber,
        TechNumber = model.TechNumber,
        ProfileCommittee = model.ProfCommittee,
        PublishDate = model.PublicationDate,
        UploadDate = DateTime.Now,
        Correspondent = model.Correspondent,
        CorrespondentIsZsk = model.CorrespondentIsZsk,
        
        SubjectOfLaw = model.SubjectOfLaw,
        InsDate = model.InsDate,

        PlaceOfPublication = model.PlaceOfPublication,

        UnloadingTimestamp = model.UnloadingTimestamp,

        //Publish = model.Publish
      };
    }
  }
}
