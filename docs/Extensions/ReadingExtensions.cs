using docs.Database;
using System.Linq;

namespace docs.Extensions
{
  public static class ReadingExtensions
  {
    public static IQueryable<Reading> ByName(this IQueryable<Reading> readings, string name)
    {
      return readings
        .Where(el => el.Name == name);
    }
  }
}
