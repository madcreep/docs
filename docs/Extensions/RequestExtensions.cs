using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace docs.Extensions
{
    public static class RequestExtensions
    {
    private static bool IsInternetExplorer(string userAgent)
    {
      return (userAgent.Contains("MSIE") || userAgent.Contains("Trident"));
    }

    // Extension for Request
    public static bool IsInternetExplorer(this HttpRequest req)
    {
      return IsInternetExplorer(req.Headers["User-Agent"]);
    }
  }
}
