using docs.Database;
using System.Linq;

namespace docs.Extensions
{
  public static class ResultsExtensions
  {
    public static IQueryable<Result> ByName(this IQueryable<Result> results, string name)
    {
      return results
        .Where(el => el.Name == name);
    }
  }
}
