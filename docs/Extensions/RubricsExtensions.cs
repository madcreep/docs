using docs.Database;
using System.Linq;

namespace docs.Extensions
{
  public static class RubricsExtensions
  {
    public static IQueryable<Rubric> ByName(this IQueryable<Rubric> rubrics, string name)
    {
      return rubrics
        .Where(el => el.Name == name);
    }
  }
}
