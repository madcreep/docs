using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using docs.Database;
using docs.Model;
using Microsoft.EntityFrameworkCore;

namespace docs.Extensions
{
  public static class SessionExtensions
  {
    public static string ComplexNumber(this docs.Database.Session session)
    {
      return
        $"{session.Number}{(session.Date.HasValue ? $" ({session.Date.Value.ToShortDateString()})" : string.Empty)}";
    }

    public static IQueryable<docs.Database.Session> WithAllFields(this IQueryable<docs.Database.Session> sessions)
    {
      return sessions
        .Include(d => d.Convocation)
        .Include(el=>el.Documents);
    }

    public static IQueryable<docs.Database.Session> ById(this IQueryable<docs.Database.Session> sessions, long id)
    {
      return sessions
        .Where(el => el.Id == id);
    }

    public static IQueryable<docs.Database.Session> ByIds(this IQueryable<docs.Database.Session> sessions, params long[] ids)
    {
      if (ids == null || !ids.Any())
      {
        return sessions;
      }

      return sessions
        .Where(el => ids.Contains(el.Id));
    }

    public static IQueryable<docs.Database.Session> DyDateRange(this IQueryable<docs.Database.Session> sessions, DateTime from, DateTime to)
    {
      return sessions
        .Where(el => el.Date >= from && el.Date <= to);
    }

    public static IQueryable<docs.Database.Session> BySessionNumber(this IQueryable<docs.Database.Session> sessions, int sessionNumber, DateTime? sessionDate = null)
    {
      return sessions
        .Where(el => el.NumberInt == sessionNumber && (sessionDate == null || el.Date == sessionDate));
    }

    public static IQueryable<docs.Database.Session> WithDocuments(this IQueryable<docs.Database.Session> sessions, bool allDocumentsInfo = true)
    {
      if (allDocumentsInfo)
      {
        return sessions
          .Include(el => el.Documents);
      }

      return sessions
        .Include(el => el.Documents).ThenInclude(el => el.Rubric)
        .Include(el => el.Documents).ThenInclude(el => el.Reading)
        .Include(el => el.Documents).ThenInclude(el => el.Stage)
        .Include(el => el.Documents).ThenInclude(el => el.Result)
        .Include(el => el.Documents).ThenInclude(el => el.ChildeDocuments)
        .Include(el => el.Documents).ThenInclude(el => el.Files);
    }

    public static IQueryable<docs.Database.Session> ByConvocationId(this IQueryable<docs.Database.Session> sessions, long id)
    {
      return sessions
        .Where(el => el.ConvocationId == id);
    }

    public static docs.Database.Session ToDbSession(this docs.Models.Session session, Convocation convocation)
    {
      var result = new docs.Database.Session
      {
        Number = session.Number,
        Date = session.Date,
        SessionType = session.SessionType,
        Convocation = convocation,
        Documents = new List<Document>()
      };

      return result;
    }
  }
}
