using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using docs.Database;

namespace docs.Extensions
{
  public static class StagesExtensions
  {
    public static IQueryable<Stage> ByName(this IQueryable<Stage> stages, string name)
    {
      return stages.Where(el => el.Name == name);
    }
  }
}
