using System.Text.RegularExpressions;

namespace docs.Extensions
{
  public static class StringExtensions
  {
    public static int? ExtractNumber(this string str)
    {
      if (string.IsNullOrWhiteSpace(str))
      {
        return null;
      }

      var regex = new Regex(@"\d+");

      var match = regex.Match(str);

      if (!match.Success)
      {
        return null;
      }

      if (int.TryParse(match.Value, out var result))
      {
        return result;
      }

      return null;
    }
  }
}
