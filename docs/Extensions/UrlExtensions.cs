using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace docs.Extensions
{
    public static class UrlExtensions
    {
      public static string AbsoluteContent(this IUrlHelper url, string contentPath)
      {
        var request = url.ActionContext.HttpContext.Request;
        return new Uri(new Uri(request.Scheme + "://" + request.Host.Value), url.Content(contentPath)).ToString();
      }
  }
}
