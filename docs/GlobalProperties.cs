using System;
namespace docs
{
    public static class GlobalProperties
    {
        public static string ConnectionString { get; set; }

        public static string AppUrl { get; set; }
        
        public static string StoragePath { get; set; }

        public static int PageSize { get; set; }

        public static string MainSiteBaseUrl { get; set; }

        public static string StartYear { get; set; }
    }
}
