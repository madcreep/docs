using System.Collections.Generic;
using docs.Database;
using docs.Extensions;
using docs.Managers.ElasticSearch;
using docs.Models;
using log4net;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace docs.Helpers.Find
{
  public class FindDocumentsHelper
  {
    public static async Task<FindDocumentsResult> FindAsync(DocumentsContext _context, FindFormModel model, ElasticManager elasticManager = null, bool forLanSite = true)
    {
      var logger = LogManager.GetLogger(typeof(FindDocumentsHelper));

      var pageSize = GlobalProperties.PageSize;

      var result = new FindDocumentsResult
      {
        Page = model.Page
      };

      var documents = _context.Documents
        .OnlyForLanSite(forLanSite)
        .Displayed()
        .WithAllFields()
        .Find(model);

      if (string.IsNullOrWhiteSpace(model.ContextFind))
      {
        documents = documents
          .OrderByDescending(el => el.Date)
          .ThenBy(el => el.DocumentType)
          .ThenBy(el => el.DocumentType == DocumentType.DecreeProject || el.DocumentType == DocumentType.LawProject
            ? el.TechNumber
            : el.Number);

        result.Count = await documents.CountAsync();

        if (model.Page.HasValue)
        {
          documents = documents
            .Skip(pageSize * (model.Page.Value - 1))
            .Take(pageSize);
        }

        result.Documents = await documents.ToArrayAsync();
      }
      else
      {
        var docsIds = await elasticManager.SearchAsync(model.ContextFind);

        var docUuidList = docsIds.Keys;

        var docs = await documents
          .Where(el => docUuidList.Contains(el.Uuid))
          .Distinct()
          .ToArrayAsync();

        docs = docs
          .Join(docsIds,
            d => d.Uuid,
            i => i.Key,
            (d, i) => new
            {
              doc = d,
              score = i.Value
            })
          .OrderByDescending(el => el.score)
          .Select(el => el.doc)
          .ToArray();

        if (docs.Any())
        {
          logger.ErrorFormat("FindAsync => first docs = {0}", docs.First().Uuid);
        }

        result.Count = docs.Length;

        if (model.Page.HasValue)
        {
          docs = docs
            .Skip(pageSize * (model.Page.Value - 1))
            .Take(pageSize)
            .ToArray();
        }

        result.Documents = docs;

      }

      //result.Count = await documents.CountAsync();

      //if (model.Page.HasValue)
      //{
      //  documents = documents
      //    .Skip(pageSize * (model.Page.Value - 1))
      //    .Take(pageSize);
      //}

      //result.Documents = await documents.ToArrayAsync();

      if (!string.IsNullOrWhiteSpace(model.ContextFind) && result.Documents.Any())
      {
        logger.ErrorFormat("FindAsync => first result.Documents = {0}", result.Documents.First().Uuid);
      }

      return result;
    }


    public static async Task<FindDocumentsResult> Find1Async(DocumentsContext _context, FindFormModel model, ElasticManager elasticManager = null)
    {
      var result = new FindDocumentsResult
      {
        Page = model.Page
      };

      var documents = _context.Documents
        .Displayed()
        .WithAllFields();

      if (model.IsEmpty || !model.NpaFind.HasValue)
      {
        documents = documents
          .Where(el =>
            (el.DocumentType == DocumentType.LawProject &&
             el.ChildeDocuments.Any(x => x.DocumentType == DocumentType.Law))
            || (el.DocumentType == DocumentType.DecreeProject &&
                el.ChildeDocuments.Any(x => x.DocumentType == DocumentType.Decree))
            || (el.DocumentType == DocumentType.Decree && el.ParentDocument.DocumentType == DocumentType.LawProject));

        if (model.DateFind.HasValue)
        {
          documents = documents
            .Find(model);
        }
      }
      else
      {
        documents = documents
          .Where(el =>
            (el.DocumentType == DocumentType.LawProject)
            || (el.DocumentType == DocumentType.DecreeProject)
            || (el.DocumentType == DocumentType.Decree && el.ParentDocument.DocumentType == DocumentType.LawProject))
          .Find(model);
      }

      if (string.IsNullOrWhiteSpace(model.ContextFind))
      {
        documents = documents
          .OrderByDescending(el => el.Date);
      }
      else
      {
        var docsIds = await elasticManager.SearchAsync(model.ContextFind);

        documents = from d in documents
                    join ed in docsIds on d.Uuid equals ed.Key
                    orderby ed.Value descending
                    select d;
      }

      result.Count = await documents.CountAsync();

      if (model.Page.HasValue)
      {
        var pageSize = GlobalProperties.PageSize;

        documents = documents
          .Skip(pageSize * (model.Page.Value - 1))
          .Take(pageSize);
      }

      result.Documents = await documents.ToArrayAsync();

      return result;
    }
  }
}
