using docs.Database;
using docs.Extensions;
using docs.Models;
using System.Text;

namespace docs.Helpers.Mail
{
  internal class AntiCorruptionMessage : IEmailMessage
  {
    public AntiCorruptionMessage(Document doc, string fileDirectoryPath)
    {
      MessageSubject = $"Антикоррупционная экспертиза (проект № {doc.FinalDocumentNumber})";
      MessageBody = GenerateBody(doc);
      IsBodyHtml = true;
      ToSectionName = doc.ProfileCommittee;
      FileDirectory = fileDirectoryPath;
    }

    private static string GenerateBody(Document doc)
    {
      var sb = new StringBuilder(512);

      sb.AppendLine("<h2>Антикоррупционная экспертиза</h2>");
      sb.AppendLine(
        $"<p>{doc.FinalDocumentType.Name()} № {doc.FinalDocumentNumber} от {doc.FinalDate.ToString("dd:MM:yyyy")}</p>");

      sb.AppendLine(
        $"<p>Профильный комитет: <b>{doc.ProfileCommittee}</b></p>");

      sb.AppendLine("<br/><br/>");
      sb.AppendLine("<p>Передано с сайта kubzsk.ru</p>");

      return sb.ToString();
    }

    public string FromSectionName { get; set; }
    public string ToSectionName { get; set; }
    public string MessageSubject { get; set; }
    public string MessageBody { get; set; }
    public bool IsBodyHtml { get; set; }
    public string FileDirectory { get; set; }
  }
}
