using System.Net.Mail;

namespace docs.Helpers.Mail
{
  public interface IMailMessage
  {
    MailMessage Message { get; }
  }
}
