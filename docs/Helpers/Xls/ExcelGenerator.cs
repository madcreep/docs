using System;
using docs.Database;
using Syncfusion.XlsIO;
using System.IO;
using System.Linq;
using Syncfusion.Drawing;

namespace docs.Helpers.Xls
{
  public static class ExcelGenerator
  {
    static ExcelGenerator()
    {
      Syncfusion.Licensing.SyncfusionLicenseProvider
        .RegisterLicense("MTU3MDMwQDMxMzcyZTMzMmUzMEsvMEU3eTAzUTBQTFQ5Ym5MSEtCUUtIdUI1czF5MDF6SGFFVkJJaDBVdjQ9");
    }

    private const string DateEmptyString = "__.__.____";

    public static Stream GenerateSessionDocumentsSummary(Document[] docs, string[] title)
    {


      using (var excelEngine = new ExcelEngine())
      {
        var application = excelEngine.Excel;

        application.DefaultVersion = ExcelVersion.Excel2013;

        var workbook = application.Workbooks.Create(1);

        var worksheet = workbook.Worksheets[0];

        worksheet.Range[1, 1, 1, 5].Merge();

        worksheet.Range[1, 1].Text = string.Join(Environment.NewLine, title);


        worksheet.Range[2, 1].Text = "№";
        worksheet.Range[2, 2].Text = "Дата";
        worksheet.Range[2, 3].Text = "Наименование";
        worksheet.Range[2, 4].Text = "Экспертиза";
        worksheet.Range[2, 5].Text = "Опубликовано (размещено)";


        if (docs != null && docs.Any())
        {
          for (var i = 0; i < docs.Count(); i++)
          {
            var doc = docs[i];

            var row = i + 3;

            worksheet.Range[row, 1].Text = doc.FinalDocumentNumber;
            worksheet.Range[row, 2].Text = doc.FinalDate.ToString("dd.MM.yyyy");

            string contentPrefix;

            switch (doc.FinalDocumentType)
            {
              case DocumentType.Law:
                contentPrefix = "Закон Краснодарского края";
                break;
              case DocumentType.Decree:
                contentPrefix = "Постановление Законодательного Собрания Краснодарского края";
                break;
              case DocumentType.LawProject:
                contentPrefix = "Проект закона Краснодарского края";
                break;
              case DocumentType.DecreeProject:
                contentPrefix = "Прект постановления Законодательного Собрания Краснодарского края";
                break;
              case DocumentType.Undefined:
              case DocumentType.Letter1:
              case DocumentType.Letter2:
              case DocumentType.Letter3:
              case DocumentType.PlenarySession:
                contentPrefix = null;
                break;
              default:
                throw new ArgumentOutOfRangeException();
            }

            worksheet.Range[row, 3].Text = contentPrefix == null ? doc.FinalContent : $"{contentPrefix} \"{doc.FinalContent}\"";

            var expertizeDoc = doc.ParentDocument ?? doc;

            if (expertizeDoc.AnticorruptionExpertise)
            {
              worksheet.Range[row, 4].Text = $"{expertizeDoc.ExpertiseStart?.ToString("dd.MM.yyyy") ?? DateEmptyString} - {expertizeDoc.ExpertiseEnd?.ToString("dd.MM.yyyy") ?? DateEmptyString}";
            }

            worksheet.Range[row, 5].Text = $"{(doc.DocumentType == DocumentType.DecreeProject || doc.DocumentType == DocumentType.Decree ? "опубликовано" : "размещен")} {doc.FinalPublishDate?.ToString("dd.MM.yyyy") ?? DateEmptyString}";
          }
        }

        FormatSessionSummary(worksheet, docs?.Length ?? 0, title.Length);

        Stream stream = new MemoryStream();

        workbook.SaveAs(stream);

        return stream;
      }
    }

    private static void FormatSessionSummary(IWorksheet worksheet, int docsCount, int titleStringCount)
    {
      var titleStyle = worksheet.Workbook.Styles.Add("TitleStyle");
      titleStyle.BeginUpdate();
      titleStyle.Color = Color.AliceBlue;
      titleStyle.Font.Size = 16;
      titleStyle.Font.Bold = true;
      titleStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
      titleStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;
      titleStyle.WrapText = true;
      titleStyle.EndUpdate();

      var titleRange = worksheet.Range[1, 1];
      titleRange.CellStyle = titleStyle;
      
      var headerStyle = worksheet.Workbook.Styles.Add("HeaderStyle");
      headerStyle.BeginUpdate();
      headerStyle.Font.Size = 14;
      headerStyle.Font.Bold = true;
      headerStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
      headerStyle.EndUpdate();

      var headerRange = worksheet.Range[2, 1, 2, 5];
      headerRange.CellStyle = headerStyle;

      var centerStyle = worksheet.Workbook.Styles.Add("CenterStyle");
      centerStyle.BeginUpdate();
      centerStyle.Font.Size = 11;
      centerStyle.Font.Bold = false;
      centerStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
      centerStyle.VerticalAlignment = ExcelVAlign.VAlignTop;
      centerStyle.EndUpdate();

      var countAddRows = docsCount == 0 ? 0 : docsCount - 1;

      worksheet.Range[3, 1, 3 + countAddRows, 2].CellStyle = centerStyle;
      worksheet.Range[3, 4, 3 + countAddRows, 5].CellStyle = centerStyle;

      var nameStyle = worksheet.Workbook.Styles.Add("NameStyle");
      nameStyle.BeginUpdate();
      nameStyle.Font.Size = 11;
      nameStyle.Font.Bold = false;
      nameStyle.HorizontalAlignment = ExcelHAlign.HAlignLeft;
      nameStyle.VerticalAlignment = ExcelVAlign.VAlignTop;
      nameStyle.WrapText = true;
      nameStyle.EndUpdate();

      worksheet.Range[3, 3, 3 + countAddRows, 3].CellStyle = nameStyle;

      worksheet.AutofitColumn(1);
      worksheet.AutofitColumn(2);
      worksheet.SetColumnWidth(3, 120);
      worksheet.AutofitColumn(4);
      worksheet.AutofitColumn(5);
      worksheet.SetRowHeight(1, 60);

      worksheet.Range[3, 1, 3 + countAddRows, 5].AutofitRows();

      worksheet.Rows[0].RowHeight = titleStringCount * titleStyle.Font.Size * 1.5;
    }
  }
}
