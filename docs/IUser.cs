﻿using System;
namespace docs
{
    public interface IUser
    {
        string Email { get; set; }
        string Password { get; set; }
    }
}
