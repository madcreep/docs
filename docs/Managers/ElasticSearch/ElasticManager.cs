using docs.Database;
using docs.Extensions;
using docs.Models;
using log4net;
using Microsoft.EntityFrameworkCore;
using org.bit.ElasticSearch;
using org.bit.ElasticSearch.Models;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace docs.Managers.ElasticSearch
{
  public class ElasticManager
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(ElasticManager));

    private const string DocumentType = "doc";
    private const string FileType = "file";
    private const int PageSize = 20;

    private readonly object _syncRoot = new object();
    private readonly ElasticSearchManager _manager;

    private readonly string _indexForDoc;
    private readonly string _indexForFile;

    private int? _reindexPercent;

    public int? ReindexPercent
    {
      get
      {
        lock (_syncRoot)
        {
          return _reindexPercent;
        }
      }
      private set
      {
        lock (_syncRoot)
        {
          _reindexPercent = value.HasValue ? value > 100
            ? 100
            : value < 0
              ? 0
              : value
            : null;
        }
      }
    }

    public bool Inited { get; }

    public ElasticManager(string url, string indexForDoc, string indexForFile)
    {
      try
      {
        _indexForDoc = indexForDoc;

        _indexForFile = indexForFile;

        _manager = new ElasticSearchManager(url);

        var task = ReinitAsync();

        task.Wait();

        Inited = task.Result;
      }
      catch
      {
        Inited = false;
      }
    }

    public async Task<bool> ReinitAsync()
    {
      return await InitAsync(_indexForDoc) && await InitAsync(_indexForFile);
    }

    private async Task<bool> InitAsync(string index)
    {
      if (!_manager.HasIndex(index))
      {
        return await _manager.CreateIndexAsync(index)
               && await _manager.SetIndexAnalyzerAsync(index, "russian");
      }

      var analyzerOptions = await _manager.GetIndexAnalyzerOptionsAsync(index);

      if (analyzerOptions != null
          && analyzerOptions.Type == "standard"
          && analyzerOptions.Stopwords == "_russian_"
          && analyzerOptions.MaxTokenLength == 5)
      {
        return true;
      }

      return await _manager.SetIndexAnalyzerAsync(index, "russian");
    }

    public async Task<bool> AddAsync(Document doc, bool withFiles = false)
    {
      var context = new DocumentsContext();

      //if (doc == null
      //    || (doc.DocumentType != Database.DocumentType.DecreeProject
      //        && doc.DocumentType != Database.DocumentType.LawProject))
      //{
      //  return false;
      //}

      context.Documents.Attach(doc);

      context.Entry(doc).State = EntityState.Modified;

      return await AddAsync(context, doc, withFiles, true);
    }

    private async Task<bool> AddAsync(DocumentsContext context, Document doc, bool withFiles, bool saveChanges)
    {
      var result = true;

      var id = doc.Uuid.ToString();

      var type = new ElasticDocument
      {
        Type = ElasticType.Document,
        DocType = doc.DocumentType,
        Date = doc.Date,
        Number = doc.DocumentType == Database.DocumentType.DecreeProject || doc.DocumentType == Database.DocumentType.LawProject
                   ? string.IsNullOrWhiteSpace(doc.TechNumber) ? doc.Number : doc.TechNumber : doc.Number,
        Session = doc.Session?.ComplexNumber(),
        SubjectOfLaw = doc.SubjectOfLaw,
        AnticorruptionExpertise = doc.AnticorruptionExpertise,
        ExpertiseStart = doc.ExpertiseStart,
        ExpertiseEnd = doc.ExpertiseEnd,
        Title = doc.DocumentType.Name(),
        Content = doc.Content
      };

      var addResult = await _manager.AddTypeObjectAsync(_indexForDoc, DocumentType, id, type);

      doc.Indexing = addResult;

      result &= addResult;

      if (withFiles)
      {
        foreach (var file in doc.Files ?? await GetFilesAsync(context, doc.Uuid))
        {
          result &= await AddAsync(context, doc, file, false);
        }

        //if (doc.DocumentType == Database.DocumentType.DecreeProject ||
        //    doc.DocumentType == Database.DocumentType.LawProject)
        //{

        //  foreach (var chDoc in doc.ChildeDocuments ?? await GetChildeDocumentsAsync(context, doc.Uuid))
        //  {
        //    foreach (var file in chDoc.Files ?? await GetFilesAsync(context, chDoc.Uuid))
        //    {
        //      result &= await AddAsync(context, chDoc, file, false);
        //    }
        //  }
        //}
      }

      if (saveChanges)
      {
        await context.SaveChangesAsync();
      }

      return result;
    }

    public async Task<bool> DeleteAsync(Document doc)
    {
      var context = new DocumentsContext();

      if (doc == null)
      {
        return false;
      }

      context.Documents.Attach(doc);

      context.Entry(doc).State = EntityState.Modified;

      return await DeleteAsync(context, doc, true);
    }

    private async Task<bool> DeleteAsync(DocumentsContext context, Document doc, bool saveResult)
    {
      var result = true;

      try
      {
        var delResult = await _manager.DeleteTypeAsync(_indexForDoc, DocumentType, doc.Uuid.ToString());

        if (delResult)
        {
          doc.Indexing = false;
        }

        result &= delResult;

        foreach (var file in doc.Files ?? await GetFilesAsync(context, doc.Uuid))
        {
          delResult = await DeleteAsync(context, file, false);

          result &= delResult;
        }

        foreach (var childrenDoc in doc.ChildeDocuments ?? await GetChildeDocumentsAsync(context, doc.Uuid))
        {
          foreach (var file in doc.Files ?? await GetFilesAsync(context, childrenDoc.Uuid))
          {
            delResult = await DeleteAsync(context, file, false);

            result &= delResult;
          }
        }
      }
      catch (Exception ex)
      {
        Logger.Error($"Ошибка при удалении документа из индекса (Uuid : {doc.Uuid.ToString()})", ex);

        result = false;
      }

      if (saveResult)
      {
        await context.SaveChangesAsync();
      }

      return result;
    }

    public async Task<bool> AddAsync(DocumentFile file)
    {
      var context = new DocumentsContext();

      context.DocumentFiles.Attach(file);

      context.Entry(file).State = EntityState.Modified;

      var doc = await GetDocumentAsync(context, file.DocumentUuid);

      return await AddAsync(context, doc, file, true);
    }

    private async Task<bool> AddAsync(DocumentsContext context, Document doc, DocumentFile file, bool saveChanges)
    {
      Document parentDoc;

      if (doc.Uuid != file.DocumentUuid)
      {
        Logger.Error($"Передан документ не соответсвующий файлу");

        return false;
      }

      if (doc.ParentUuid.HasValue)
      {
        parentDoc = doc.ParentDocument ?? await GetDocumentAsync(context, doc.ParentUuid.Value);
      }
      else
      {
        parentDoc = doc;
      }

      var fileInfo = FileManager.GetFilePathInfo(file);

      var id = file.Uuid.ToString();

      var type = new ElasticDocument
      {
        Type = ElasticType.File,
        DocType = parentDoc.FinalDocumentType,
        Date = parentDoc.FinalDate,
        Number = parentDoc.FinalDocumentNumber,
        Session = parentDoc.Session?.ComplexNumber(),
        SubjectOfLaw = parentDoc.FinalSubjectOfLaw,
        AnticorruptionExpertise = parentDoc.AnticorruptionExpertise,
        ExpertiseStart = parentDoc.ExpertiseStart,
        ExpertiseEnd = parentDoc.ExpertiseEnd,
        Title = Path.GetFileNameWithoutExtension(file.Description),
        Content = fileInfo.Extension.ToLower() == ".pdf"
          ? LoadDocumentContent(fileInfo)
          : string.Empty
      };

      var addResult = await _manager.AddTypeObjectAsync(_indexForFile, FileType, id, type);

      file.Indexing = addResult;

      if (saveChanges)
      {
        await context.SaveChangesAsync();
      }

      return addResult;
    }

    public async Task<bool> DeleteAsync(DocumentFile file)
    {
      var context = new DocumentsContext();

      return await DeleteAsync(context, file, true);
    }

    private async Task<bool> DeleteAsync(DocumentsContext context, DocumentFile file, bool saveResult)
    {
      try
      {
        var delResult = await _manager.DeleteTypeAsync(_indexForFile, FileType, file.Uuid.ToString());

        if (!delResult)
        {
          return false;
        }

        file.Indexing = false;

        context.DocumentFiles.Attach(file);
        context.Entry(file).State = EntityState.Modified;

        if (saveResult)
        {
          await context.SaveChangesAsync();
        }

        return true;
      }
      catch (Exception ex)
      {
        Logger.Error($"Ошибка при удалении файла из индекса (Uuid : {file.Uuid.ToString()})", ex);

        return false;
      }
    }

    public async Task<bool> ReindexingAll(bool onlyNotIndexing = false)
    {
      var context = new DocumentsContext();

      var result = true;

      try
      {
        ReindexPercent = 0;

        var pageNumber = 0;

        Document[] documents;

        var searchDocuments = context.Documents
          .Independent()
          .WithAllFields();

        if (onlyNotIndexing)
        {
          searchDocuments = searchDocuments
            .Where(el => el.Indexing != el.DisplayOnTheSite);
        }

        searchDocuments = searchDocuments
          .OrderBy(el => el.Uuid);

        var countDocs = await searchDocuments
          .CountAsync();

        var countDocsIndexing = 0;

        do
        {
          documents = await searchDocuments
            .Skip(PageSize * pageNumber)
            .Take(PageSize)
            .ToArrayAsync();

          foreach (var doc in documents)
          {
            if (doc.DisplayOnTheSite)
            {

              if (doc.ParentDocument != null
                  && (doc.DocumentType == Database.DocumentType.Law
                      || (doc.DocumentType == Database.DocumentType.Decree &&
                          doc.ParentDocument.DocumentType == Database.DocumentType.DecreeProject)))
              {
                var _ = await DeleteAsync(context, doc.ParentDocument, false);
              }

              result &= await AddAsync(context, doc, true, false);
            }
            else
            {
              result &= await DeleteAsync(context, doc, false);
            }

            countDocsIndexing++;

            ReindexPercent = (int)Math.Round(countDocsIndexing / (double)countDocs * 100, 0);
          }

          pageNumber++;

          await context.SaveChangesAsync();

        } while (documents.Length == PageSize);

        ReindexPercent = null;
      }
      catch (Exception ex)
      {
        Logger.Error("Ошибка процесса переиндексирования", ex);

        result = false;
      }
      finally
      {
        ReindexPercent = null;
      }

      return result;
    }

    private static string LoadDocumentContent(FileInfo file)
    {
      if (file == null || !file.Exists)
      {
        return null;
      }

      try
      {
        var sb = new StringBuilder();

        PdfLoadedDocument loadedDocument = null;

        using (Stream stream = file.OpenRead())
        {
          try
          {
            loadedDocument = new PdfLoadedDocument(stream);

            foreach (PdfPageBase page in loadedDocument.Pages)
            {
              var text = page.ExtractText();

              if (string.IsNullOrWhiteSpace(text))
              {
                continue;
              }

              sb.Append(" ");
              sb.Append(text);
            }
          }
          finally
          {
            loadedDocument?.Close(true);
          }  
        }

        return sb.ToString();
      }
      catch (Exception ex)
      {
        Logger.Error($"Ошибка при получении текста файла : {file.FullName}", ex);

        return string.Empty;
      }
    }

    public async Task<IDictionary<Guid, double>> SearchAsync(string request, ElasticDocument filter = null)
    {

      //var hits = await _manager.SearchAsync(
      //  new[] { _indexForDoc, _indexForFile },
      //  request,
      //  new[] { "Title", "Content" });

      var hits = await _manager.SearchAsync(
        new[] { _indexForDoc },
        request,
        new[] {"Content" });

      var docIds = hits
        .Where(el => el.Type == DocumentType)
        .Select(el => new UuidScore
        {
          Uuid = Guid.Parse(el.Id),
          Score = double.Parse(el.Score.Replace(',', '.'), CultureInfo.InvariantCulture)
        });

      //var result = docIds
      //  .Union(await ByFile(hits))
      //  .GroupBy(el => el.Uuid)
      //  .ToDictionary(el => el.Key, el => el.Sum(x => x.Score));

      var result = docIds
        .ToDictionary(el => el.Uuid, el => el.Score);

      return result;
    }

    private async Task<IEnumerable<UuidScore>> ByFile(IEnumerable<ElasticHit> hits)
    {
      var fileIds = hits
        .Where(el => el.Type == FileType)
        .ToDictionary(el => Guid.Parse(el.Id),
          el => double.Parse(el.Score.Replace(',', '.'), CultureInfo.InvariantCulture));

      if (!fileIds.Any())
      {
        return new UuidScore[0];
      }

      var context = new DocumentsContext();

      var result = await (from d in context.Documents
                          join f in context.DocumentFiles on d.Uuid equals f.DocumentUuid
                          join u in fileIds on f.Uuid equals u.Key
                          select new UuidScore
                          {
                            Uuid = d.ParentUuid ?? d.Uuid,
                            Score = u.Value
                          })
        .ToArrayAsync();

      return result;
    }

    private class UuidScore
    {
      public Guid Uuid { get; set; }

      public double Score { get; set; }
    }

    private static async Task<Document> GetDocumentAsync(DocumentsContext context, Guid uuid)
    {
      return await context.Documents
        .OnlyForLanSite(true)
        .ByUuid(uuid)
        .WithAllFields()
        .FirstOrDefaultAsync();
    }

    private async Task<IEnumerable<Document>> GetChildeDocumentsAsync(DocumentsContext context, Guid baseDocUuid)
    {
      return await context.Documents
        .Where(el => el.ParentUuid == baseDocUuid)
        .ToArrayAsync();
    }

    private async Task<IEnumerable<DocumentFile>> GetFilesAsync(DocumentsContext context, Guid docUuid)
    {
      return await context.DocumentFiles
        .Where(el => el.DocumentUuid == docUuid)
        .ToArrayAsync();
    }
  }
}
