using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using docs.Database;

namespace docs.Managers
{
  public class FileManager
  {
    public static string GetDocumentDirectory(Guid docUuid)
    {
      return Path.Combine(GlobalProperties.StoragePath, docUuid.ToString());
    }

    public static DirectoryInfo GetDocumentDirectoryInfo(Guid docUuid)
    {
      return new DirectoryInfo(GetDocumentDirectory(docUuid));
    }

    public static string GetFilePath(DocumentFile file)
    {
      return Path.Combine(GetDocumentDirectory(file.DocumentUuid), file.FileName);
    }

    public static FileInfo GetFilePathInfo(DocumentFile file)
    {
      return new FileInfo(GetFilePath(file));
    }

    public static void DeleteFile(DocumentFile dFile)
    {
      var fInfo = GetFilePathInfo(dFile);

      if (!fInfo.Exists)
      {
        return;
      }

      try
      {
        fInfo.Delete();
      }
      catch
      {
        //ignore
      }
    }

    public static void DeleteFileDirectory(Guid docUuid)
    {
      var dInfo = GetDocumentDirectoryInfo(docUuid);

      if (!dInfo.Exists)
      {
        return;
      }

      try
      {
        dInfo.Delete(true);
      }
      catch
      {
        //ignore
      }
    }
  }
}
