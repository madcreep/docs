using docs.Database;
using docs.Extensions;
using docs.Models;
using log4net;
using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace docs.Managers
{
  public interface IMailManager
  {
    string MailStoragePath { get; }
    //bool Send(IMailMessage message, string toSectionName, string fromSectionName = null);
    Task<bool> SendAsync(IEmailMessage message, string toSectionName, string fromSectionName = null);
  }

  public class MailManager : IMailManager, IDisposable
  {
    private const int NotSentMessageBeforeInterval = -5;

    private readonly ILog _logger = LogManager.GetLogger(typeof(MailManager));

    private readonly IMailSettings _mailSettings;

    private Thread _sendThread;

    private Timer _sendTimer;

    private readonly object _syncRoot = new object();

    private bool _isDisposed;

    public MailManager(IMailSettings mailSettings)
    {
      _mailSettings = mailSettings;

      SetSendTimer();
    }

    //public bool Send(IMailMessage message, string toSectionName, string fromSectionName = null)
    //{
    //  SmtpClient smtp = null;

    //  try
    //  {
    //    _mailSettings.Init(toSectionName, fromSectionName);

    //    message.Message.From = _mailSettings.From;

    //    foreach (var mailAddress in _mailSettings.To)
    //    {
    //      message.Message.To.Add(mailAddress);
    //    }

    //    smtp = _mailSettings.SmtpPort.HasValue
    //      ? new SmtpClient(_mailSettings.SmtpHost, _mailSettings.SmtpPort.Value)
    //      : new SmtpClient(_mailSettings.SmtpHost);

    //    smtp.Credentials = _mailSettings.NetworkCredential;

    //    smtp.EnableSsl = _mailSettings.EnableSsl;

    //    smtp.Send(message.Message);

    //    return true;
    //  }
    //  catch (Exception ex)
    //  {
    //    _logger.Error("Ошибка при отправке E_mail", ex);

    //    return false;
    //  }
    //  finally
    //  {
    //    smtp?.Dispose();

    //    message?.Message.Dispose();
    //  }
    //}

    public string MailStoragePath => _mailSettings.MailStoragePath;

    public async Task<bool> SendAsync(IEmailMessage message, string toSectionName, string fromSectionName = null)
    {
      DocumentsContext context = null;

      bool result;

      try
      {
        context = new DocumentsContext();

        var sendMessage = CreateNewEmailMessage(message);

        context.EmailMessages.Add(sendMessage);

        await context.SaveChangesAsync();

        ThreadPool.QueueUserWorkItem(SendSingleMessage, sendMessage.Id);

        result = true;
      }
      catch (Exception ex)
      {
        _logger.Error($"Ошибка отправки сообщения электронной почты{Environment.NewLine}{ex}");

        result = false;
      }
      finally
      {
        context?.Dispose();
      }

      return result;
    }

    private EmailMessage CreateNewEmailMessage(IEmailMessage inMsg)
    {
      return new EmailMessage
      {
        ToSectionName = inMsg.ToSectionName,
        FromSectionName = inMsg.FromSectionName,
        MessageSubject = inMsg.MessageSubject,
        MessageBody = inMsg.MessageBody,
        IsBodyHtml = inMsg.IsBodyHtml,
        FileDirectory = inMsg.FileDirectory,

        CreatedDate = DateTime.Now,
        IsSent = false,
        NeedSend = true
      };
    }

    private void SendSingleMessage(object obj)
    {
      DocumentsContext context = null;
      try
      {
        context = new DocumentsContext();

        var msgId = (long)obj;

        var message = context.EmailMessages
          .ById(msgId)
          .Single();

        SendSingleMessage(context, message);
      }
      catch (Exception ex)
      {
        _logger.Error($"Ошибка отправки сообщения электронной почты{Environment.NewLine}{ex}");
      }
      finally
      {
        context?.Dispose();
      }
    }

    private EmailMessage[] GetNotSentMessagesAsync(DocumentsContext context)
    {
      var toDate = DateTime.Now.AddMinutes(NotSentMessageBeforeInterval);

      return context.EmailMessages
        .NeedSend()
        .NotSent()
        .Where(el => el.CreatedDate < toDate)
        .OrderByDescending(el => el.CreatedDate)
        .ToArray();
    }

    private void SendAllNotSent()
    {
      lock (_syncRoot)
      {
        if (_isDisposed)
        {
          return;
        }
      }

      DocumentsContext context = null;

      try
      {
        _logger.Info("Поток отправки сообщений запущен");

        context = new DocumentsContext();

        var needSend = GetNotSentMessagesAsync(context);

        _logger.Debug($"Сообщений электронной почты для отправки {needSend.Length}");

        foreach (var message in needSend)
        {
          SendSingleMessage(context, message);
        }
      }
      catch (ThreadAbortException)
      {
        _logger.Info("Поток отправки сообщений остановлен принудительно");
      }
      catch (Exception ex)
      {
        _logger.Error($"Ошибка при отправке сообщений электронной почты{Environment.NewLine}{ex}");
      }
      finally
      {
        context?.Dispose();
      }

      _sendThread = null;
    }

    private void SendSingleMessage(DocumentsContext context, EmailMessage message)
    {
      SmtpClient smtp = null;

      MailMessage mailMessage = null;

      try
      {
        var mailSettings = _mailSettings[message.FromSectionName, message.ToSectionName];

        mailMessage = new MailMessage()
        {
          Subject = message.MessageSubject,
          From = mailSettings.From.FromAddress,
          Body = message.MessageBody,
          IsBodyHtml = message.IsBodyHtml
        };

        foreach (var mailAddress in mailSettings.To)
        {
          mailMessage.To.Add(mailAddress);
        }

        if (!string.IsNullOrWhiteSpace(message.FileDirectory))
        {
          var directoryPath = Path.Combine(_mailSettings.MailStoragePath, message.FileDirectory);

          AttachFiles(mailMessage, directoryPath);
        }

        smtp = mailSettings.From.SmtpPort.HasValue
          ? new SmtpClient(mailSettings.From.SmtpHost, mailSettings.From.SmtpPort.Value)
          : new SmtpClient(mailSettings.From.SmtpHost);

        smtp.Credentials = mailSettings.From.NetworkCredential;

        smtp.EnableSsl = mailSettings.From.EnableSsl;

        smtp.Send(mailMessage);

        message.IsSent = true;

        message.SentError = null;
      }
      catch (ThreadAbortException ex)
      {
        message.IsSent = false;

        message.SentError = ex.Message;

        throw;

      }
      catch (Exception ex)
      {
        message.IsSent = false;

        message.SentError = ex.Message;
      }
      finally
      {
        smtp?.Dispose();

        mailMessage?.Dispose();

        message.SendDate = DateTime.Now;

        context.SaveChanges();
      }
    }

    private static void AttachFiles(MailMessage message, string directoryPath)
    {
      if (!Directory.Exists(directoryPath))
      {
        throw new DirectoryNotFoundException($"Отсутствует директория {directoryPath}");
      }

      var files = Directory.GetFiles(directoryPath);

      foreach (var file in files)
      {
        var attachment = new Attachment(file);

        message.Attachments.Add(attachment);
      }
    }

    private void SetSendTimer()
    {
      lock (_syncRoot)
      {
        if (_sendTimer != null)
        {
          return;
        }

        var delayTime = new TimeSpan(0, 0, 30);

        var intervalTime = new TimeSpan(0, 0, 15, 0);

        _sendTimer = new Timer(StartSendThread, null, delayTime, intervalTime);
      }
    }

    private void StartSendThread(object obj)
    {
      lock (_syncRoot)
      {
        if (_isDisposed || _sendThread != null)
        {
          return;
        }

        _sendThread = new Thread(SendAllNotSent);

        _sendThread.Start();
      }
    }

    public void Dispose()
    {
      lock (_syncRoot)
      {
        _isDisposed = true;

        if (_isDisposed)
        {
          return;
        }

        _sendTimer?.Dispose();

        if (_sendThread != null)
        {
          _sendThread.Abort();

          _sendThread.Join();
        }
      }
    }
  }
}
