using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace docs.Managers
{
  public interface IMailSettings
  {
    string MailStoragePath { get; }
    ISmtpParam this[string fromSectionName, string toSectionName] { get; }
  }

  public interface IFromEmailParam
  {
    MailAddress FromAddress { get; }
    string SmtpHost { get; }
    int? SmtpPort { get; }
    NetworkCredential NetworkCredential { get; }
    bool EnableSsl { get; }
  }

  public interface ISmtpParam
  {
    IFromEmailParam From { get; }
    MailAddress[] To { get; }
  }

  public class MailSettings : IMailSettings
  {
    private const string SmtpSectionName = "Smtp";

    private const string FromSectionName = "From";

    private const string DefaultSectionName = "Default";

    private const string ToSectionName = "To";

    private IConfiguration _config;

    private IConfigurationSection _smtpSection;

    //public MailAddress From { get; private set; }

    //public string SmtpHost { get; private set; }

    //public int? SmtpPort { get; private set; }

    //public NetworkCredential NetworkCredential { get; private set; }

    //public bool EnableSsl { get; private set; }

    //public MailAddress[] To { get; private set; }


    public MailSettings(IConfiguration config)
    {
      _config = config;

      _smtpSection = GetSmtpSection();
    }

    //public void Init(string toSectionName, string fromSectionName)
    //{
    //  fromSectionName = string.IsNullOrWhiteSpace(fromSectionName)
    //    ? DefaultSectionName
    //    : fromSectionName;

    //  toSectionName = string.IsNullOrWhiteSpace(toSectionName)
    //    ? DefaultSectionName
    //    : toSectionName;

    //  var smtpSection = GetSmtpSection();

    //  AddFromSection(smtpSection, fromSectionName);

    //  To = GetToAddresses(smtpSection, toSectionName);
    //}

    private IConfigurationSection GetSmtpSection()
    {
      var smtpSection = _config.GetSection(SmtpSectionName);

      if (smtpSection == null)
      {
        throw new Exception($"В конфигурации отсутствует секция {SmtpSectionName}");
      }

      return smtpSection;
    }

    //private void AddFromSection(IConfigurationSection smtpSection, string fromSectionName)
    //{
    //  var from = smtpSection.GetSection(FromSectionName);

    //  if (!from.Exists())
    //  {
    //    throw new Exception($"В секции {smtpSection.Path} конфигурации отсутствует секция {FromSectionName}");
    //  }

    //  var fromSection = from.GetSection(fromSectionName);

    //  if (!fromSection.Exists())
    //  {
    //    throw
    //      new Exception($"В секции {from.Path} конфигурации отсутствует секция {fromSectionName}");
    //  }

    //  From = new MailAddress(
    //    fromSection.GetValue<string>("Address"),
    //    fromSection.GetValue<string>("Name"));

    //  SmtpHost = fromSection.GetValue<string>("SmtpHost");

    //  SmtpPort = fromSection.GetValue<int?>("SmtpPort", null);

    //  NetworkCredential = new NetworkCredential(
    //    fromSection.GetValue<string>("Login"),
    //    fromSection.GetValue<string>("Password"));

    //  EnableSsl = fromSection.GetValue<bool>("EnableSsl");
    //}

    private FromParam GetFrom(string fromSectionName)
    {
      var from = _smtpSection.GetSection(FromSectionName);

      if (!from.Exists())
      {
        throw new Exception($"В секции {_smtpSection.Path} конфигурации отсутствует секция {FromSectionName}");
      }

      var fromSection = from.GetSection(fromSectionName);

      if (!fromSection.Exists())
      {
        fromSection = from.GetSection(DefaultSectionName);

        if (!fromSection.Exists())
        {
          throw
            new Exception($"В секции {from.Path} конфигурации отсутствует секция {fromSectionName}");
        }
      }

      return new FromParam
      {

        FromAddress = new MailAddress(
          fromSection.GetValue<string>("Address"),
          fromSection.GetValue<string>("Name")),

        SmtpHost = fromSection.GetValue<string>("SmtpHost"),

        SmtpPort = fromSection.GetValue<int?>("SmtpPort", null),

        NetworkCredential = new NetworkCredential(
        fromSection.GetValue<string>("Login"),
        fromSection.GetValue<string>("Password")),

        EnableSsl = fromSection.GetValue<bool>("EnableSsl")
      };
    }

    private MailAddress[] GetToAddresses(string toSectionName)
    {
      var toSection = string.IsNullOrWhiteSpace(toSectionName)
        ? DefaultSectionName
        : toSectionName;

      var to = _smtpSection.GetSection(ToSectionName);

      if (!to.Exists())
      {
        throw new Exception($"В секции {_smtpSection.Path} конфигурации отсутствует секция {toSection}");
      }

      var addressesStrings = to.GetSection(toSection).Get<string[]>();

      if (addressesStrings == null || !addressesStrings.Any())
      {
        addressesStrings = to.GetSection(DefaultSectionName).Get<string[]>();

        if (addressesStrings == null || !addressesStrings.Any())
        {
          throw new Exception($"В секции {SmtpSectionName}/{toSection} конфигурации отсутствуют адреса для отправки почты");
        }
      }

      var result = addressesStrings
        .Select(el => new MailAddress(el))
        .ToArray();

      return result;
    }


    public string MailStoragePath => _config.GetSection("MailStoragePath").Get<string>();

    public ISmtpParam this[string fromSectionName, string toSectionName] =>
      new SmtpParam
      {
        From = GetFrom(fromSectionName),
        To = GetToAddresses(toSectionName)
      };

    public class SmtpParam : ISmtpParam
    {
      public IFromEmailParam From { get; set; }

      public MailAddress[] To { get; set; }
    }



    public class FromParam : IFromEmailParam
    {
      public MailAddress FromAddress { get; set; }

      public string SmtpHost { get; set; }

      public int? SmtpPort { get; set; }

      public NetworkCredential NetworkCredential { get; set; }

      public bool EnableSsl { get; set; }
    }
  }
}
