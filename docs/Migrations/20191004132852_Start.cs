﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace docs.Migrations
{
    public partial class Start : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    description = table.Column<string>(maxLength: 255, nullable: true),
                    specific_right = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    fullname = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Convocations",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    number = table.Column<int>(nullable: false),
                    name = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Convocations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Readings",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    name = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Readings", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Results",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    name = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Results", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Rubrics",
                schema: "public",
                columns: table => new
                {
                    Uuid = table.Column<Guid>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    due = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rubrics", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Stages",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    name = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stages", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "UserModel",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    ConfirmPassword = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserModel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "public",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                schema: "public",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "public",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sessions",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    number_int = table.Column<int>(nullable: false),
                    number = table.Column<string>(maxLength: 128, nullable: true),
                    date = table.Column<DateTime>(nullable: true),
                    session_type = table.Column<string>(nullable: true),
                    convocation = table.Column<string>(maxLength: 128, nullable: true),
                    convocation_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessions", x => x.id);
                    table.ForeignKey(
                        name: "FK_Sessions_Convocations_convocation_id",
                        column: x => x.convocation_id,
                        principalSchema: "public",
                        principalTable: "Convocations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                schema: "public",
                columns: table => new
                {
                    uuid = table.Column<Guid>(nullable: false),
                    parent_uuid = table.Column<Guid>(nullable: true),
                    display = table.Column<bool>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    parent_id = table.Column<int>(nullable: true),
                    order = table.Column<int>(nullable: true),
                    document_type = table.Column<int>(nullable: false),
                    is_project = table.Column<bool>(nullable: false),
                    upload_date = table.Column<DateTime>(nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    number = table.Column<string>(maxLength: 64, nullable: true),
                    tech_number = table.Column<string>(maxLength: 255, nullable: true),
                    publish_date = table.Column<DateTime>(nullable: true),
                    anticorruption_expertise = table.Column<bool>(nullable: false),
                    expertise_start = table.Column<DateTime>(nullable: true),
                    expertise_end = table.Column<DateTime>(nullable: true),
                    correspondent = table.Column<string>(maxLength: 1024, nullable: true),
                    correspondent_is_zsk = table.Column<bool>(nullable: false),
                    profile_committee = table.Column<string>(maxLength: 1024, nullable: true),
                    convening = table.Column<int>(nullable: true),
                    session_number = table.Column<int>(nullable: true),
                    subject_of_law = table.Column<string>(maxLength: 1024, nullable: true),
                    content = table.Column<string>(nullable: true),
                    group = table.Column<string>(maxLength: 1024, nullable: true),
                    has_approved_act = table.Column<bool>(nullable: true),
                    rubric_uuid = table.Column<Guid>(nullable: true),
                    session_id = table.Column<long>(nullable: true),
                    agenda_number = table.Column<string>(maxLength: 64, nullable: true),
                    reading_id = table.Column<long>(nullable: true),
                    stage_id = table.Column<long>(nullable: true),
                    result_id = table.Column<long>(nullable: true),
                    indexing = table.Column<bool>(nullable: false),
                    ins_date = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.uuid);
                    table.ForeignKey(
                        name: "FK_Documents_Documents_parent_uuid",
                        column: x => x.parent_uuid,
                        principalSchema: "public",
                        principalTable: "Documents",
                        principalColumn: "uuid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_Readings_reading_id",
                        column: x => x.reading_id,
                        principalSchema: "public",
                        principalTable: "Readings",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_Results_result_id",
                        column: x => x.result_id,
                        principalSchema: "public",
                        principalTable: "Results",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_Rubrics_rubric_uuid",
                        column: x => x.rubric_uuid,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Uuid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_Sessions_session_id",
                        column: x => x.session_id,
                        principalSchema: "public",
                        principalTable: "Sessions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_Stages_stage_id",
                        column: x => x.stage_id,
                        principalSchema: "public",
                        principalTable: "Stages",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentFiles",
                schema: "public",
                columns: table => new
                {
                    uuid = table.Column<Guid>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    filename = table.Column<string>(maxLength: 512, nullable: true),
                    extension = table.Column<string>(maxLength: 12, nullable: true),
                    size = table.Column<long>(nullable: true),
                    description = table.Column<string>(maxLength: 512, nullable: true),
                    order_num = table.Column<int>(nullable: true),
                    send = table.Column<bool>(nullable: false),
                    document_uuid = table.Column<Guid>(nullable: false),
                    indexing = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentFiles", x => x.uuid);
                    table.ForeignKey(
                        name: "FK_DocumentFiles_Documents_document_uuid",
                        column: x => x.document_uuid,
                        principalSchema: "public",
                        principalTable: "Documents",
                        principalColumn: "uuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReviewStages",
                schema: "public",
                columns: table => new
                {
                    uuid = table.Column<Guid>(nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    number = table.Column<string>(maxLength: 64, nullable: true),
                    stage_type = table.Column<int>(nullable: false),
                    stage_name = table.Column<string>(maxLength: 1024, nullable: true),
                    profile_committee = table.Column<string>(maxLength: 1024, nullable: true),
                    send_date = table.Column<DateTime>(nullable: true),
                    DocumentUuid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewStages", x => x.uuid);
                    table.ForeignKey(
                        name: "FK_ReviewStages_Documents_DocumentUuid",
                        column: x => x.DocumentUuid,
                        principalSchema: "public",
                        principalTable: "Documents",
                        principalColumn: "uuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "description", "Name", "NormalizedName", "specific_right" },
                values: new object[,]
                {
                    { new Guid("773464a8-fc23-4115-80ed-07911edf3af1"), "2a73fc30-8bb2-4010-9c22-16e395212e2e", null, "admin", "ADMIN", false },
                    { new Guid("fedb050f-1efb-445d-8187-7308afda8726"), "5396fe71-7ed1-477d-a12b-efd0fb3d6aff", null, "user", "USER", false },
                    { new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"), "8499f30b-dfde-4118-a637-8861c0d33e00", null, "service", "SERVICE", false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                schema: "public",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "public",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                schema: "public",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                schema: "public",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                schema: "public",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "public",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "public",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_UserName",
                schema: "public",
                table: "AspNetUsers",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentFiles_document_uuid",
                schema: "public",
                table: "DocumentFiles",
                column: "document_uuid");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentFiles_filename",
                schema: "public",
                table: "DocumentFiles",
                column: "filename");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentFiles_id",
                schema: "public",
                table: "DocumentFiles",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_date",
                schema: "public",
                table: "Documents",
                column: "date");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_id",
                schema: "public",
                table: "Documents",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_number",
                schema: "public",
                table: "Documents",
                column: "number");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_parent_uuid",
                schema: "public",
                table: "Documents",
                column: "parent_uuid");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_reading_id",
                schema: "public",
                table: "Documents",
                column: "reading_id");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_result_id",
                schema: "public",
                table: "Documents",
                column: "result_id");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_rubric_uuid",
                schema: "public",
                table: "Documents",
                column: "rubric_uuid");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_session_id",
                schema: "public",
                table: "Documents",
                column: "session_id");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_stage_id",
                schema: "public",
                table: "Documents",
                column: "stage_id");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewStages_DocumentUuid",
                schema: "public",
                table: "ReviewStages",
                column: "DocumentUuid");

            migrationBuilder.CreateIndex(
                name: "IX_Rubrics_due",
                schema: "public",
                table: "Rubrics",
                column: "due");

            migrationBuilder.CreateIndex(
                name: "IX_Rubrics_id",
                schema: "public",
                table: "Rubrics",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_convocation_id",
                schema: "public",
                table: "Sessions",
                column: "convocation_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens",
                schema: "public");

            migrationBuilder.DropTable(
                name: "DocumentFiles",
                schema: "public");

            migrationBuilder.DropTable(
                name: "ReviewStages",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserModel",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetRoles",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUsers",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Documents",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Readings",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Results",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Rubrics",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Sessions",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Stages",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Convocations",
                schema: "public");
        }
    }
}
