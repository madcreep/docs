﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace docs.Migrations
{
    public partial class DeleteConvocztionSrtFromSessions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "convocation",
                schema: "public",
                table: "Sessions");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "3e485859-97a9-4089-90c4-a1a76afef071");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "5ec8af54-9d8f-41b8-bec5-14958324282e");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "2cbc07b9-4e26-4f77-8a5d-78c593a3ff29");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "convocation",
                schema: "public",
                table: "Sessions",
                maxLength: 128,
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "8499f30b-dfde-4118-a637-8861c0d33e00");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "2a73fc30-8bb2-4010-9c22-16e395212e2e");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "5396fe71-7ed1-477d-a12b-efd0fb3d6aff");
        }
    }
}
