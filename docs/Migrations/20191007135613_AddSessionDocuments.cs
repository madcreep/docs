﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace docs.Migrations
{
    public partial class AddSessionDocuments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SessionDocuments",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    session_id = table.Column<long>(nullable: false),
                    agenda = table.Column<string>(nullable: true),
                    annotation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionDocuments", x => x.id);
                    table.ForeignKey(
                        name: "FK_SessionDocuments_Sessions_session_id",
                        column: x => x.session_id,
                        principalSchema: "public",
                        principalTable: "Sessions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "97e51fc6-205a-4478-91c5-57786c582cbb");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "056efe49-f174-4710-aea4-24a8df86c8e1");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "b0bf0c72-1b7e-4fba-8d7a-323242134807");

            migrationBuilder.CreateIndex(
                name: "IX_SessionDocuments_session_id",
                schema: "public",
                table: "SessionDocuments",
                column: "session_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SessionDocuments",
                schema: "public");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "3e485859-97a9-4089-90c4-a1a76afef071");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "5ec8af54-9d8f-41b8-bec5-14958324282e");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "2cbc07b9-4e26-4f77-8a5d-78c593a3ff29");
        }
    }
}
