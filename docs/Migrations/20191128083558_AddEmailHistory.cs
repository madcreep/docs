using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace docs.Migrations
{
    public partial class AddEmailHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmailMessages",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    from_section = table.Column<string>(maxLength: 1024, nullable: true),
                    to_section = table.Column<string>(maxLength: 1024, nullable: true),
                    message_subject = table.Column<string>(maxLength: 512, nullable: true),
                    file_directory = table.Column<string>(maxLength: 128, nullable: true),
                    message_body = table.Column<string>(nullable: true),
                    is_body_html = table.Column<bool>(nullable: false),
                    created_date = table.Column<DateTime>(nullable: false),
                    send_date = table.Column<DateTime>(nullable: true),
                    is_sent = table.Column<bool>(nullable: false),
                    sent_error = table.Column<string>(nullable: true),
                    need_send = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailMessages", x => x.id);
                });

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "22c7296a-6121-48a3-b6b1-d704f207fdfc");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "d81cadb7-4568-4e4c-aacf-2ddc6daebc7c");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "2b373942-db93-4de1-a7e8-9e0e3fcf16b0");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailMessages",
                schema: "public");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "329d98e5-80c1-42ea-9fde-f6247ae4c928");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "b88919bc-8339-44c3-ae26-4b89ebf02f38");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "514f0936-52f7-4955-9d18-eebd1c18d77b");
        }
    }
}
