﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace docs.Migrations
{
    public partial class AddPlaceOfPublication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "message_body",
                schema: "public",
                table: "EmailMessages",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 2048,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "place_of_publication",
                schema: "public",
                table: "Documents",
                maxLength: 256,
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "975a87f8-234b-4c5e-b790-dd8204ae7351");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "a11bc56e-329b-41bd-9b21-9ad6074bf30a");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "30b4f3c6-9d85-46ee-ba72-659a58eeef23");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "place_of_publication",
                schema: "public",
                table: "Documents");

            migrationBuilder.AlterColumn<string>(
                name: "message_body",
                schema: "public",
                table: "EmailMessages",
                maxLength: 2048,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "22c7296a-6121-48a3-b6b1-d704f207fdfc");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "d81cadb7-4568-4e4c-aacf-2ddc6daebc7c");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "2b373942-db93-4de1-a7e8-9e0e3fcf16b0");
        }
    }
}
