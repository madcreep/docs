﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace docs.Migrations
{
    public partial class AddUnloadingTimestamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "updating_timestamp",
                schema: "public",
                table: "Documents",
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "80360e48-05ed-4816-a5a0-c1d68c02c8ea");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "0dfae72f-60f3-4f00-9e43-582ca62fe660");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "096c12ed-baab-4f71-8f8d-0a3e9308bc8a");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "updating_timestamp",
                schema: "public",
                table: "Documents");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "d8398850-6efc-4c42-ab52-bd969225e3fe");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "75fabe52-23a6-4f55-b878-2beea9cd5d99");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "6b9f7d91-1207-4d71-8bbb-071b0a5659a8");
        }
    }
}
