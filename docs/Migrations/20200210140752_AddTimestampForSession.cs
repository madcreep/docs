using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace docs.Migrations
{
    public partial class AddTimestampForSession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"));

            migrationBuilder.DeleteData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"));

            migrationBuilder.DeleteData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"));

            migrationBuilder.AddColumn<bool>(
                name: "is_deleted",
                schema: "public",
                table: "Sessions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "updating_timestamp",
                schema: "public",
                table: "Sessions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_number_int",
                schema: "public",
                table: "Sessions",
                column: "number_int");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_tech_number",
                schema: "public",
                table: "Documents",
                column: "tech_number");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Sessions_number_int",
                schema: "public",
                table: "Sessions");

            migrationBuilder.DropIndex(
                name: "IX_Documents_tech_number",
                schema: "public",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "is_deleted",
                schema: "public",
                table: "Sessions");

            migrationBuilder.DropColumn(
                name: "updating_timestamp",
                schema: "public",
                table: "Sessions");

            migrationBuilder.InsertData(
                schema: "public",
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "description", "Name", "NormalizedName", "specific_right" },
                values: new object[,]
                {
                    { new Guid("773464a8-fc23-4115-80ed-07911edf3af1"), "587d8fc1-2098-473f-ae23-79bc65a7719f", null, "admin", "ADMIN", false },
                    { new Guid("fedb050f-1efb-445d-8187-7308afda8726"), "9ac9ebf4-4cf9-42be-9f13-ff2cf7815390", null, "user", "USER", false },
                    { new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"), "d27ff28d-ea0f-4619-91e3-529c1e10b241", null, "service", "SERVICE", false }
                });
        }
    }
}
