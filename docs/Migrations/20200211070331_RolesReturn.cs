﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace docs.Migrations
{
    public partial class RolesReturn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "public",
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "description", "Name", "NormalizedName", "specific_right" },
                values: new object[,]
                {
                    { new Guid("773464a8-fc23-4115-80ed-07911edf3af1"), "233cd737-9f72-4f09-b79c-f2e9b87c0789", null, "admin", "ADMIN", false },
                    { new Guid("fedb050f-1efb-445d-8187-7308afda8726"), "946c73b2-c770-4bf1-a833-6375375bd37c", null, "user", "USER", false },
                    { new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"), "5c5f4486-fb73-48b3-a5e2-a2e0c201d244", null, "service", "SERVICE", false }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"));

            migrationBuilder.DeleteData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"));

            migrationBuilder.DeleteData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"));
        }
    }
}
