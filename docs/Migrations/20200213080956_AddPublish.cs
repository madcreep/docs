using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace docs.Migrations
{
    public partial class AddPublish : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "publish",
                schema: "public",
                table: "Documents",
                nullable: false,
                defaultValue: true);

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "5dd72223-7ca1-4996-bee9-0703f4109e5a");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "bff19ca0-07b8-4121-acd5-ddb290c59835");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "7ff787dd-4440-4f3e-8626-4018d02f1496");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "publish",
                schema: "public",
                table: "Documents");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("354a823f-3b0a-47fa-9404-bf1a13b54322"),
                column: "ConcurrencyStamp",
                value: "5c5f4486-fb73-48b3-a5e2-a2e0c201d244");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("773464a8-fc23-4115-80ed-07911edf3af1"),
                column: "ConcurrencyStamp",
                value: "233cd737-9f72-4f09-b79c-f2e9b87c0789");

            migrationBuilder.UpdateData(
                schema: "public",
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("fedb050f-1efb-445d-8187-7308afda8726"),
                column: "ConcurrencyStamp",
                value: "946c73b2-c770-4bf1-a833-6375375bd37c");
        }
    }
}
