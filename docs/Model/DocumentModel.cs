using System;
using System.Collections.Generic;

namespace docs.Model
{
    public class DocumentModel
    {
        public Guid Uuid { get; set; }

        public int Id { get; set; }
        
        public int? ParentId { get; set; }

        public Guid? ParentUuid { get; set; }
        
        public bool IsProject { get; set; }

        public DateTime UploadDate { get; set; }

        public DateTime Date { get; set; }
        
        public string Number { get; set; }
        
        public DateTime? PublishDate { get; set; }

        public bool AnticorruptionExpertise { get; set; }
        public DateTime? ExpertiseStart { get; set; }
        public DateTime? ExpertiseEnd { get; set; }

        public string Сorrespondent { get; set; }
        
        public string ProfileCommittee { get; set; }
        
        public int? SessionNumber { get; set; }
        
        public string Content { get; set; }
        
        public RubricModel Rubric { get; set; }

        public ICollection<DocumentFileModel> Files { get; set; }
    }
}