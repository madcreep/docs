using System;

namespace docs.Model
{
    public class RoleForm
    {
        public Guid Id { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool SpecificRight { get; set; }
    }
}