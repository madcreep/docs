using System;

namespace docs.Model
{
    public class RubricModel
    {
        public Guid Uuid { get; set; }

        public int Id { get; set; }
        public string Due { get; set; }
        public string Name { get; set; }
    }
}