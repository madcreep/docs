using System;
using System.Collections.Generic;

namespace docs.Model
{
  public class UserForm : IUser
  {
    public Guid Id { get; set; }
    public string FullName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public bool RememberMe { get; set; }
    public bool IsNotDeleted { get; set; }
    public bool OnlyPasswordChange { get; set; }
    public List<RoleForm> AllRoles { get; set; }
    public IList<string> UserRoles { get; set; }

    public UserForm()
    {
      AllRoles = new List<RoleForm>();
      UserRoles = new List<string>();
    }
  }
}
