namespace docs.Models
{
  public class ApiSummary
  {
    public bool ElasticIsInit { get; set; }

    public long DocumentsCount { get; set; }

    public long NotSentEmailCount { get; set; }
    }
}
