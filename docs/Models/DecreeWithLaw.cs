using System;
using System.Collections.Generic;
using System.Linq;
using docs.Database;
using Microsoft.EntityFrameworkCore.Internal;

namespace docs.Models
{
  public class DecreeWithLaw
  {
    public Document Letter { get; }

    public Document Decree { get; }

    public DecreeWithLaw(Document letter, Document decree)
    {
      Letter = letter;
      Decree = decree;
    }

    public bool HasMainFiles
    {
      get { return Letter?.MainFile != null
        || Decree?.MainFile != null; }
    }
  }

  public class LettersWithDecree
  {
    private IEnumerable<Document> _conclusion;

    public IEnumerable<Document> Letters { get; }

    public IEnumerable<Document> Conclusion
    {
      get
      {
        if (_conclusion == null || Letters == null || !EnumerableExtensions.Any(Letters))
        {
          return null;
        }

        var letterType = Letters.First().DocumentType;

        DocumentType conclusionType;

        switch (letterType)
        {
          case DocumentType.Letter1:
            conclusionType = DocumentType.Conclusion1;
            break;
          case DocumentType.Letter2:
            conclusionType = DocumentType.Conclusion2;
            break;
          case DocumentType.Letter3:
            return null;
          case DocumentType.LawProject:
          case DocumentType.DecreeProject:
          case DocumentType.Conclusion1:
          case DocumentType.Conclusion2:
          case DocumentType.Law:
          case DocumentType.Decree:
          case DocumentType.Undefined:
          case DocumentType.PlenarySession:
          default:
            throw new ArgumentOutOfRangeException();
        }

        return _conclusion
          .Where(el => el.DocumentType == conclusionType)
          .OrderBy(el => el.Date);
      }
      set
      {
        if (Letters == null || !EnumerableExtensions.Any(Letters))
        {
          return;
        }

        _conclusion = value;
      }
    }

    public Document Decree { get; }

    public LettersWithDecree(IEnumerable<Document> letters, Document decree)
    {
      Letters = letters;
      Decree = decree;
    }
  }
}
