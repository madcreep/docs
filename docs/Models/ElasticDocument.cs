using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using docs.Database;

namespace docs.Models
{
  public class ElasticDocument
  {
    public ElasticType Type { get; set; }

    public DocumentType DocType { get; set; }

    public DateTime Date { get; set; }

    public string Number { get; set; }

    public string Session { get; set; }

    public string SubjectOfLaw { get; set; }

    public bool AnticorruptionExpertise { get; set; }

    public DateTime? ExpertiseStart { get; set; }

    public DateTime? ExpertiseEnd { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }
  }

  public enum ElasticType
  {
    [EnumMember(Value = "document")]
    Document,
    [EnumMember(Value = "file")]
    File
  }
}
