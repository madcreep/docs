using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using docs.Database;

namespace docs.Models
{
    public class FindDocumentsResult
    {
      public int Count { get; set; }

      public IEnumerable<Document> Documents { get; set; }

      public int? Page { get; set; }
    }
}
