using docs.Database;
using System;

namespace docs.Models
{
  public class FindFormModel
  {
    public string BaseSiteName { get; set; }

    public string ContextFind { get; set; }

    public DocumentType? NpaFind { get; set; }

    public int? DateFind { get; set; }

    public long? SessionFind { get; set; }

    public string NumberActFind { get; set; }

    public string SubjectFind { get; set; }

    public bool HasExpertizeFind { get; set; }

    public DateTime? DateExpertizeFromFind { get; set; }

    public DateTime? DateExpertizeToFind { get; set; }

    public int? Page { get; set; }

    public bool IsEmpty =>
      string.IsNullOrWhiteSpace(ContextFind)
      && !NpaFind.HasValue
      && !DateFind.HasValue
      && !SessionFind.HasValue
      && string.IsNullOrWhiteSpace(SubjectFind)
      && string.IsNullOrWhiteSpace(NumberActFind)
      && !HasExpertizeFind;
  }
}
