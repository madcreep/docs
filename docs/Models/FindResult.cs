using docs.Database;
using System.Collections.Generic;

namespace docs.Models
{
  public class FindResult
  {
    public bool ForSession { get; set; }

    public string Title { get; set; }

    public int Count { get; set; }

    public IEnumerable<Document> Documents { get; set; }
  }
}
