using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docs.Models
{
  public class GetSessionsByYearModel
  {
    public int? year { get; set; }
  }
}
