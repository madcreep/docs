using System.ComponentModel.DataAnnotations;

namespace docs.Models
{
  public class IndexingSummaryDataModel
  {
    [Display(Name = "Количество документов")]
    public int CountDocuments { get; set; }

    [Display(Name = "Количество неиндексированных документов")]
    public int NotIndexing { get; set; }

    [Display(Name = "Количество файлов")]
    public int CountFiles { get; set; }

    [Display(Name = "Количество неиндексированных файлов")]
    public int NotIndexingFiles { get; set; }

    public int? ProcessReindexingPercent { get; set; }

    public bool IndexingInited { get; set; }
  }
}
