namespace docs.Models
{
  public class PostDocumentAnswer
  {
    public bool Success
    {
      get { return ErrorMsg == null; }
    }

    public bool CanDelete { get; set; }

    public string ErrorMsg { get; set; }

    public PostDocumentAnswer()
    {
      CanDelete = true;
    }
  }
}
