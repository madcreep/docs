using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using docs.Database;

namespace docs.Models
{
  public class RcProject
  {
    #region Реализация IProcessResult
    /// <summary>
    /// Тип документа
    /// </summary>
    public DocumentType RcType { get; set; }

    /// <summary>
    /// ISN документа
    /// </summary>
    public int Isn { get; set; }

    /// <summary>
    /// Прикрепленнык файлы
    /// </summary>
    public List<RcFilesInfo> Files { get; set; }

    /// <summary>
    /// Признак необходимости сброса/установки Оубликовать/Опубликовано
    /// </summary>
    public bool NeedSetPublished { get; set; }
    #endregion

    /// <summary>
    /// Индекс сортировки
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// ISN родительского документа
    /// </summary>
    public int? ParentIsn { get; set; }

    /// <summary>
    /// Регистрационный номер
    /// </summary>
    public string RegistrationNumber { get; set; }

    /// <summary>
    /// Дата регистрации
    /// </summary>
    public DateTime RegistrationDate { get; set; }

    /// <summary>
    /// Дата публикации
    /// </summary>
    public DateTime? PublicationDate { get; set; }

    /// <summary>
    /// Дата подписания
    /// </summary>
    public DateTime? SigningDate { get; set; }

    /// <summary>
    /// Описание
    /// </summary>
    public string Content { get; set; }

    /// <summary>
    /// Корреспондент
    /// </summary>
    public string Correspondent { get; set; }

    /// <summary>
    /// Признак того, что корреспондент это подразделение ЗСК
    /// </summary>
    public bool CorrespondentIsZsk { get; set; }

    /// <summary>
    /// Рубрика
    /// </summary>
    public string Rubric { get; set; }

    /// <summary>
    /// Технический номер
    /// </summary>
    public string TechNumber { get; set; }

    /// <summary>
    /// Профильный комитет
    /// </summary>
    public string ProfCommittee { get; set; }

    /// <summary>
    /// Антикоррупционная экспертиза
    /// </summary>
    public AntiCorruptionExpertise AntiCorruptionExpertise { get; set; }

    /// <summary>
    /// Сессия
    /// </summary>
    public docs.Models.Session Session { get; set; }

    /// <summary>
    /// Чтение
    /// </summary>
    public string Reading { get; set; }

    /// <summary>
    /// Номер повестки дня
    /// </summary>
    public string AgendaNumber { get; set; }

    /// <summary>
    /// Стадия рассмотрения
    /// </summary>
    public string Stage { get; set; }

    /// <summary>
    /// Результат рассмотрения
    /// </summary>
    public string Result { get; set; }

    /// <summary>
    /// Субъект права
    /// </summary>
    public string SubjectOfLaw { get; set; }

    /// <summary>
    /// Дата добавления документа
    /// </summary>
    public DateTime? InsDate { get; set; }

    /// <summary>
    /// Место публикации
    /// </summary>
    public string PlaceOfPublication { get; set; }

    /// <summary>
    /// ISN связанных одноранговых документов
    /// </summary>
    public List<int> SingleRangeIsn { get; set; }

    /// <summary>
    /// Отметка времени выгрузки
    /// </summary>
    public DateTime UnloadingTimestamp { get; set; }

    /// <summary>
    /// Признак публикации на открытом сайте
    /// </summary>
    public bool Publish { get; set; }
  }

  public class RcFilesInfo
  {
    public int Isn { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int? OrderNum { get; set; }
  }

  public class AntiCorruptionExpertise
  {
    public DateTime? From { get; set; }

    public DateTime? To { get; set; }
  }

  public class Session
  {
    public long Id { get; set; }

    public virtual ICollection<Document> Documents { get; set; }

    public string Number { get; set; }

    public DateTime? Date { get; set; }

    public string SessionType { get; set; }

    public string Convocation { get; set; }
  }
}
