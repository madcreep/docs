using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docs.Models
{
  public class SessionCardModel
  {
    public Database.Session Session { get; set; }

    /// <summary>
    /// Признак наличия повески
    /// </summary>
    public bool HasAgenda { get; set; }

    /// <summary>
    /// Признак наличия документов
    /// </summary>
    public bool HasDocuments { get; set; }

    /// <summary>
    /// Признак наличия аннотации
    /// </summary>
    public bool HasAnnotation { get; set; }

    public string SessionName
    {
      get
      {
        string type;

        switch (Session.SessionType.ToLower().Trim())
        {
          case "очередная":
            type = "Очередное";
            break;
          case "внеочередная":
            type = "Внеочередное";
            break;
          default:
            type = Session.SessionType;
            break;
        }
        return $"{type} {Session.NumberInt}";
      }
    }
  }
}
