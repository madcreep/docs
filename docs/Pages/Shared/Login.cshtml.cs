using docs.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace docs.Pages.Shared
{
  public class LoginModel : PageModel
  {
    public string UserName { get; private set; }

    public void OnGet(UserManager<User> userManager)
    {
      if (!User.Identity.IsAuthenticated)
      {
        return;
      }

      var userEmail = User.Identity.Name;

      var task = userManager.FindByEmailAsync(userEmail);

      task.Wait();

      var dbUser = task.Result;

      UserName = dbUser?.FullName;
    }
    public IActionResult OnGetPartial(UserManager<User> userManager)
    {
      if (!User.Identity.IsAuthenticated)
      {
        return null;
      }

      var userEmail = User.Identity.Name;

      var task = userManager.FindByEmailAsync(userEmail);

      task.Wait();

      var dbUser = task.Result;

      UserName = dbUser?.FullName;

      return Partial("Login");
    }
    

  }
}
