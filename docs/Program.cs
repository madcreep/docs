using docs.Database;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using docs.Managers.ElasticSearch;
using log4net;

namespace docs
{
  public class Program
  {
    private static readonly ILog Logger = log4net.LogManager.GetLogger(typeof(Program));

    public static void Main(string[] args)
    {
      var host = CreateWebHostBuilder(args).Build();

      ConfigureLog4Net();

      Logger.Info("--------------- СТАРТ ПРИЛОЖЕНИЯ ---------------");

      using (var scope = host.Services.CreateScope())
      {
        var services = scope.ServiceProvider;
        try
        {
          var userManager = services.GetRequiredService<UserManager<User>>();

          var rolesManager = services.GetRequiredService<RoleManager<Role>>();

          var task = DbInitializer.InitializeAsync(userManager, rolesManager);

          task.Wait();
        }
        catch (Exception ex)
        {
          var logger = services.GetRequiredService<ILogger<Program>>();
          logger.LogError(ex, "An error occurred while seeding the database.");
        }

        try
        {
          var elasticManager = services.GetRequiredService<ElasticManager>();

          if (elasticManager.Inited)
          {
            Logger.Info("ElasticManager INITED");
          }
        }
        catch (Exception ex)
        {
          Logger.Warn("ElasticManager NOT INITED", ex);
        }
      }

      host.Run();
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>();

    private static void ConfigureLog4Net()
    {
      var log4netConfig = new XmlDocument();

      using (var stream = File.OpenRead("log4net.config"))
      {
        log4netConfig.Load(stream);
      }
      
      var repo = log4net.LogManager.CreateRepository(
        Assembly.GetEntryAssembly(),
        typeof(log4net.Repository.Hierarchy.Hierarchy));

      log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
    }
  }
}
