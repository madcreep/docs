using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using docs.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using docs.Managers;
using docs.Managers.ElasticSearch;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.FileProviders;

namespace docs
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      var connectionString = Configuration.GetConnectionString("MainDatabase");

      GlobalProperties.ConnectionString = connectionString;

      GlobalProperties.AppUrl = Configuration["AppUrl"];

      GlobalProperties.MainSiteBaseUrl = Configuration["MainSiteBaseUrl"];

      GlobalProperties.StoragePath = Configuration["StoragePath"];

      GlobalProperties.StartYear = Configuration["StartYear"];

      GlobalProperties.PageSize = int.TryParse(Configuration["PageSize"], out var pageSize)
        ? pageSize
        : 20;

      services
        .AddEntityFrameworkNpgsql()
        .AddDbContext<DocumentsContext>()
        .BuildServiceProvider();

      services.AddIdentity<User, Role>(options =>
        {
          options.Password.RequireDigit = true;
          options.Password.RequireLowercase = false;
          options.Password.RequireNonAlphanumeric = false;
          options.Password.RequireUppercase = false;
          options.Password.RequiredLength = 6;
          options.User.RequireUniqueEmail = true;
          options.User.AllowedUserNameCharacters = null;

          // Lockout settings.
          options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
          options.Lockout.MaxFailedAccessAttempts = 3;
          options.Lockout.AllowedForNewUsers = true;

          options.User.RequireUniqueEmail = true;
        }
      ).AddEntityFrameworkStores<DocumentsContext>();

      services.AddSession(options => { options.IdleTimeout = TimeSpan.FromMinutes(60); });

      services.AddAuthentication().AddCookie(CookieAuthenticationDefaults.AuthenticationScheme);
      services.AddSingleton<IAuthorizationHandler, DocumentsAuthorizationHandler>();
      services.AddScoped<DocumentsContext>();

      services.AddSingleton<IMailSettings, MailSettings>();

      services.AddSingleton<IMailManager, MailManager>();

      services.AddSingleton(s => new ElasticManager(
        Configuration["ElasticSearch:ServiceUrl"],
        Configuration["ElasticSearch:IndexForDoc"],
        Configuration["ElasticSearch:IndexForFile"]));

      services.AddAuthorization(options =>
      {
        options.DefaultPolicy = new AuthorizationPolicyBuilder()
          .RequireAuthenticatedUser()
          .RequireClaim("SpecificRight")
          .Build();

        options.AddPolicy("SpecificRight", policy =>
          policy.Requirements.Add(new SpecificRequirement(true)));
      });

      services.ConfigureApplicationCookie(options =>
      {
        options.Cookie.Name = "auth_cookie";
        options.Cookie.SameSite = SameSiteMode.Strict;
        options.LoginPath = new PathString("/Users/LegislativeProcess/Index");
        options.AccessDeniedPath = new PathString("/Users/LegislativeProcess/Index");

        options.ExpireTimeSpan = TimeSpan.FromMinutes(5);
        options.SlidingExpiration = true;
        // Not creating a new object since ASP.NET Identity has Created
        // one already and hooked to the OnValidatePrincipal event.
        // See https://github.com/aspnet/AspNetCore/blob/5a64688d8e192cacffda9440e8725c1ed41a30cf/src/Identity/src/Identity/IdentityServiceCollectionExtensions.cs#L56
        options.Events.OnRedirectToLogin = context =>
        {
          context.Response.StatusCode = StatusCodes.Status401Unauthorized;
          return Task.CompletedTask;
        };
      });
      services.AddCors(options =>
      {
        options.AddPolicy("EnableCORS",
          builder => { builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials().Build(); });
      });
      //services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");
      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

      // In production, the Angular files will be served from this directory
      //services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/dist"; });

      using (var context = new DocumentsContext())
      {
        context.Database.Migrate();
      }
    }

    private DocumentsAuthorizationHandler ImplementationFactory(IServiceProvider arg)
    {
      throw new NotImplementedException();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
      }

      app.UseHttpsRedirection();
      app.UseAuthentication();
      app.UseStaticFiles(new StaticFileOptions
      {
        ServeUnknownFileTypes = true
      });
      app.UseStaticFiles(new StaticFileOptions
      {
        FileProvider = new PhysicalFileProvider(
          Path.Combine(Directory.GetCurrentDirectory(), "ClientApp")),
        RequestPath = "/ClientApp"
      });
      //app.UseSpaStaticFiles();
      app.UseCors("EnableCORS");
      app.UseSession();

      app.Use(async (context, next) =>
      {
        context.Response.Headers.Add("X-Frame-Options", $"ALLOWALL");
        //context.Response.Headers.Add("X-Frame-Options", $"ALLOW-FROM {(context.Request.IsHttps ? "https" : "http")}://{Configuration["MainSiteBaseUrl"]}");
        //context.Response.Headers.Add("X-Frame-Options", $"ALLOW-FROM {(context.Request.IsHttps ? "https" : "http")}://www.{Configuration["MainSiteBaseUrl"]}/");
        var contentSecurityPolicy = Configuration.GetSection("Content-Security-Policy").Get<List<string>>();

        var outContentSecurityPolicy = new List<string>(contentSecurityPolicy.Count * 2);

        var protocol = context.Request.IsHttps ? "https" : "http";

        foreach (var addr in contentSecurityPolicy)
        {
          outContentSecurityPolicy.Add($"{protocol}://{addr}/");
          outContentSecurityPolicy.Add($"{protocol}://www.{addr}/");
        }
        //context.Response.Headers.Add("Content-Security-Policy", $"frame-ancestors {(context.Request.IsHttps ? "https" : "http")}://{Configuration["MainSiteBaseUrl"]}/  {(context.Request.IsHttps ? "https" : "http")}://www.{Configuration["MainSiteBaseUrl"]}/");
        context.Response.Headers.Add("Content-Security-Policy", $"frame-ancestors {string.Join(' ', outContentSecurityPolicy)}");
        await next();
      });

      var supportedCultures = new[] { new CultureInfo("ru-RU") };
      app.UseRequestLocalization(new RequestLocalizationOptions
      {
        DefaultRequestCulture = new RequestCulture("ru-RU"),
        SupportedCultures = supportedCultures,
        SupportedUICultures = supportedCultures
      });

      //app.Use(async (context, next) =>
      //{
      //  if (context.Request.Path == "/")
      //  {
      //    //send the request token as a JavaScript-readable cookie, and Angular will use it by default
      //    var tokens = antiforgery.GetAndStoreTokens(context);
      //    context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
      //              new CookieOptions { HttpOnly = false });
      //  }
      //});
      app.UseMvc(routes =>
      {
        routes.MapRoute(
          name: "Managment1",
          template: "Management",
          defaults: new { area="Management", controller = "Administration", action = "Index" });

        routes.MapRoute(
          name: "Managment2",
          template: "Management/Administration/{id?}",
          defaults: new { area = "Management", controller = "Administration", action = "Index" });

        routes.MapRoute(
          name: "default",
          template: "{area=Users}/{controller=LegislativeProcess}/{action=Index}/{id?}");
      });


      app.ApplicationServices.GetService<IMailManager>();
      //app.UseSpa(spa =>
      //{
      //  // To learn more about options for serving an Angular SPA from ASP.NET Core,
      //  // see https://go.microsoft.com/fwlink/?linkid=864501

      //  spa.Options.SourcePath = "ClientApp";

      //  if (env.IsDevelopment())
      //  {
      //    spa.UseAngularCliServer(npmScript: "start");
      //  }
      //});
    }
  }
}
