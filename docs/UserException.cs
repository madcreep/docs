using System;

namespace docs
{
  public class UserException : Exception
  {
    public UserException(string message)
      : base(message)
    { }
  }
}
