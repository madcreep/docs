using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace docs.Utils
{
  public static class ParseUtils
  {

    public static bool TryIntPart(this string convocationName, out int result)
    {
      result = default;

      if (string.IsNullOrWhiteSpace(convocationName))
      {
        return false;
      }

      var resultString = Regex.Match(convocationName, @"\d+").Value;

      return !string.IsNullOrWhiteSpace(resultString)
             && int.TryParse(resultString, out result);
    }

    public static int GetIntPartFromString(string convocationName)
    {
      var resultString = Regex.Match(convocationName, @"\d+").Value;

      return int.Parse(resultString);

    }
  }
}
