$(document).ready(function () {
  $("#convocations").change(function() {
    var convocationId = $(this).val();
    updateSessions(convocationId);
  })
    .change();
});

function SessionSelected(element) {
  $("#menu_content").hide();

  $("#session_header").hide();

  $('#documents_spinner .spinner-border').removeClass('d-none');

  $('#menu_content').empty();

  var externalReference = element.text();

  $.ajax({
    type: "GET",
    url: baseUrl + "Management/Documents/GetDocumentsBySession?sessionId=" + element.attr('session_id'),
    success: function (msg) {

      $("#session_header").text(externalReference);

      $("#session_header").show();

      $("#menu_content").show();

      $('#documents_spinner .spinner-border').addClass('d-none');

      $('#menu_content').html(msg);

      var previous;

      $('input.display-selector').on('focus', function () {
        previous = $(this)[0].checked;
      }).change(function () {

        let thisElement = $(this);

        DisplayChanged(thisElement, previous);
      })
        .each(function () {
          let that = $(this);

          SetCardColor(that);

          SetShowModal();
        });
    }
  });
}

function DisplayChanged(el, previous) {
  $(el).prop('disabled', true);

  $.ajax({
    type: "POST",
    url: baseUrl + "Management/Documents/ChangePublishValue",
    contentType: "application/json",
    dataType: "json",
    data: JSON.stringify({ documentUuid: el.attr('documentuuid'), display: el[0].checked }),
    success: function (data) {
      if (data.success) {
        SetCardColor(el);
      } else {
        el.checked = previous;
      }

      $(el).prop('disabled', false);
    },
    error: function () {
      el.checked = previous;

      $(el).prop('disabled', false);
    }

  });
}

function SetCardColor(el) {

  let newClass = el[0].checked ? "bg-success" : "bg-danger";

  el.parent("div")
    .removeClass("bg-success")
    .removeClass("bg-danger")
    .removeClass("bg-warning")
    .addClass(newClass);
}

function SetShowModal() {
  $("a.card-anchor").click(function () {

    let that = $(this);

    let doc_uuid = that.attr("documentuuid");

    CardShowModal(doc_uuid);

    return false;
  });
}

function updateSessions(convocationId) {
  $el = $("#session-navbar");
  $el.empty();

  $("#menu_content").hide();

  $("#session_header").hide();

  $('#menu_content').empty();

  $.ajax({
    type: "GET",
    url: baseUrl + "Management/Documents/GetSesions/" + convocationId,
    contentType: "application/json",
    dataType: "json",
    success: function (data) {
      if (data.success) {
        $.each(data.data,
          function(key, value) {
            $el.append($('<a class="nav-link navbar-text mb-1 p-1" id="session_' +
                value.id +
                '" session_id="' +
                value.id +
                '" data-toggle="pill" href="" role="tab" aria-controls="v-pills-home" aria-selected="false">')
              .text(value.name));
          });

        $('#session-navbar a').click(function () {

          let thisElement = $(this);

          SessionSelected(thisElement);
        });
      } else {
        $el.append($("<h3>Ошибка получения сессий</h3>"));
      }
    },
    error: function () {
      $el.append($("<h3>Ошибка сервера</h3>"));
    }
  });
}
