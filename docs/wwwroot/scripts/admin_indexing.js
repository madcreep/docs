var indexing_data;

var timerId;

$(document).ready(function () {
  get_indexing_data();
  init_indexing_buttons();
});

function get_indexing_data() {
  $.ajax({
    type: "GET",
    url: baseUrl + "Management/Indexing/GetIndexingSummary",
    contentType: "application/json",
    success: function (data) {
      if (data.success) {
        indexing_data = data.data;

        update_indexing_elements();
      }
    }
  });
}

function update_indexing_elements() {
  update_indexInitedState();

  update_indexing_counts();

  update_indexing_progressbar();

  if (indexing_data.processReindexingPercent == null || indexing_data.processReindexingPercent == undefined) {
    enable_all_input();
  } else {
    disable_all_input();

    start_update_percent_timer();
  }

  $("#indexing_content").show();
}

function update_indexInitedState() {

  var el = $("#indexInitedState");

  el.find("span").text(indexing_data.indexingInited ? "ИНИЦИАЛИЗИРОВАНА" : "НЕ ИНИЦИАЛИЗИРОВАНА");

  if (indexing_data.indexingInited) {
    el.removeClass("text-danger");
  } else {
    el.addClass("text-danger");
  }
}

function update_indexing_counts() {
  $("#count_documents").text(indexing_data.countDocuments);
  $("#count_notindexing_documents").text(indexing_data.notIndexing);
  $("#count_files").text(indexing_data.countFiles);
  $("#count_notindexing_files").text(indexing_data.notIndexingFiles);
}

function update_indexing_progressbar() {
  let percent = indexing_data.processReindexingPercent;
  let strVal;
  if (percent != null && percent != undefined) {
    strVal = percent + '%';
    $('#indexing_progress').show();

  } else {
    strVal = '0%';
    $('#indexing_progress').hide();
  }

  $('#indexing_progress_bar').css('width', strVal).text(strVal);
}

function init_indexing_buttons() {
  $("#reinit_indexing").click(function () {
    reinit_indexing_system();
  });

  $("#reindex_all").click(function () {
    reindex_all();
  });
}

function reinit_indexing_system() {

  disable_all_input();

  $.ajax({
    type: "POST",
    url: baseUrl + "Management/Indexing/ReinitIndexing",
    contentType: "application/json",
    success: function (data) {
      indexing_data.indexingInited = data.success;

      update_indexInitedState();

      enable_all_input();
    },
    error: function () {
      console.log("Ошибка переинициализации");

      enable_all_input();
    }
  });
}

function reindex_all() {
  disable_all_input();

  $.ajax({
    type: "POST",
    url: baseUrl + "Management/Indexing/ReindexAll/" + $('#onlyNotIndexing').is(":checked"),
    success: function (data) {
      if (data.success) {
        indexing_data.processReindexingPercent = data.percent;

        update_indexing_progressbar();

        start_update_percent_timer();
      } else {
        if (!timerId) {
          enable_all_input();
          get_indexing_data();
        }
      }
    },
    error: function () {
      console.log("Ошибка старта реиндексации");

      enable_all_input();
      get_indexing_data();
    }
  });
}

function disable_all_input() {
  $("#indexing_content").find("input, button").prop('disabled', true);
}

function enable_all_input() {
  $("#indexing_content").find("input, button").prop('disabled', false);
}

function get_indexing_percent() {
  $.ajax({
    type: "POST",
    url: baseUrl + "Management/Indexing/GetReindexPercent",
    success: function (data) {
      indexing_data.processReindexingPercent = data.percent;

      if (data.success) {
        if (indexing_data.processReindexingPercent != null && indexing_data.processReindexingPercent != undefined) {
          start_update_percent_timer();
        } else {
          timerId = null;
          indexing_data.processReindexingPercent = 100;
          setTimeout(function tick() {
            get_indexing_data();
          }, 1000);
          
        }
      } else {
        indexing_data.processReindexingPercent = null;
        enable_all_input();
      }

      update_indexing_progressbar();
    },
    error: function () {
      console.log("Ошибка получения данных о прогрессе переиндексации");
      indexing_data.processReindexingPercent = null;
      update_indexing_progressbar();
      enable_all_input();
    }
  });
}

function start_update_percent_timer() {
  timerId = setTimeout(function tick() {
    get_indexing_percent();
  }, 1000);
}
