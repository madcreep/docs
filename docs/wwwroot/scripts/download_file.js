var uploadFileEl;

//$(document).ready(function () {
function downloadFileInit() {
  $("input#uploadFile[type=file]").change(function() {
    uploadFile();
  });

  $(".btn-send-file").click(function () {

    var that = $(this);

    var attr = that.attr('disabled');

    if (attr !== undefined) {
      return false;
    }

    uploadFileEl = that;

    $("input#uploadFile[type=file]").click();

    return false;
  });
};

function uploadFile() {
  var inputFileEl = $("input#uploadFile[type=file]")[0];

  if (!uploadFileEl || inputFileEl.files.length <= 0) {
    return false;
  }

  var fd = new FormData();
  fd.append("docUuid", uploadFileEl.attr("docUuid"));
  var file = inputFileEl.files[0];
  fd.append('file', file);

  var currentElement = uploadFileEl;
  
  $.ajax({
    url: baseUrl + 'Files/File/UploadAntiCorruptionFile',
    type: 'POST',
    data: fd,
    contentType: false,
    processData: false,
    beforeSend: function() {
      currentElement
        .attr("disabled", true)
        .find("span")
        .text("Отправка заключения");
    },
    success: function (response) {
      if (response.success) {
        currentElement
          .find("span")
          .text("Заключение отправлено");
      }
      else {
        currentElement
            .find("span")
            .text("Ошибка отправки заключения");

        change_text_button_timer(currentElement);
      }
    },
    error: function() {
      currentElement
        .find("span")
        .text("Ошибка отправки заключения");

      change_text_button_timer(currentElement);
    }
  }); 
}

function change_text_button_timer(element) {
  setTimeout(function() {
    element
      .removeAttr("disabled")
      .find("span")
      .text("Отправить заключение");
  }, 5000);
};
