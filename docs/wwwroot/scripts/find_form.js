var selectedPage;

var filterSettings;

var restoreFilterSettingsMode = false;

var startInited = false;

$(document).ready(function () {
  initPaginationFirst(0, 0);

  if (showSingleDocumentId) {
    ffFindByDocId(showSingleDocumentId);
  } else {
    ffInitExpertizeToggle();
    ffInitButtons();
    ffInitDatePickers();
  }
  setFilterButtonTemplate();
  ffSetHiddenEvent();
  
});

function ffInitExpertizeToggle() {
  $('#hasExpertizeFind').change(function () {
    if (this.checked) {
      $('.experize-date').removeAttr('disabled');
    } else {
      $('.experize-date').attr('disabled', 'disabled');
      $("form#findForm").find("#dateExpertizeFromFind").data("DateTimePicker").date(null);
      $("form#findForm").find("#dateExpertizeToFind").data("DateTimePicker").date(null);
    }
  });
}

function ffInitButtons() {
  $('#ffClearButton')
    .click(function () {

      ffClear();

      return false;
    });

  $('#ffFindButton')
    .click(function () {
      selectedPage = null;

      initPagination(0, 0);

      ffFind();
    });
}

function ffInitDatePickers() {

  $('form#findForm')
    .find("#dateFind")

    .datetimepicker({
      locale: 'en',
      format: 'YYYY',
      showTodayButton: true,
      showClear: true,
      showClose: true,
      minDate: moment([ffStartYear, 0, 1]),
      ignoreReadonly: true,
      icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-check',
        clear: 'fa fa-trash',
        close: 'fa fa-times'
      },
      tooltips: {
        today: 'К текущей дате',
        clear: 'Очистить',
        close: 'Закрыть',
        selectMonth: 'Выбрать месяц',
        prevMonth: 'Предыдущий месяц',
        nextMonth: 'Следующий месяц',
        selectYear: 'Выбрать год',
        prevYear: 'Предыдущий год',
        nextYear: 'Следующий год',
        selectDecade: 'Выбрать декаду',
        prevDecade: 'Предыдущая декада',
        nextDecade: 'Следующая декада',
        prevCentury: 'Предыдущий век',
        nextCentury: 'Следующий век'
      }
    })
    .on("dp.change",
      function (e) {
        var year = $(this).val();
        applySessionsByYear(year);
    });

  $('form#findForm')
    .find("#dateFind")
    .data("DateTimePicker").date(new Date(startDate));

  $('form#findForm')
    .find(".experize-date")
    .datetimepicker({
      locale: 'ru',
      format: 'DD.MM.YYYY',
      showTodayButton: true,
      showClear: true,
      showClose: true,
      ignoreReadonly: true,
      icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-check',
        clear: 'fa fa-trash',
        close: 'fa fa-times'
      },
      tooltips: {
        today: 'К текущей дате',
        clear: 'Очистить',
        close: 'Закрыть',
        selectMonth: 'Выбрать месяц',
        prevMonth: 'Предыдущий месяц',
        nextMonth: 'Следующий месяц',
        selectYear: 'Выбрать год',
        prevYear: 'Предыдущий год',
        nextYear: 'Следующий год',
        selectDecade: 'Выбрать декаду',
        prevDecade: 'Предыдущая декада',
        nextDecade: 'Следующая декада',
        prevCentury: 'Предыдущий век',
        nextCentury: 'Следующий век'
      }
    });

  $('form#findForm')
    .find(".experize-date")
    .data("DateTimePicker").date(null);

  saveFilterSettings();

  filterSettings.sessionFind = startSessionId;

  restoreFilterSettingsMode = true;
};

function ffClear() {
  $('form#findForm').find('input').val("");

  $('form#findForm')
    .find("#dateFind")
    .data("DateTimePicker").date(null);

  $('form#findForm').find('input[type=checkbox]')
    .prop('checked', false)
    .change();

  $('form#findForm').find('select').val("").change();
}

function ffFindByDocId(id) {
  $.ajax({
    type: "GET",
    url: (document.baseURI ? document.baseURI : "") + "Users/LegislativeProcess/Find/" + id + "?scheme=" + window.location.protocol,
    cache: false,
    beforeSend: ffBeforeSubmit,
    success: ffSuccessSubmit,
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(textStatus, errorThrown);
      $('#documents_spinner .spinner-border').addClass('d-none');

      $('#card_place').html("Ошибка поиска");
    }
  });
}

function ffFind() {
  hideImportant($(".pagination-div:last"));
  
  $('form#findForm').ajaxSubmit({
    cache: false,
    beforeSubmit: ffBeforeSubmit,
    success: ffSuccessSubmit,
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(textStatus, errorThrown);
      $('#documents_spinner .spinner-border').addClass('d-none');

      $('#card_place').html("Ошибка поиска");
    }
  });
}

function ffBeforeSubmit(formData, jqForm, options) {

  if (options) {
    options.url += "?scheme=" + window.location.protocol;
  }

  if (!showSingleDocumentId) {

    var t = $.grep(formData, function (el) { return el.name === "HasExpertizeFind"; });

    if (t && t.length) {
      t[0].value = String($('#hasExpertizeFind')[0].checked);
    }

    if (selectedPage) {
      formData.push({ name: "Page", value: selectedPage, type: "text", required: false });
    }

    formData.push({ name: "BaseSiteName", value: baseSiteName, type: "text", required: false });
  }

  $('#documents_spinner .spinner-border').removeClass('d-none');

  $('#card_place').html("");

  return true;
}

function hideImportant(elem) {
  elem.attr("style", "display:none !important");
}

function showImportant(elem) {
  elem.removeAttr("style");
}

function ffSuccessSubmit(data) {
  $('#documents_spinner .spinner-border').addClass('d-none');

  if (data.count >= 0) {
    initPagination(data.count, selectedPage ? selectedPage : 1);
  }

  $('#card_place').html(data.context);

  SetShowModal();

  if (showSingleDocumentId) {
    $("a.card-anchor").click();
  }

  OnResize(10);
}

function ffInitDatePicker() {
  $('#dateExpertizeToFind').datetimepicker();
}

function SetShowModal() {
  $("a.card-anchor").click(function () {

    var that = $(this);

    var doc_uuid = that.attr("documentuuid");

    CardShowModal(doc_uuid);

    OnScrollTop();

    return false;
  });
};

function initPaginationFirst(countItems, page) {

  //var countPages = countItems <= 0 ? 0 : Math.ceil(countItems / pageSize);

  $('.pagination-div').bootpag({
    //total: countItems,
    //page: page,
    //maxVisible: 10,
    //leaps: true,
    //firstLastUse: true,
    //wrapClass: 'pagination pagination-sm'
  }).on("page",
    function (event, /* page number here */ num) {
      if (selectedPage !== num) {
        selectedPage = num;
        ffFind();
      }
    });
}

function initPagination(countItems, page) {
  var pagination = $('.pagination-div');

  var countPages = countItems <= 0 ? 0 : Math.ceil(countItems / pageSize);

  pagination.bootpag({
    total: countPages,
    page: page,
    maxVisible: 12,
    leaps: true,
    firstLastUse: true,
    wrapClass: 'pagination pagination-sm',
    activeClass: 'active',
    disabledClass: 'disabled',
    first: '<i class="fa fa-fast-backward"></i>',
    last: '<i class="fa fa-fast-forward"></i>',
    next: '<i class="fa fa-forward"></i>',
    prev: '<i class="fa fa-backward"></i>'
  });
  //$('#pagination').bootpag({
  //  total: countItems,
  //  page: page
  //});


  //var pagination = $('.pagination');

  //pagination.html("");

  //var countPages = countItems <= 0 ? 0 : Math.ceil(countItems / pageSize);

  if (countPages <= 1) {
    hideImportant(pagination);
    //pagination.hide();
  } else {
    showImportant(pagination);
    //pagination.show();
  };
  //  pagination.append($('<li id="prev" class="page-item' + (page === 1 ? " disabled" : "") + '" onclick="prevNextClick(this)"><a class="page-link" href="#"><i class="fa fa-backward"></i></a></li>'));

  //  for (var i = 1; i <= countPages; i++) {
  //    pagination.append($('<li class="page-item' + (i === page ? " active" : "") + '" page="' + i + '" onclick="pageClick(this)"><a class="page-link" href="#">' + i + '</a></li>'));
  //  }

  //  pagination.append($('<li id="next" class="page-item' + (page === countPages ? " disabled" : "") + '" onclick="prevNextClick(this)"><a class="page-link" href="#"><i class="fa fa-forward"></i></a></li>'));
  //}

  //selectedPage = page;
}

function setPaginationActivePage(page) {
  selectedPage = page;

  var pagUl = $(".pagination");

  var pageCount = pagUl.first().find("li[page]").length;

  if (page === 1) {
    pagUl.find("#prev").addClass("disabled");
    pagUl.find("#next").removeClass("disabled");
  }
  else if (page === pageCount) {
    pagUl.find("#prev").removeClass("disabled");
    pagUl.find("#next").addClass("disabled");
  } else {
    pagUl.find("#prev").removeClass("disabled");
    pagUl.find("#next").removeClass("disabled");
  }

  pagUl.find("li[page]").removeClass("active");

  pagUl.find("li[page=" + page + "]").addClass("active");
}

function prevNextClick(that) {
  var el = $(that);

  var currentPage = parseInt(el.parent('ul').find('li.active').attr('page'));

  var pageCount = el.parent('ul').find("li[page]").length;

  var newPage = currentPage;

  if (el.attr("id") === "prev") {
    newPage = currentPage === 1 ? currentPage : currentPage - 1;
  }
  if (el.attr("id") === "next") {
    
    newPage = currentPage === pageCount ? currentPage : currentPage + 1;
  }

  if (newPage !== currentPage) {
    setPaginationActivePage(newPage);

    ffFind();
  }
}

function pageClick(that) {
  var el = $(that);

  if (el.hasClass("active")) {
    return;
  }

  var newPage = parseInt(el.attr("page"));

  setPaginationActivePage(newPage);

  ffFind();
}

function applySessionsByYear(year) {
  var $el = $("#sessionFind");
  $el.attr('disabled', 'disabled');
  $.ajax({
    type: "POST",
    url: baseUrl + "Users/LegislativeProcess/GetSessionsByYear",
    data: JSON.stringify({
      year: year
    }),
    contentType: "application/json",
    dataType: "json",
    success: function (data) {
      $el.empty(); // remove old options

      $el.append($("<option></option>")
        .attr("value", "")
        //.attr("aria-selected", "True")
        .text("Все сессии"));

      $.each(data.data, function (key, value) {
        $el.append($("<option></option>")
          .attr("value", value.id)
          .text(value.value));
      });

      if (restoreFilterSettingsMode) {
        $el.val(filterSettings.sessionFind);
        setFilterButtonTemplate();
        restoreFilterSettingsMode = false;
      }

      $el.removeAttr('disabled');

      if (!startInited) {

        $('#ffFindButton').click();

        startInited = true;
      }
    },
    error: function () {
      $el.empty();
    }
  });
}

function ffSetHiddenEvent() {
  $('#findModal').on('hide.bs.modal',
    function (e) {
      if ($(document.activeElement).attr('id') === "ffFindButton") {
        saveFilterSettings();
      } else {
        restoreSavedeFilterSettings();
      }

      setFilterButtonTemplate();
    });
}

function setFilterButtonTemplate() {
  var btn = $("#filterToggleButton");

  var findForm = $("form#findForm");
  if (findForm) {
    var emptyFilterValues = isEmptyOrSpaces(findForm.find("#contextFind").val()) &&
      isEmptyOrSpaces(findForm.find("#numberActFind").val()) &&
      isEmptyOrSpaces(findForm.find("#npaFind").val()) &&
      isEmptyOrSpaces(findForm.find("#sessionFind").val()) &&
      isEmptyOrSpaces(findForm.find("#subjectFind").val()) &&
      isEmptyOrSpaces(findForm.find("#dateFind").val()) &&
      !findForm.find("#hasExpertizeFind").is(":checked");

    if (emptyFilterValues) {
      btn.removeClass("btn-filter-active");
    } else {
      btn.addClass("btn-filter-active");

    }
  }
}

function isEmptyOrSpaces(str) {
  return str === undefined || str === null || str.match(/^ *$/) !== null;
}

function saveFilterSettings() {
  var findForm = $("form#findForm");
  var df = findForm.find("#dateFind").data("DateTimePicker").date();
  var deff = findForm.find("#dateExpertizeFromFind").data("DateTimePicker").date();
  var detf = findForm.find("#dateExpertizeToFind").data("DateTimePicker").date();
  filterSettings = {
    "contextFind": findForm.find("#contextFind").val(),
    "numberActFind": findForm.find("#numberActFind").val(),
    "npaFind": findForm.find("#npaFind").val(),
    "sessionFind": findForm.find("#sessionFind").val(),
    "subjectFind": findForm.find("#subjectFind").val(),
    "dateFind": (df ? df.format("YYYY") : null),
    "hasExpertizeFind": findForm.find("#hasExpertizeFind").is(":checked"),
    "dateExpertizeFromFind": (deff ? deff.format("YYYY-MM-DD") : null),
    "dateExpertizeToFind": (detf ? detf.format("YYYY-MM-DD") : null)
  }
}

function restoreSavedeFilterSettings() {
  restoreFilterSettingsMode = true;

  var findForm = $("form#findForm");

  findForm.find("#contextFind").val(filterSettings.contextFind);
  findForm.find("#numberActFind").val(filterSettings.numberActFind);
  findForm.find("#npaFind").val(filterSettings.npaFind);
  findForm.find("#subjectFind").val(filterSettings.subjectFind);
  findForm.find("#dateFind").data("DateTimePicker").date((filterSettings.dateFind ? new Date(filterSettings.dateFind) : null));
  findForm.find("#sessionFind").val(filterSettings.sessionFind);
  findForm.find("#hasExpertizeFind").prop('checked', filterSettings.hasExpertizeFind);
  findForm.find("#dateExpertizeFromFind").data("DateTimePicker").date((filterSettings.dateExpertizeFromFind ? new Date(filterSettings.dateExpertizeFromFind) : null));
  findForm.find("#dateExpertizeToFind").data("DateTimePicker").date((filterSettings.dateExpertizeToFind ? new Date(filterSettings.dateExpertizeToFind) : null));
  findForm.find("#hasExpertizeFind").change();
}

function ffToExcel() {
  saveFilterSettings();
  var ffToExcelFrame = $("#ffToExcelFrame");
  filterSettings.baseSiteName = baseSiteName;
  ffToExcelFrame.attr("src", baseUrl + "Files/File/GetFindSummary?" + $.param(filterSettings));
}
