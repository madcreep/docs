$(document).ready(function () {
  if (window.addEventListener) {
    window.addEventListener("message", listener);
  } else {
    window.attachEvent("onmessage", listener);
  }
});

function listener(event) {
  console.log(event.data);
  if (event.data === 'windowResized') {
    OnResize();
  }
}

function OnResize(addHeigth) {
  if (!addHeigth) {
    addHeigth = 0;
  }
  top.postMessage('reloadIframe_' + ($("body").height() + addHeigth), '*');
}

function OnScrollTop() {
  top.postMessage('iframeTop', '*');
}
