function CardShowModal(document_uuid) {

  $('#documentModal').find('#modalLabelLarge').text("");

  $('#documentModal').find('.modal-body').html("<div class='d-flex justify-content-center'><div class='spinner-border text-primary m-5' role='status'><span class='sr-only'>Loading...</span ></div></div>");

  $('#documentModal').modal("show");

  $.ajax({
    type: "GET",
    url: baseUrl + "Management/Documents/Details?id=" + document_uuid + "&baseSiteName=" + baseSiteName + "&scheme=" + window.location.protocol,
    success: function (msg) {
      $('#documentModal').find('#modalLabelLarge').text(msg.title);

      $('#documentModal').find('.modal-body').html(msg.content);

      if ($("body").height() < $('#documentModal').find(".modal-content").height()) {
        OnResize($('#documentModal').find(".modal-content").height() - $("body").height() + 100);
      }
    }
  });

  return false;
}
