using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using org.bit.ElasticSearch;
using org.bit.ElasticSearch.Attributes;
using org.bit.ElasticSearch.Models;
using org.bit.ElasticSearch.Utility;

namespace docsTests.Elastic
{
  [TestClass]
  public class SenderTests
  {
    private const string Uri = "http://localhost:9200";

    private const string Index = "blog";

    //  [TestMethod]
    //  public void SendWorkingTest()
    //  {
    //    var a = new Uri(Uri);

    //    var s = new Uri(a, "/blog");

    //    var path = s.ToString();

    //    var result = ElasticSender.Send(path);

    //    Console.WriteLine(result);

    //    Assert.IsNotNull(result, "Ответ должен возвращатся");
    //  }

    //  [TestMethod]
    //  public void AddIndexTest()
    //  {
    //    var a = new Uri(Uri);

    //    var s = new Uri(a, "/blog");

    //    var path = s.ToString();

    //    var json = @"{
    //""settings"": {
    //    ""analysis"": {
    //      ""filter"": {
    //        ""russian_stop"": {
    //          ""type"":""stop"",
    //          ""stopwords"":""_russian_""
    //        },
    //        ""russian_stemmer"": {
    //          ""type"":""stemmer"",
    //          ""language"":""russian""
    //        }
    //      },
    //      ""analyzer"": {
    //        ""rebuilt_russian"": {
    //          ""tokenizer"":""standard"",
    //          ""filter"": [""lowercase"",""russian_stop"",""russian_stemmer""]
    //        }
    //      }
    //    }
    //  }
    //}";

    //    json = @"{
    //""settings"": {
    //    ""analysis"": {
    //      ""analyzer"": {
    //        ""russian"": {
    //          ""type"": ""standard"",
    //          ""max_token_length"": 5,
    //          ""stopwords"": ""_russian_""
    //        }
    //      }
    //    }
    //  }
    //}";

    //    var result = ElasticSender.Send(path, json, "PUT");

    //    Console.WriteLine(new string('-', 50));
    //    Console.WriteLine("Answer Json");
    //    Console.WriteLine(result);
    //  }

    //  [TestMethod]
    //  public void AddPostTest()
    //  {
    //    var a = new Uri(Uri);

    //    var s = new Uri(a, "/blog/post/3?pretty");

    //    var path = s.ToString();

    //    var json = @"{
    //    ""title"": ""Радостный кот"",
    //    ""content"": ""Коты сегодня в моде"",
    //    ""tags"": [
    //    ""коты"",
    //    ""закон""
    //      ],
    //    ""published_at"": ""2014-09-12T20:44:42+00:00""
    //  }";

    //    var result = ElasticSender.Send(path, json);

    //    Assert.IsNotNull(result, "Ответ должен возвращатся");

    //    Console.WriteLine(result);

    //    var answer = JsonUtility.Deserialize<ElasticAddAnswer>(result);

    //    Console.WriteLine($"Результат операции : {answer.Result.ToString()}");
    //  }

    //  [TestMethod]
    //  public void GetPostByIdTest()
    //  {
    //    var a = new Uri(Uri);

    //    var s = new Uri(a, "/blog/post/2?pretty");

    //    var path = s.ToString();

    //    var result = ElasticSender.Send(path);

    //    Assert.IsNotNull(result, "Ответ должен возвращатся");

    //    Console.WriteLine(result);

    //    var answer = JsonUtility.Deserialize<ElasticGetByIdAnswer<Post>>(result);

    //    Console.WriteLine($"Результат операции : {answer.Source.Content}");
    //  }

    [TestMethod]
    public void SearchPostTest()
    {
      var a = new Uri(Uri);

      var s = new Uri(a, "zskk_legislative_process_doc,zskk_legislative_process_file/_search?size=1000&pretty");

      var path = s.ToString();

      var searchPost = new Post
      {
        Content = "котята",
        Tags = new[] { "котята" }
      };

      var search = new ElasticSearch<Post>(searchPost, "собака")
      {
        Source = false
      };

      var json = JsonUtility.Serialize(search);

      json = @"{
  ""query"":{
    ""multi_match"": {
      ""fields"": [""title"",""content""],
      ""query"": ""имущества"",
      ""fuzziness"": ""6"",
      ""prefix_length"": 2
    }
  },
  ""_source"": false
}";
      //""_source"": false,
      //""stored_fields"" : [""number"", ""doctype""]
      Console.WriteLine("Request Json");
      Console.WriteLine(json);

      var result = ElasticSender.Send(path, json, "POST");

      Assert.IsNotNull(result, "Ответ должен возвращатся");

      Console.WriteLine(new string('-', 50));
      Console.WriteLine("Answer Json");
      Console.WriteLine(result.Content);

      //var answer = JsonUtility.Deserialize<ElasticSearchAnswer>(result);

      //Console.WriteLine($"Результат операции Total : {answer.Hits.Total.Value}");
    }

    //  [TestMethod]
    //  public void DeleteIndexTest()
    //  {
    //    var a = new Uri(Uri);

    //    var s = new Uri(a, "/blog");

    //    var path = s.ToString();

    //    var result = ElasticSender.Send(path, method:"DELETE");

    //    Console.WriteLine(new string('-', 50));
    //    Console.WriteLine("Answer Json");
    //    Console.WriteLine(result);
    //  }

    [TestMethod]
    public void ManagerCloseIndexTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.CloseIndex(Index);

      Console.WriteLine(result);
    }

    [TestMethod]
    public void ManagerOpenIndexTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.OpenIndex(Index);

      Console.WriteLine(result);
    }

    [TestMethod]
    public void ManagerDeleteIndexTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.DeleteIndex(Index);

      Console.WriteLine(result);
    }

    [TestMethod]
    public void ManagerCreateIndexTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.CreateIndex(Index);

      Console.WriteLine(result);
    }




    [TestMethod]
    public void ManagerAddTest()
    {
      var post = new Post
      {
        Title = "История про собаку",
        Content = "Собаки умные животные",
        //Tags = new []{"собака","животное"},
        PublishedAt = new DateTime(2019, 9, 9)
      };

      var manager = new ElasticSearchManager(Uri);

      var result = manager.AddTypeObject(Index,"post", "1233", post);

      //var result = manager.AddTypeObject("ticket", "1236", post);

      Console.WriteLine(result.ToString());

      Assert.IsTrue(result,"Запись должна создаваться (редактироваться)");
    }

    [TestMethod]
    public void ManageDeleteTypeTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.DeleteType(Index, "post", "1236");

      Console.WriteLine(result.ToString());

    }

    [TestMethod]
    public void ManagerSearchTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.Search(new []{ Index },"собака"
        , new[] {"Content"});

      Console.WriteLine("Возвращено записей : {0}", result.Count());
    }

    

    [TestMethod]
    public void ManagerGetIndexSettingsTest()
    {
      var manager = new ElasticSearchManager(Uri);

      manager.GetIndexSettings(Index);
    }

    [TestMethod]
    public void ManagerSetAnalyzerTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.SetIndexAnalyzer(Index,"russian");

      Console.WriteLine(result);

      Assert.IsTrue(result,"Анализатор должен устанавливаться");
    }

    [TestMethod]
    public void ManageHasIndexTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.HasIndex(Index);

      Console.WriteLine(result);

      //Assert.IsTrue(result, "Анализатор должен устанавливаться");
    }

    [TestMethod]
    public void ManageGetIndexSettingsTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.GetIndexSettings(Index);

      //Console.WriteLine(result.blog.settings.index.analysis.analyzer.russians != null);

      //Assert.IsTrue(result, "Анализатор должен устанавливаться");
    }

    [TestMethod]
    public void ManageGetAnalyzerOptionsTest()
    {
      var manager = new ElasticSearchManager(Uri);

      var result = manager.GetIndexAnalyzerOptions(Index);

      Console.WriteLine("Type : {0}; Stopwords : {1}; MaxTokenLength : {2};",
        result.Type,
        result.Stopwords,
        result.MaxTokenLength);

      //Assert.IsTrue(result, "Анализатор должен устанавливаться");
    }

    public class Post
    {
      [ElasticFieldSearch]
      public string Title { get; set; }

      [ElasticFieldSearch]
      public string Content { get; set; }

      public string[] Tags { get; set; }

      public DateTime? PublishedAt { get; set; }
    }
  }
}
