using System;
using System.Net;
using System.Net.Mail;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using org.bit.ElasticSearch.Models;
using org.bit.ElasticSearch.Utility;

namespace docsTests
{
  [TestClass]
  public class SenderTests
  {
    [TestMethod]
    public void TestMethod1()
    {
      var elasticAnalyzerOptions = new ElasticAnalyzerOptions
      {
        Type = "standard",
        MaxTokenLength = 55,
        Stopwords = "_russian_"
      };

      //var analyzer = ElasticAnalyzer.Instance("RUssian", elasticAnalyzerOptions);

      var settings = new ElasticSettings("russian", elasticAnalyzerOptions);

      var json = JsonUtility.Serialize(settings);

      Console.WriteLine(json);
    }

    [TestMethod]
    public void TestMethod2()
    {
      var to = "t_emil@mail.ru";
      var from = "ake@kubzsk.ru";
      var message = new MailMessage(from, to);
      message.Subject = "Using the new SMTP client.";
      message.Body = @"Using this new feature, you can send an email message from an application very easily.";
      var client = new SmtpClient("mail.kubzsk.ru");
      client.Port = 25;
      client.EnableSsl = false;
      client.Credentials = new NetworkCredential("ake@kubzsk.ru", "Nd4!uL5");
      // Credentials are necessary if the server requires the client 
      // to authenticate before it will send email on the client's behalf.
      //client.UseDefaultCredentials = true;

      try
      {
        client.Send(message);
      }
      catch (Exception ex)
      {
        Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
          ex.ToString());
      }
    }
  }
}
